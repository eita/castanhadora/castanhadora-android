package br.coop.eita.castanhadora.ui.adapters;

import android.content.Context;
import android.content.res.ColorStateList;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import androidx.databinding.DataBindingUtil;

import java.util.List;

import br.coop.eita.castanhadora.R;
import br.coop.eita.castanhadora.databinding.DialogoCastanhadoraGridViewItemBinding;
import br.coop.eita.castanhadora.databinding.ListitemUpfIntegranteBinding;
import br.coop.eita.castanhadora.db.models.UpfIntegrante;
import br.coop.eita.castanhadora.util.DialogoCastanhadoraGridViewItem;

public class DialogoCastanhadoraSafraInicioAdapter extends ArrayAdapter<DialogoCastanhadoraGridViewItem> {
    private List<DialogoCastanhadoraGridViewItem> dialogoCastanhadoraGridViewItem;

    public void setDialogoCastanhadoraGridViewItem(List<DialogoCastanhadoraGridViewItem> dialogoCastanhadoraGridViewItem) {
        this.dialogoCastanhadoraGridViewItem = dialogoCastanhadoraGridViewItem;
        notifyDataSetChanged();
    }

    public DialogoCastanhadoraSafraInicioAdapter(Context context, List<DialogoCastanhadoraGridViewItem> objects) {
        super(context, 0, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        DialogoCastanhadoraGridViewItem dialogoCastanhadoraGridViewItem;

        DialogoCastanhadoraGridViewItemBinding binding;

        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.dialogo_castanhadora_grid_view_item, null);
            binding = DataBindingUtil.bind(convertView);
            convertView.setTag(binding);
        } else {
            binding = (DialogoCastanhadoraGridViewItemBinding) convertView.getTag();
        }

        DialogoCastanhadoraGridViewItem item = getItem(position);
        binding.setItem(item);
        if (item.isTextRed()) {
            binding.textView13.setTextColor(getContext().getResources().getColor(R.color.red900));
            binding.textView4.setTextColor(getContext().getResources().getColor(R.color.red900));
        }
        return binding.getRoot();
    }
}
