package br.coop.eita.castanhadora.db.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import br.coop.eita.castanhadora.db.models.Upf;

@Dao
public abstract class UpfDao extends BaseIdentifiableDao<Upf> {

}
