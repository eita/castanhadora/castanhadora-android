package br.coop.eita.castanhadora.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.Bindable;
import androidx.lifecycle.LiveData;

import java.math.BigDecimal;
import java.util.List;

import br.coop.eita.castanhadora.db.DataRepository;
import br.coop.eita.castanhadora.db.models.Safra;
import br.coop.eita.castanhadora.db.models.TrabalhoUpf;
import br.coop.eita.castanhadora.db.models.TrabalhoUpfIntegrante;
import br.coop.eita.castanhadora.db.models.derived.TrabalhoEIntegrantesUpf;
import br.coop.eita.castanhadora.db.models.derived.UpfIntegranteTrabalha;

public class TrabalhoUpfViewModel extends BaseViewModel {

    private TrabalhoUpf currentTrabalhoUpf;

    private Integer currentTrabalhoUpfId;

    private LiveData<List<UpfIntegranteTrabalha>> listIntegrantes;

    public TrabalhoUpfViewModel(@NonNull Application application) {
        super(application);
    }

    public Safra getSafraAtiva() {
        return dataRepository.getSafraAtiva();
    }

    @Bindable
    public TrabalhoUpf getCurrentTrabalhoUpf() {
        return currentTrabalhoUpf;
    }

    public void setCurrentTrabalhoUpf(TrabalhoUpf currentTrabalhoUpf) {
        this.currentTrabalhoUpf = currentTrabalhoUpf;
        currentTrabalhoUpfId = currentTrabalhoUpf != null ? currentTrabalhoUpf.getId() : null;
    }

    public TrabalhoUpf loadCurrentTrabalhoUpf(Integer despesaId) {
        if (despesaId != currentTrabalhoUpfId) {
            if (despesaId != null && despesaId > 0) {
                currentTrabalhoUpf = dataRepository.load(TrabalhoUpf.class, despesaId);
            }
            currentTrabalhoUpfId = despesaId;
        }
        return currentTrabalhoUpf;
    }

    public TrabalhoUpf newTrabalhoUpf() {
        currentTrabalhoUpf = dataRepository.newTrabalhoUpf();
        return currentTrabalhoUpf;
    }

    public void saveCurrentTrabalhoUpf(DataRepository.OnSaveEntityListener listener) {
        dataRepository.save(currentTrabalhoUpf, this.listIntegrantes.getValue() ,listener);
    }

    public LiveData<List<UpfIntegranteTrabalha>> loadIntegrantes(Integer trabalhoId) {
        this.listIntegrantes = dataRepository.getUpfIntegranteTrabalhaList(trabalhoId);
        return this.listIntegrantes;
    }

    public void deleteTrabalhoUpf(TrabalhoUpf trabalhoUpf) {
        dataRepository.delete(trabalhoUpf);
    }

    public LiveData<List<TrabalhoUpf>> getTrabalhos() {
        return dataRepository.getTrabalhos();
    }

    public LiveData<List<TrabalhoEIntegrantesUpf>> getTrabalhosEIntegrantesUpf() {
        return dataRepository.getTrabalhosEIntegrantesUpf();
    }

    public LiveData<List<TrabalhoEIntegrantesUpf>> getTrabalhosEIntegrantesUpfBySafraIdLive(Integer safraId) {
        return dataRepository.getTrabalhosEIntegrantesUpfBySafraIdLive(safraId);
    }

    public List<TrabalhoEIntegrantesUpf> getTrabalhosEIntegrantesUpfBySafraId(Integer safraId) {
        return dataRepository.getTrabalhosEIntegrantesUpfBySafraId(safraId);
    }

    public BigDecimal getTotalDias() {
        return dataRepository.getTotalDias();
    }

    public List<TrabalhoUpfIntegrante> getIntegrantesByTrabalhoId(Integer trabalhoId) {
        return dataRepository.getIntegrantesByTrabalhoId(trabalhoId);
    }
}
