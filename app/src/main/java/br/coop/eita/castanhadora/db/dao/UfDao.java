package br.coop.eita.castanhadora.db.dao;

import androidx.room.Dao;

import br.coop.eita.castanhadora.db.models.Uf;

@Dao
public abstract class UfDao extends BaseIdentifiableDao<Uf> {

}

