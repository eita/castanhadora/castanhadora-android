package br.coop.eita.castanhadora.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProviders;

import com.warkiz.widget.IndicatorSeekBar;
import com.warkiz.widget.OnSeekChangeListener;
import com.warkiz.widget.SeekParams;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import br.coop.eita.castanhadora.R;
import br.coop.eita.castanhadora.databinding.ActivitySafraVendaInicioFormBinding;
import br.coop.eita.castanhadora.db.models.Venda;
import br.coop.eita.castanhadora.db.models.derived.TotalGeral;
import br.coop.eita.castanhadora.util.DialogoCastanhadoraGridViewItem;
import br.coop.eita.castanhadora.util.Util;
import br.coop.eita.castanhadora.viewmodels.VendaViewModel;

public class SafraVendaInicioFormActivity extends AppCompatActivity {

    public static final int RESULT_VENDA_FINALIZADA = 1;

    private VendaViewModel viewModel;

    Boolean changingQuantidade = false;
    Boolean changingValor = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        final ActivitySafraVendaInicioFormBinding binding = DataBindingUtil.setContentView(this,
                R.layout.activity_safra_venda_inicio_form);

        viewModel = ViewModelProviders.of(this).get(VendaViewModel.class);
        binding.setViewmodel(viewModel);

        final TotalGeral totalGeral = viewModel.getTotalGeral();

        final Venda venda = viewModel.newVenda();

        String unidade = viewModel.getUpf().getUnidadeValor();

        ArrayList<DialogoCastanhadoraGridViewItem> valores = new ArrayList<>();

        valores.add(new DialogoCastanhadoraGridViewItem(
                getResources().getString(R.string.safra_totais_em_estoque),
                totalGeral.getQtdeEstoque().toString() + " " + unidade));

        if (totalGeral.getPrecoSugerido().compareTo(BigDecimal.ZERO) > 0) {
            valores.add(new DialogoCastanhadoraGridViewItem(
                    getResources().getString(R.string.safra_totais_preco_sugerido),
                    Util.formatMoney(totalGeral.getPrecoSugerido())));
        } else {
            valores.add(new DialogoCastanhadoraGridViewItem(
                    getResources().getString(R.string.safra_totais_preco_medio),
                    Util.formatMoney(totalGeral.getValorMedioVendido())));
        }

        if (totalGeral.getPrecoSemDepreciacao().compareTo(BigDecimal.ZERO) > 0) {
            valores.add(new DialogoCastanhadoraGridViewItem(
                    getResources().getString(R.string.safra_totais_preco_sem_depreciacao),
                    Util.formatMoney(totalGeral.getPrecoSemDepreciacao())));
        }

        if (totalGeral.getPrecoApenasDespesas().compareTo(BigDecimal.ZERO) > 0) {
            valores.add(new DialogoCastanhadoraGridViewItem(
                    getResources().getString(R.string.safra_totais_preco_so_despesas),
                    Util.formatMoney(totalGeral.getPrecoApenasDespesas())));
        }

        binding.painelSintese.setValores(valores);

        binding.seekQuantidade.setOnSeekChangeListener(new OnSeekChangeListener() {
            @Override
            public void onSeeking(SeekParams seekParams) {
                venda.setQuantidade(seekParams.progress, true);
            }

            @Override
            public void onStartTrackingTouch(IndicatorSeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(IndicatorSeekBar seekBar) {
            }
        });

        binding.seekValor.setOnSeekChangeListener(new OnSeekChangeListener() {
            @Override
            public void onSeeking(SeekParams seekParams) {
                venda.setValorUnitario(new BigDecimal(seekParams.progressFloat),true);
                if (seekParams.progressFloat >= totalGeral.getPrecoSugerido().floatValue()) {
                    binding.seekValor.thumbColor(getResources().getColor(R.color.green900));
                } else if (seekParams.progressFloat >= totalGeral.getPrecoSemDepreciacao().floatValue()) {
                    binding.seekValor.thumbColor(getResources().getColor(R.color.yellow900));
                } else {
                    binding.seekValor.thumbColor(getResources().getColor(R.color.red900));
                }

//                app:isb_thumb_color="@color/colorAccent"
//                app:isb_indicator_color="@color/colorAccent"
//                app:isb_track_progress_color="@color/colorAccent"
            }

            @Override
            public void onStartTrackingTouch(IndicatorSeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(IndicatorSeekBar seekBar) {

            }
        });

        binding.seekQuantidade.setMax(totalGeral.getQtdeEstoque());

        float precoSugeridoSeekBar = getPrecoSugeridoSeekBar();
        binding.seekValor.setMax(precoSugeridoSeekBar * (float)3.0);
        binding.seekValor.setProgress(precoSugeridoSeekBar);

        //Inputs text
        binding.vendaQuantidade.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (!hasFocus) {
                    try {
                        EditText editText = (EditText) view;
                        Integer quantidade = Integer.parseInt(editText.getText().toString());
                        binding.seekQuantidade.setProgress(quantidade);
                    } catch (NumberFormatException exp) {
                        Log.i("Castanhadora", "number exception");

                    }
                }
            }
        });

        binding.vendaValor.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (!hasFocus) {
                    try {
                        EditText editText = (EditText) view;
                        Float valor = viewModel.getVendaAtual().getValorUnitario().floatValue();
                        if (binding.seekValor.getMax() < valor) {
                            binding.seekValor.setMax(valor);
                        }
                        binding.seekValor.setProgress(valor);
                    } catch (NumberFormatException exp) {
                        Log.i("Castanhadora", "number exception");

                    }
                }
            }
        });

        setTitle(R.string.label_venda_nova);
    }

    private float getPrecoSugeridoSeekBar() {
        List<Venda> vendas = viewModel.getVendasBySafraId(viewModel.getSafraAtiva().getId());
        BigDecimal somaVendas = new BigDecimal("0.0");
        float fromMediaVendas = (float) 0.0;

        if (vendas != null && vendas.size() > 0) {
            for (Venda v : vendas) {
                somaVendas = somaVendas.add(v.getValorUnitario());
            }
            fromMediaVendas = somaVendas.divide(BigDecimal.valueOf(vendas.size()),
                    2, RoundingMode.HALF_EVEN).floatValue();
        }
        float fromPrecoSugerido = viewModel.getTotalGeral().getPrecoSugerido().floatValue();

        if (fromMediaVendas > 1.0 || fromPrecoSugerido > 1.0) {
            return Math.max(fromPrecoSugerido, fromMediaVendas);
        } else {
            return (float) 50.0;
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent replyIntent = new Intent();
                setResult(RESULT_CANCELED, replyIntent);
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onClickSimularVenda(View view) {
        Venda venda = viewModel.getVendaAtual();
        List<String> validationErrors  = venda.validateDadosBasicos();
        if (validationErrors.size() == 0) {
            Intent intent = new Intent(SafraVendaInicioFormActivity.this,
                    SafraVendaSimulacaoActivity.class);
            startActivity(intent);

        } else {
            Toast.makeText(this, validationErrors.get(0), Toast.LENGTH_LONG).show();
        }
    }

    public void onClickFecharVenda(View view) {
        Venda venda = viewModel.getVendaAtual();
        List<String> validationErrors  = venda.validateDadosBasicos();
        if (validationErrors.size() == 0) {

            Intent intent = new Intent(SafraVendaInicioFormActivity.this,
                    SafraVendaFormActivity.class);
            startActivityForResult(intent, RESULT_VENDA_FINALIZADA);
        } else {
            Toast.makeText(this, validationErrors.get(0), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == RESULT_VENDA_FINALIZADA) {
            if (resultCode == RESULT_OK) {
                finish();
            }
        }
    }
}
