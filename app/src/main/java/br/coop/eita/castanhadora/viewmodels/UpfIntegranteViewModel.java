package br.coop.eita.castanhadora.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.Bindable;
import androidx.lifecycle.LiveData;

import java.math.BigDecimal;
import java.util.List;

import br.coop.eita.castanhadora.db.DataRepository;
import br.coop.eita.castanhadora.db.models.Safra;
import br.coop.eita.castanhadora.db.models.TrabalhoUpf;
import br.coop.eita.castanhadora.db.models.UpfIntegrante;
import br.coop.eita.castanhadora.db.models.derived.TrabalhoEIntegrantesUpf;
import br.coop.eita.castanhadora.db.models.derived.UpfIntegranteTrabalha;

public class UpfIntegranteViewModel extends BaseViewModel {

    public UpfIntegranteViewModel(@NonNull Application application) {
        super(application);
    }

    public List<UpfIntegrante> getUpfIntegrantes(Integer safraId) {
        return dataRepository.getUpfIntegrantes(safraId);
    }
}
