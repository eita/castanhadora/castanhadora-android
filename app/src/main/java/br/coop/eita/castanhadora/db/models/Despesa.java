package br.coop.eita.castanhadora.db.models;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.text.format.DateUtils;

import androidx.annotation.NonNull;
import androidx.databinding.Bindable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

import org.w3c.dom.Text;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.coop.eita.castanhadora.BR;
import br.coop.eita.castanhadora.Castanhadora;
import br.coop.eita.castanhadora.R;
import br.coop.eita.castanhadora.util.Util;

import static androidx.room.ForeignKey.CASCADE;

@Entity(tableName = Despesa.TABLE_NAME, foreignKeys = {@ForeignKey(onDelete = CASCADE, entity = Safra.class, parentColumns = "id", childColumns = "safra_id"), @ForeignKey(entity = CategoriaDespesa.class, parentColumns = "id", childColumns = "categoria_despesa_id"),})
public class Despesa extends BaseIdentifiableModel {
    public static final String TABLE_NAME = "despesa";

    public static final String TIPO_ALIMENTO = "alimento";
    public static final String TIPO_COMBUSTIVEL = "combustivel";
    public static final String TIPO_MANUTENCAO = "manutencao";
    public static final String TIPO_TRANSPORTE = "transporte";
    public static final String TIPO_CONTAS = "contas";
    public static final String TIPO_REUNIAO = "reuniao";
    public static final String TIPO_FERRAMENTAS = "ferramentas";
    public static final String TIPO_DIARIAS = "diarias";

    @PrimaryKey(autoGenerate = true)
    private int id;

    @NonNull
    @ColumnInfo(name = "tipo")
    private String tipo;

    @NonNull
    @ColumnInfo(name = "valor")
    private BigDecimal valor;

    @NonNull
    @ColumnInfo(name = "data")
    private Date data;

    @ColumnInfo(name = "observacoes")
    private String observacoes;

    @ColumnInfo(name = "aviamento")
    private Boolean aviamento;

    @ColumnInfo(name = "diaria_dias")
    private BigDecimal diariaDias;

    @ColumnInfo(name = "diaria_quantidade_diaristas")
    private Integer diariaQuantidadeDiaristas;

    @ColumnInfo(name = "diaria_valor_diaria")
    private BigDecimal diariaValorDiaria;

    @ColumnInfo(name = "safra_id", index = true)
    public int safraId;

    @ColumnInfo(name = "categoria_despesa_id", index = true)
    public Integer categoriaDespesaId;

    public Despesa() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @NonNull
    public String getTipo() {
        return tipo;
    }

    public void setTipo(@NonNull String tipo) {
        this.tipo = tipo;
    }

    @NonNull
    public BigDecimal getValor() {
        return valor;
    }

    @Bindable
    public String getValorFormatado() {
        return Util.formatMoney(valor);
    }

    public void setValor(@NonNull BigDecimal valor) {
        this.valor = valor;
    }

    @NonNull
    public Date getData() {
        return data;
    }

    public void setData(@NonNull Date data) {
        this.data = data;
    }

    public String getObservacoes() {
        return observacoes;
    }

    public void setObservacoes(String observacoes) {
        this.observacoes = observacoes;
    }

    public Boolean getAviamento() {
        return aviamento;
    }

    public void setAviamento(Boolean aviamento) {
        this.aviamento = aviamento;
    }

    public BigDecimal getDiariaDias() {
        return diariaDias;
    }

    public void setDiariaDias(BigDecimal diariaDias) {
        this.diariaDias = diariaDias;
        atualizaValorPorDiarias();
    }

    public Integer getDiariaQuantidadeDiaristas() {
        return diariaQuantidadeDiaristas;
    }

    public void setDiariaQuantidadeDiaristas(Integer diariaQuantidadeDiaristas) {
        this.diariaQuantidadeDiaristas = diariaQuantidadeDiaristas;
        atualizaValorPorDiarias();
    }

    public BigDecimal getDiariaValorDiaria() {
        return diariaValorDiaria;
    }

    public void setDiariaValorDiaria(BigDecimal diariaValorDiaria) {
        this.diariaValorDiaria = diariaValorDiaria;
        atualizaValorPorDiarias();
    }

    public int getSafraId() {
        return safraId;
    }

    public void setSafraId(int safraId) {
        this.safraId = safraId;
    }

    public Integer getCategoriaDespesaId() {
        return categoriaDespesaId;
    }

    public void setCategoriaDespesaId(Integer categoriaDespesaId) {
        this.categoriaDespesaId = categoriaDespesaId;
    }

    public void atualizaValorPorDiarias() {
        if (TIPO_DIARIAS.equals(tipo)) {
            if ((diariaValorDiaria != null && diariaValorDiaria.floatValue() >= 0.01) &&
                    (diariaQuantidadeDiaristas != null && diariaQuantidadeDiaristas > 0) &&
                    (diariaDias != null && diariaDias.floatValue() >= 0.01)) {
                BigDecimal novoValor = diariaValorDiaria.multiply(diariaDias).multiply(new BigDecimal(diariaQuantidadeDiaristas));
                if (valor == null || Math.abs(novoValor.floatValue() - valor.floatValue()) >= 0.01) {
                    setValor(novoValor);
                    notifyPropertyChanged(BR.valorFormatado);
                }
            } else {
                if (valor != null) {
                    setValor(null);
                    notifyPropertyChanged(BR.valorFormatado);
                }
            }
        }
    }

    @Override
    public List<String> validate() {
        ArrayList<String> validationErrors = new ArrayList<String>();
        if (TextUtils.isEmpty(this.tipo)) {
            validationErrors.add(Castanhadora.APP.getResources().getString(
                    R.string.error_val_despesa_tipo_empty));
        }
        if (this.tipo == Despesa.TIPO_DIARIAS) {
            if (this.diariaDias == null || this.diariaDias.floatValue() < 0.01) {
                validationErrors.add(Castanhadora.APP.getResources().getString(
                        R.string.error_val_despesa_dias_empty));
            }
            if (this.diariaQuantidadeDiaristas == null || this.diariaQuantidadeDiaristas == 0) {
                validationErrors.add(Castanhadora.APP.getResources().getString(
                        R.string.error_val_despesa_quantidade_diaristas_empty));
            }
            if (this.diariaValorDiaria == null || this.diariaValorDiaria.floatValue() < 0.01) {
                validationErrors.add(Castanhadora.APP.getResources().getString(
                        R.string.error_val_despesa_valor_diaria_empty));
            }
        }
        if (this.valor == null || this.valor.floatValue() < 0.01) {
            validationErrors.add(Castanhadora.APP.getResources().getString(
                    R.string.error_val_despesa_valor_empty));
        }
        if (this.data == null) {
            validationErrors.add(Castanhadora.APP.getResources().getString(
                    R.string.error_val_despesa_data_empty));
        }
        if (this.safraId == 0) {
            validationErrors.add(Castanhadora.APP.getResources().getString(
                    R.string.error_val_despesa_safra_empty));
        }

        return validationErrors;
    }

    public String getDescricao() {
        String dData = DateUtils.formatDateTime(Castanhadora.APP, getData().getTime(),
                DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_NUMERIC_DATE |
                        DateUtils.FORMAT_SHOW_YEAR);
        return dData + " - " + getTipo();
    }

    public String getDataCard() {
        String dData = DateUtils.formatDateTime(Castanhadora.APP, getData().getTime(),
                DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_NUMERIC_DATE |
                        DateUtils.FORMAT_SHOW_YEAR);
        return dData;
    }

    public String getDescricaoCard() {
        String observacoes = getObservacoes();
        String descricaoCard = "";
        if (getTipo().equals("diarias")) {
            descricaoCard = descricaoCard + Castanhadora.APP.getApplicationContext().getString(R.string.descricao_card_diarias, getDiariaDias().toString(), getDiariaQuantidadeDiaristas(), Util.formatMoney(getDiariaValorDiaria()));
        }
        if (!TextUtils.isEmpty(observacoes)) {
            descricaoCard = descricaoCard + Castanhadora.APP.getApplicationContext().getString(R.string.descricao_card_observacao, observacoes);
        }

        return descricaoCard;
    }

    public String getSubtituloCard() {
        return "Data: " + getDataCard() + " - Tipo de despesa: " + getTipo();
    }

    public int getIcone() {
        Context context = Castanhadora.APP.getApplicationContext();
        return context.getResources().getIdentifier(
                "ic_despesa_" + getTipo() + "_black_48dp",
                "drawable",
                context.getPackageName());
    }

}
