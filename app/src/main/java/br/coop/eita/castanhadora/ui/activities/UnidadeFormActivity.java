package br.coop.eita.castanhadora.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import java.util.List;

import br.coop.eita.castanhadora.R;
import br.coop.eita.castanhadora.databinding.ActivityUnidadeFormBinding;
import br.coop.eita.castanhadora.db.DataRepository;
import br.coop.eita.castanhadora.ui.util.Wizardable;
import br.coop.eita.castanhadora.viewmodels.UpfViewModel;

public class UnidadeFormActivity extends AppCompatActivity implements Wizardable {

    private UpfViewModel mUpfViewModel;

    private Boolean isWizard;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        isWizard = getIntent().getBooleanExtra("wizard", false);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null && !isWizard) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        ActivityUnidadeFormBinding binding = DataBindingUtil.setContentView(this,
                R.layout.activity_unidade_form);

        mUpfViewModel = ViewModelProviders.of(this).get(UpfViewModel.class);

        binding.setViewmodel(mUpfViewModel);

        ArrayAdapter<CharSequence> unidadeAreaAdapter = ArrayAdapter.createFromResource(this,
                R.array.unidades_area, android.R.layout.simple_spinner_item);
        unidadeAreaAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.inputUnidadeArea.setAdapter(unidadeAreaAdapter);
        binding.inputUnidadeArea.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int pos, long l) {
                        String item = parent.getItemAtPosition(pos).toString();
                        mUpfViewModel.getUpf().setUnidadeArea(item);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });
        Integer positionArea = unidadeAreaAdapter.getPosition(
                mUpfViewModel.getUpf().getUnidadeArea());
        positionArea = positionArea == null ? 0 : positionArea;
        binding.inputUnidadeArea.setSelection(positionArea);

        ArrayAdapter<CharSequence> unidadeValorAdapter = ArrayAdapter.createFromResource(this,
                R.array.unidades_valor, android.R.layout.simple_spinner_item);
        unidadeValorAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.inputUnidadeValor.setAdapter(unidadeValorAdapter);
        binding.inputUnidadeValor.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int pos, long l) {
                        String item = parent.getItemAtPosition(pos).toString();
                        mUpfViewModel.getUpf().setUnidadeValor(item);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });
        Integer positionValor = unidadeValorAdapter.getPosition(
                mUpfViewModel.getUpf().getUnidadeValor());
        positionValor = positionValor == null ? 0 : positionValor;
        binding.inputUnidadeValor.setSelection(positionValor);

        binding.setWizard(isWizard);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!isWizard) {
            getMenuInflater().inflate(R.menu.menu_form, menu);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mUpfViewModel.reloadUpf();
                finish();
                return true;
            case R.id.action_save:
                final UnidadeFormActivity self = this;
                saveUpf(new AfterSaveListener() {
                    @Override
                    public void doAfterSave(Integer id) {
                        Intent replyIntent2 = new Intent();
                        setResult(RESULT_OK, replyIntent2);
                        Toast.makeText(self, R.string.saved_unidades, Toast.LENGTH_LONG).show();
                        finish();
                    }
                });

                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private interface AfterSaveListener {
        void doAfterSave(Integer id);
    }

    @Override
    public void onClickWizardPrevious(View view) {
        Intent intent = new Intent(this, UpfIntegranteListActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        intent.putExtra("wizard", true);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        finish();
    }

    @Override
    public void onClickWizardNext(View view) {
        final UnidadeFormActivity self = this;
        saveUpf(new AfterSaveListener() {
            @Override
            public void doAfterSave(Integer id) {
                Intent intent = new Intent(self, SafraCastanhalFormActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                intent.putExtra("wizard", true);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                finish();
            }
        });
    }

    public void saveUpf(final AfterSaveListener afterSaveListener) {
        final UnidadeFormActivity self = this;
        if (mUpfViewModel.getUpf().unidadesCompletas()) {
            mUpfViewModel.saveUpf(new DataRepository.OnSaveEntityListener() {
                @Override
                public void onSuccess(Integer... ids) {
                    afterSaveListener.doAfterSave(ids[0]);
                }

                @Override
                public void onFailure(int errorCode, List<String> errorMessages) {
                    Toast.makeText(self, errorMessages.get(0), Toast.LENGTH_LONG).show();
                }
            });
        }
    }
}
