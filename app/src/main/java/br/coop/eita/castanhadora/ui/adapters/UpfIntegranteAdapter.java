package br.coop.eita.castanhadora.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import androidx.databinding.DataBindingUtil;

import java.util.List;

import br.coop.eita.castanhadora.R;
import br.coop.eita.castanhadora.databinding.ListitemUpfIntegranteBinding;
import br.coop.eita.castanhadora.db.models.UpfIntegrante;
import br.coop.eita.castanhadora.ui.activities.UpfIntegranteListActivity;

public class UpfIntegranteAdapter extends ArrayAdapter<UpfIntegrante> {
    private List<UpfIntegrante> upfIntegrantes;
    private UpfIntegranteListActivity activity;

    public void setUpfIntegrantes(List<UpfIntegrante> upfIntegrantes) {
        this.upfIntegrantes = upfIntegrantes;
        notifyDataSetChanged();
    }

    public UpfIntegranteAdapter(Context context, List<UpfIntegrante> objects) {
        super(context, 0, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ListitemUpfIntegranteBinding binding;

        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.listitem_upf_integrante, null);
            binding = DataBindingUtil.bind(convertView);
            convertView.setTag(binding);
        } else {
            binding = (ListitemUpfIntegranteBinding) convertView.getTag();
        }

        final UpfIntegrante upfIntegrante = getItem(position);
        final UpfIntegranteAdapter self = this;

        binding.setUpfIntegrante(upfIntegrante);

        binding.editUpfIntegranteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.openEditIntegranteForm(upfIntegrante);
            }
        });

        binding.removeUpfIntegranteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.openDeleteIntegranteDialog(upfIntegrante);
            }
        });

        return binding.getRoot();
    }

    public void setActivity(UpfIntegranteListActivity activity) {
        this.activity = activity;
    }
}
