package br.coop.eita.castanhadora.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import java.util.List;

import br.coop.eita.castanhadora.R;
import br.coop.eita.castanhadora.databinding.ActivityUpfIntegranteFormBinding;
import br.coop.eita.castanhadora.db.DataRepository;
import br.coop.eita.castanhadora.db.models.UpfIntegrante;
import br.coop.eita.castanhadora.viewmodels.UpfViewModel;

public class UpfIntegranteFormActivity extends AppCompatActivity {
    private UpfViewModel mUpfViewModel;

    public static final String INTEGRANTE_UPF_ID_REPLY = "br.coop.eita.castanhadora.integrante_upf_id.REPLY";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        ActivityUpfIntegranteFormBinding binding = DataBindingUtil.setContentView(this,
                R.layout.activity_upf_integrante_form);

        mUpfViewModel = ViewModelProviders.of(this).get(UpfViewModel.class);

        binding.setViewmodel(mUpfViewModel);

        Integer integranteUpfId = getIntent().getIntExtra("upf_integrante_id", 0);

        final UpfIntegrante integranteUpf = mUpfViewModel.loadCurrentIntegranteUpf(integranteUpfId);

        setTitle(integranteUpf.getId() > 0 ? R.string.label_integrante_upf_editar :
                R.string.label_integrante_upf_nova);
        
        //Populate values for spinners
        //parentesco
        ArrayAdapter<CharSequence> parentescoAdapter = ArrayAdapter.createFromResource(this,
                R.array.upf_integrante_parentesco, android.R.layout.simple_spinner_item);
        parentescoAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.inputParentesco.setAdapter(parentescoAdapter);
        binding.inputParentesco.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int pos, long l) {
                        String item = parent.getItemAtPosition(pos).toString();
                        integranteUpf.setParentesco(item);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });
        Integer positionParentesco = parentescoAdapter.getPosition(
                integranteUpf.getParentesco());
        positionParentesco = positionParentesco == null ? 0 : positionParentesco;
        binding.inputParentesco.setSelection(positionParentesco);
        
        //gênero
        ArrayAdapter<CharSequence> generoAdapter = ArrayAdapter.createFromResource(this,
                R.array.upf_integrante_genero, android.R.layout.simple_spinner_item);
        generoAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.inputGenero.setAdapter(generoAdapter);
        binding.inputGenero.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int pos, long l) {
                        String item = parent.getItemAtPosition(pos).toString();
                        integranteUpf.setGenero(item);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });
        Integer positionGenero = generoAdapter.getPosition(
                integranteUpf.getGenero());
        positionGenero = positionGenero == null ? 0 : positionGenero;
        binding.inputGenero.setSelection(positionGenero);
        

        binding.inputUpfIntegranteNome.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(
                Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(binding.inputUpfIntegranteNome, InputMethodManager.SHOW_IMPLICIT);

    }

    public void gravar(View view) {
        final UpfIntegranteFormActivity self = this;
        mUpfViewModel.saveCurrentIntegranteUpf(new DataRepository.OnSaveEntityListener() {
            @Override
            public void onSuccess(Integer... ids) {
                Intent replyIntent = new Intent();
                replyIntent.putExtra(INTEGRANTE_UPF_ID_REPLY, ids[0]);
                setResult(RESULT_OK, replyIntent);
                Toast.makeText(self, R.string.saved_current_integrante_upf,
                        Toast.LENGTH_LONG).show();
                finish();
            }

            @Override
            public void onFailure(int errorCode, List<String> errorMessages) {
                Toast.makeText(self, errorMessages.get(0), Toast.LENGTH_LONG).show();
            }
        });
    }

    public void cancelar(View view) {
        Intent replyIntent = new Intent();
        setResult(RESULT_CANCELED, replyIntent);
        finish();
    }
}
