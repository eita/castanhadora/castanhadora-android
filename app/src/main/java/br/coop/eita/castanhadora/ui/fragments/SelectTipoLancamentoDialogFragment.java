package br.coop.eita.castanhadora.ui.fragments;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;

import br.coop.eita.castanhadora.R;
import br.coop.eita.castanhadora.databinding.DialogoSelectTipoLancamentoBinding;
import br.coop.eita.castanhadora.ui.activities.EquipamentosListActivity;
import br.coop.eita.castanhadora.ui.activities.OpcoesActivity;
import br.coop.eita.castanhadora.ui.activities.SafraDespesasListActivity;
import br.coop.eita.castanhadora.ui.activities.SafraInicioActivity;
import br.coop.eita.castanhadora.ui.activities.SafraProducoesListActivity;
import br.coop.eita.castanhadora.ui.activities.SafraTrabalhosUpfListActivity;
import br.coop.eita.castanhadora.ui.activities.SafraVendasListActivity;

public class SelectTipoLancamentoDialogFragment extends DialogFragment {

    public static final int OPEN_DESPESA_LIST = 1;
    public static final int OPEN_VENDA_LIST = 2;
    public static final int OPEN_TRABALHO_LIST = 3;
    public static final int OPEN_PRODUCAO_LIST = 4;
    public static final int OPEN_EQUIPAMENTOS_LIST = 5;
    public static final int OPEN_INFORMACOES_PESSOAIS = 6;


    public static SelectTipoLancamentoDialogFragment newInstance() {
        SelectTipoLancamentoDialogFragment f = new SelectTipoLancamentoDialogFragment();
        return f;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Material_Light_Dialog);
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
        }
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        getDialog().setTitle(R.string.label_rever_lancamentos);

        DialogoSelectTipoLancamentoBinding binding = DataBindingUtil.inflate(inflater,
                R.layout.dialogo_select_tipo_lancamento, container, false);

        binding.btnDespesas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), SafraDespesasListActivity.class);
                startActivityForResult(intent, OPEN_DESPESA_LIST);
                dismiss();
            }
        });

        binding.btnProducoes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), SafraProducoesListActivity.class);
                startActivityForResult(intent, OPEN_PRODUCAO_LIST);
                dismiss();
            }
        });

        binding.btnTrabalhos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), SafraTrabalhosUpfListActivity.class);
                startActivityForResult(intent, OPEN_TRABALHO_LIST);
                dismiss();
            }
        });

        binding.btnVendas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), SafraVendasListActivity.class);
                startActivityForResult(intent, OPEN_VENDA_LIST);
                dismiss();
            }
        });

        binding.btnEquipamentos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), EquipamentosListActivity.class);
                startActivityForResult(intent, OPEN_EQUIPAMENTOS_LIST);
                dismiss();
            }
        });

        binding.btnInformacoesPessoais.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), OpcoesActivity.class).putExtra(
                        "parent_safra", true);
                startActivityForResult(intent, OPEN_INFORMACOES_PESSOAIS);
                dismiss();
            }
        });

        View v = binding.getRoot();

        return v;
    }

}
