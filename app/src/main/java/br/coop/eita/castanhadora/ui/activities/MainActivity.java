package br.coop.eita.castanhadora.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.Bundle;

import br.coop.eita.castanhadora.Constants;
import br.coop.eita.castanhadora.db.models.Safra;
import br.coop.eita.castanhadora.viewmodels.MainViewModel;

public class MainActivity extends AppCompatActivity {

    MainViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        viewModel = ViewModelProviders.of(this).get(MainViewModel.class);

        int statusInicio = viewModel.getStatusInicio();

        Intent intent;

        switch (statusInicio) {
            case Constants.CASTANHAL_INCOMPLETO:
                intent = new Intent(this, SafraCastanhalFormActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("wizard", true);
                finish();
                startActivity(intent);
                break;
            case Constants.DADOS_PESSOAIS_NAO_CADASTRADOS:
                intent = new Intent(this, UpfDadosPessoaisActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("wizard", true);
                finish();
                startActivity(intent);
                break;
            case Constants.NENHUM_INTEGRANTE_CADASTRADO:
                intent = new Intent(this, UpfIntegranteListActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("wizard", true);
                finish();
                startActivity(intent);
                break;
            case Constants.REMUNERACAO_NAO_ESPECIFICADA:
                intent = new Intent(this, UpfIntegranteListActivity.class);
                //intent = new Intent(this, SafraRemuneracaoDesejadaActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("wizard", true);
                finish();
                startActivity(intent);
                break;
            case Constants.SAFRA_ATUAL_INEXISTENTE:
                intent = new Intent(this, PrimeiraVezActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("wizard", true);
                finish();
                startActivity(intent);
                break;
            case Constants.UNIDADES_NAO_ESPECIFICADAS:
                intent = new Intent(this, UpfIntegranteListActivity.class);
                //intent = new Intent(this, UnidadeFormActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("wizard", true);
                finish();
                startActivity(intent);
                break;
            case Constants.DADOS_INICIAIS_COMPLETOS:
                intent = new Intent(this, SafraInicioActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                finish();
                startActivity(intent);
                break;
            case Constants.NENHUMA_SAFRA_ABERTA:
                intent = new Intent(this, SafraListActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                finish();
                startActivity(intent);
                break;
        }
    }
}
