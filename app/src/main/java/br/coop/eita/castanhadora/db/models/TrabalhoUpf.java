package br.coop.eita.castanhadora.db.models;

import android.text.TextUtils;
import android.text.format.DateUtils;

import androidx.annotation.NonNull;
import androidx.databinding.Bindable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.coop.eita.castanhadora.Castanhadora;
import br.coop.eita.castanhadora.R;

import static androidx.room.ForeignKey.CASCADE;

@Entity(tableName = TrabalhoUpf.TABLE_NAME, foreignKeys = {@ForeignKey(onDelete = CASCADE, entity = Safra.class, parentColumns = "id", childColumns = "safra_id"),})
public class TrabalhoUpf extends BaseIdentifiableModel {

    public static final String TABLE_NAME = "trabalho_upf";

    @PrimaryKey(autoGenerate = true)
    private int id;

    @NonNull
    @ColumnInfo(name = "data")
    private Date data;

    @ColumnInfo(name = "dias")
    private BigDecimal dias;

    @ColumnInfo(name = "remuneracao_desejada")
    private BigDecimal remuneracaoDesejada;

    @ColumnInfo(name = "observacoes")
    public String observacoes;

    @ColumnInfo(name = "safra_id", index = true)
    public int safraId;

    public TrabalhoUpf() {
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @NonNull
    @Bindable
    public Date getData() {
        return data;
    }

    public void setData(@NonNull Date data) {
        this.data = data;
    }

    @Bindable
    public BigDecimal getDias() {
        return dias;
    }

    public void setDias(BigDecimal dias) {
        this.dias = dias;
    }

    @NonNull
    public BigDecimal getRemuneracaoDesejada() {
        return remuneracaoDesejada;
    }

    public void setRemuneracaoDesejada(@NonNull BigDecimal remuneracaoDesejada) {
        this.remuneracaoDesejada = remuneracaoDesejada;
    }

    public String getObservacoes() {
        return observacoes;
    }

    public void setObservacoes(String observacoes) {
        this.observacoes = observacoes;
    }

    public int getSafraId() {
        return safraId;
    }

    public void setSafraId(int safraId) {
        this.safraId = safraId;
    }

    public String getDescricao() {
        String dData = DateUtils.formatDateTime(Castanhadora.APP, getData().getTime(),
                DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_NUMERIC_DATE | DateUtils.FORMAT_SHOW_YEAR);
        return dData + " - " + getDias().toString() + " dias";
    }

    @Override
    public List<String> validate() {
        ArrayList<String> validationErrors = new ArrayList<String>();
        if (dias == null || dias.floatValue() < 0.01) {
            validationErrors.add(
                    Castanhadora.APP.getResources().getString(R.string.error_val_trabalho_dias_empty));
        }
        if (data == null) {
            validationErrors.add(
                    Castanhadora.APP.getResources().getString(R.string.error_val_trabalho_data_empty));
        }
        return validationErrors;
    }

}
