package br.coop.eita.castanhadora.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.Bindable;
import androidx.lifecycle.LiveData;

import java.util.ArrayList;
import java.util.List;

import br.coop.eita.castanhadora.BR;
import br.coop.eita.castanhadora.Castanhadora;
import br.coop.eita.castanhadora.R;
import br.coop.eita.castanhadora.db.DataRepository;
import br.coop.eita.castanhadora.db.models.Safra;
import br.coop.eita.castanhadora.db.models.Upf;
import br.coop.eita.castanhadora.db.models.UpfIntegrante;
import br.coop.eita.castanhadora.db.models.derived.TotalGeral;

public class SafraViewModel extends BaseViewModel {

    private Integer producaoTotal;

    private Integer producaoVendida;

    private Safra safra;

    private Safra safraBase;

    public SafraViewModel(@NonNull Application application) {
        super(application);
    }

    public void saveSafra(final DataRepository.OnSaveEntityListener listener) {
        if (safraBase != null && safraBase.getId() != 0) {
            dataRepository.copiaProfundaSafra(safraBase.getId(), safra.getNome(),
                    safra.getDataInicio(), safra.getDataFinal(),
                    new DataRepository.OnSaveEntityListener() {
                        @Override
                        public void onSuccess(Integer... ids) {
                            listener.onSuccess(ids);
                        }

                        @Override
                        public void onFailure(int errorCode, List<String> errorMessages) {
                            listener.onFailure(errorCode, errorMessages);
                        }
                    });
        } else {
            dataRepository.save(safra, listener);
        }
    }

    /**
     * Loads safra and returns - Used to SafraForm
     *
     * @param safraId
     * @return
     */
    public Safra loadSafra(Integer safraId) {
        if (safraId == null || safraId == 0) {
            safra = dataRepository.newSafra();
        } else {
            safra = dataRepository.load(Safra.class, safraId);
        }
        return safra;
    }

    public Safra loadSafraAtiva() {
        safra = dataRepository.getSafraAtiva();
        return safra;
    }

    public void reloadSafraAtiva() {
        dataRepository.loadSafraAtiva();
    }

    public Safra getSafra() {
        return safra;
    }

    public LiveData<Safra> getSafraAtivaLive() {
        return dataRepository.getSafraAtivaLive();
    }

    public void setSafra(Safra safra) { this.safra = safra;}

    public Upf getUpf() {
        return dataRepository.getUpf();
    }

    public Safra getSafraAtiva() {
        return dataRepository.getSafraAtiva();
    }

    public Integer getProducaoTotal() {
        if (this.producaoTotal == null) {
            this.producaoTotal = dataRepository.getProducaoTotal();
            if (this.producaoTotal == null) {
                this.producaoTotal = 0;
            }
        }
        return this.producaoTotal;
    }

    public Integer getProducaoVendida() {
        if (this.producaoVendida == null) {
            this.producaoVendida = dataRepository.getProducaoVendida();
            if (this.producaoVendida == null) {
                this.producaoVendida = 0;
            }
        }
        return this.producaoVendida;
    }

    public Integer getProducaoEmEstoque() {
        return this.getProducaoTotal() - this.getProducaoVendida();
    }

    public LiveData<TotalGeral> getTotalGeralLive() {
        return dataRepository.getTotalGeralLive();
    }

    public TotalGeral getTotalGeral() {
        return dataRepository.getTotalGeral();
    }

    public TotalGeral getTotalGeral(Integer safraId) {
        return dataRepository.getTotalGeral(safraId);
    }

    public LiveData<List<Safra>> getSafrasLive() {
        return dataRepository.getSafrasLive();
    }

    public List<Safra> getSafras() {
        return dataRepository.getSafras();
    }

    public Safra getSafraById(Integer safraId) {
        return dataRepository.getSafraById(safraId);
    }

    public List<Safra> getSafrasComVazio() {
        ArrayList<Safra> safras = new ArrayList<>();
        Safra safraVazia = new Safra();
        safraVazia.setNome(Castanhadora.APP.getResources().getString(R.string.safra_em_branco));
        safras.add(safraVazia);
        safras.addAll(getSafras());
        return safras;
    }

    public void encerrarSafraAtiva(DataRepository.OnSaveEntityListener listener) {
        dataRepository.encerrarSafra(getSafraAtiva().getId(), listener);
    }

    public Safra getSafraBase() {
        return safraBase;
    }

    public void setSafraBase(Safra safraBase) {
        this.safraBase = safraBase;
    }

    public void ativarSafra(Integer safraId) {
        dataRepository.ativarSafra(safraId);
    }

    public void ativarSafra(Integer safraId, DataRepository.OnSafraAtivaListener listener) {
        dataRepository.ativarSafra(safraId, listener);
    }

    public void deleteSafra(Safra safra, DataRepository.OnDeleteEntityListener listener) {
        dataRepository.delete(safra, listener);
    }

}
