package br.coop.eita.castanhadora.db.models.derived;

import androidx.databinding.BaseObservable;
import androidx.room.Embedded;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

import br.coop.eita.castanhadora.db.models.Equipamento;

public class EquipamentoETipo extends BaseObservable {
    @Embedded
    private Equipamento equipamento;

    private String tipoEquipamento;

    public Equipamento getEquipamento() {
        return equipamento;
    }

    public void setEquipamento(Equipamento equipamento) {
        this.equipamento = equipamento;
    }

    public String getTipoEquipamento() {
        return tipoEquipamento;
    }

    public void setTipoEquipamento(String tipoEquipamento) {
        this.tipoEquipamento = tipoEquipamento;
    }
}
