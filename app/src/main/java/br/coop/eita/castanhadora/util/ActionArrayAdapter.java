package br.coop.eita.castanhadora.util;

import android.content.Context;
import android.widget.ArrayAdapter;

import java.util.List;

public abstract class ActionArrayAdapter<T> extends ArrayAdapter<T> {

    public ActionArrayAdapter.OnDeleteItemClickListener<T> onDeleteItemClickListener;
    public ActionArrayAdapter.OnEditItemClickListener<T> onEditItemClickListener;

    public ActionArrayAdapter(Context context, int resource) {
        super(context, resource);
    }

    public ActionArrayAdapter(Context context, int resource, int textViewResourceId) {
        super(context, resource, textViewResourceId);
    }

    public ActionArrayAdapter(Context context, int resource, T[] objects) {
        super(context, resource, objects);
    }

    public ActionArrayAdapter(Context context, int resource, int textViewResourceId, T[] objects) {
        super(context, resource, textViewResourceId, objects);
    }

    public ActionArrayAdapter(Context context, int resource, List<T> objects) {
        super(context, resource, objects);
    }

    public ActionArrayAdapter(Context context, int resource, int textViewResourceId,
                              List<T> objects) {
        super(context, resource, textViewResourceId, objects);
    }

    public static interface OnEditItemClickListener<T> {
        void onClickEdit(T object);
    }

    public static interface OnDeleteItemClickListener<T> {
        void onClickDelete(T object);
    }

    public void setOnDeleteItemClickListener(OnDeleteItemClickListener onDeleteItemClickListener) {
        this.onDeleteItemClickListener = onDeleteItemClickListener;
    }

    public void setOnEditItemClickListener(OnEditItemClickListener onEditItemClickListener) {
        this.onEditItemClickListener = onEditItemClickListener;
    }

}
