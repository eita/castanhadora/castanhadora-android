package br.coop.eita.castanhadora.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.ImageSpan;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

import br.coop.eita.castanhadora.R;
import br.coop.eita.castanhadora.databinding.ActivitySafraVendaFormBinding;
import br.coop.eita.castanhadora.databinding.ActivityWizardFimBinding;
import br.coop.eita.castanhadora.ui.util.Wizardable;

public class WizardFimActivity extends AppCompatActivity implements Wizardable {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String[] itens = getResources().getStringArray(R.array.passos_wizard);
        ArrayAdapter<String> adapter = new ArrayAdapter<> ( this,
                android.R.layout.simple_list_item_checked, itens);
        final ActivityWizardFimBinding binding = DataBindingUtil.setContentView(this,
                R.layout.activity_wizard_fim);
        binding.checkListWizard.setAdapter( adapter );
        binding.checkListWizard.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        binding.checkListWizard.setItemChecked(0,true);
        binding.checkListWizard.setItemChecked(1,true);
        binding.checkListWizard.setItemChecked(2,true);
        binding.checkListWizard.setItemChecked(3,true);
        binding.checkListWizard.setItemChecked(4,true);
        binding.checkListWizard.setItemChecked(5,true);
        binding.checkListWizard.setItemChecked(6,true);
        binding.checkListWizard.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                binding.checkListWizard.setItemChecked(position,true);
            }
        });;
    }

    public void onClickWizardAprenda(View view) {
        Intent intent = new Intent(this, TutoriaisActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onClickWizardPrevious(View view) {
        Intent intent = new Intent(this, PermissaoCompartilharNuvemActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        intent.putExtra("wizard", true);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        finish();
    }

    @Override
    public void onClickWizardNext(View view) {
        Intent intent = new Intent(this, SafraInicioActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("wizard", true);
        startActivity(intent);
        finish();
    }
}
