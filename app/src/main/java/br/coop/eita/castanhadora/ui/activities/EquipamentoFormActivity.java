package br.coop.eita.castanhadora.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.RadioButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProviders;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import br.coop.eita.castanhadora.R;
import br.coop.eita.castanhadora.databinding.ActivityEquipamentoFormBinding;
import br.coop.eita.castanhadora.db.DataRepository;
import br.coop.eita.castanhadora.db.models.Equipamento;
import br.coop.eita.castanhadora.db.models.TipoEquipamento;
import br.coop.eita.castanhadora.util.DialogoCastanhadoraGridViewItem;
import br.coop.eita.castanhadora.util.Util;
import br.coop.eita.castanhadora.viewmodels.EquipamentoViewModel;

public class EquipamentoFormActivity extends AppCompatActivity {

    private EquipamentoViewModel viewModel;

    public static final String EQUIPAMENTO_ID_REPLY = "br.coop.eita.castanhadora.equipamento_id.REPLY";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        final ActivityEquipamentoFormBinding binding = DataBindingUtil.setContentView(this,
                R.layout.activity_equipamento_form);

        viewModel = ViewModelProviders.of(this).get(EquipamentoViewModel.class);
        binding.setViewmodel(viewModel);

        LiveData<List<TipoEquipamento>> tiposEquipamento = viewModel.getTiposEquipamento();

        List<Equipamento> equipamentos = viewModel.getEquipamentos(viewModel.getSafraAtiva().getId());

        // Cálculo valor total
        BigDecimal valorTotal = new BigDecimal(0);
        BigDecimal valorDepreciacao = viewModel.getTotalGeral().getValorDepreciacao();

        for (Equipamento equipamento: equipamentos) {
            valorTotal = valorTotal.add(equipamento.getValorCompra());
        }

        ArrayList<DialogoCastanhadoraGridViewItem> valores = new ArrayList<>();

        valores.add(new DialogoCastanhadoraGridViewItem(
                getResources().getString(R.string.equipamentos_valor_pago), Util.formatMoney(valorTotal)));

        valores.add(new DialogoCastanhadoraGridViewItem(
                getResources().getString(R.string.equipamentos_valor_depreciacao_ano), Util.formatMoney(valorDepreciacao)));

        binding.painelSintese.setValores(valores);

        //Required to bind live data to view
        binding.setLifecycleOwner(this);

        Integer equipamentoId = getIntent().getIntExtra("equipamento_id", 0);

        final Equipamento equipamento = viewModel.loadCurrentEquipamento(equipamentoId);

        setTitle(
                equipamento.getId() > 0 ? R.string.label_dialog_equipamento_edit_title : R.string.label_dialog_equipamento_new_title);

        binding.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(EquipamentoFormActivity.this, VideoTutorialActivity.class);
                intent.putExtra("videoPath", R.raw.equipamento);
                intent.putExtra("videoTitle", R.string.label_equipamento_equipamento);
                startActivity(intent);
            }
        });

        /*
        binding.inputEquipamentoQuantidade.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    hideKeyboard();
                    textView.clearFocus();
                    binding.anoFabricacaoSpinner.requestFocus();
                    binding.anoFabricacaoSpinner.performClick();
                }
                return true;
            }
        });*/

    }

    public void onRadioUsadoClicked(View view) {
        // Is the home_action_nova_despesa now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio home_action_nova_despesa was clicked
        switch (view.getId()) {
            case R.id.radioEquipamentoNovo:
                if (checked) viewModel.getCurrentEquipamento().setUsado(false);
                break;
            case R.id.radioEquipamentoUsado:
                if (checked) viewModel.getCurrentEquipamento().setUsado(true);
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent replyIntent = new Intent();
                setResult(RESULT_CANCELED, replyIntent);
                finish();
                return true;
            case R.id.action_save:
                final EquipamentoFormActivity self = this;
                viewModel.saveCurrentEquipamento(new DataRepository.OnSaveEntityListener() {
                    @Override
                    public void onSuccess(Integer... ids) {
                        Intent replyIntent = new Intent();
                        replyIntent.putExtra(EQUIPAMENTO_ID_REPLY, ids[0]);
                        setResult(RESULT_OK, replyIntent);
                        Toast.makeText(self, R.string.saved_equipamento, Toast.LENGTH_LONG).show();
                        finish();
                    }

                    @Override
                    public void onFailure(int errorCode, List<String> errorMessages) {
                        Toast.makeText(self, errorMessages.get(0), Toast.LENGTH_LONG).show();
                    }
                });

                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_form, menu);
        return super.onCreateOptionsMenu(menu);
    }

    private void hideKeyboard() {
        InputMethodManager inputManager = (InputMethodManager) getSystemService(
                Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);
    }
}
