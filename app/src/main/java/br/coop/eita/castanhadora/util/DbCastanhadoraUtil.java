package br.coop.eita.castanhadora.util;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.provider.OpenableColumns;

import androidx.sqlite.db.SimpleSQLiteQuery;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import br.coop.eita.castanhadora.Constants;
import br.coop.eita.castanhadora.db.CastanhadoraDatabase;


public class DbCastanhadoraUtil {

    public static final String NOME_ARQUIVO_BACKUP = "backup_castanhadora";
    public static final String NOME_ARQUIVO_BACKUP_ROLLBACK = "backup_castanhadora_rollback";
    public static final String CAMINHO_ARQUIVO_BACKUP_ROLLBACK = "backups/rollback";
    public static final String CAMINHO_BACKUP = "backups";


    public static File backupDatabase(Context context) {
        CastanhadoraDatabase database = CastanhadoraDatabase.getDatabase(context);
        database.safraDao().checkpoint(new SimpleSQLiteQuery("pragma wal_checkpoint(full)"));

        File dbfile = context.getDatabasePath(Constants.NOME_BASE_DADOS);
        String fileName = NOME_ARQUIVO_BACKUP + "_" + new SimpleDateFormat("dd_MM_yyyy", Locale.getDefault()).format(new Date());;
        File sdir = new File(context.getCacheDir(), CAMINHO_BACKUP);
        String sfpath = sdir.getPath() + File.separator + fileName + ".db";

        if (!sdir.exists()) {
            sdir.mkdirs();
        } else {
            checkAndDeleteBackupFile(sdir);
        }

        File savefile = new File(sfpath);
        try {
            if (savefile.createNewFile()) {
                int buffersize = 8 * 1024;
                byte[] buffer = new byte[buffersize];
                int bytes_read = buffersize;
                OutputStream savedb = new FileOutputStream(sfpath);
                InputStream indb = new FileInputStream(dbfile);
                while ((bytes_read = indb.read(buffer, 0, buffersize)) > 0) {
                    savedb.write(buffer, 0, bytes_read);
                }
                savedb.flush();
                indb.close();
                savedb.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return savefile;
    }

    public static void checkAndDeleteBackupFile(File directory) {
        File[] files = directory.listFiles();
        if (files != null) {
            for (File deletingFile : files) {
                if (deletingFile.exists()) {
                    deletingFile.delete();
                }
            }
        }
    }

    public static boolean importarBackup(Uri fileUri, Context context) {
        boolean sucesso = false;
        try {
            assert fileUri != null;
            InputStream inputStream = context.getContentResolver().openInputStream(fileUri);

            if (validFile(fileUri, context, inputStream)) {
                restoreDatabase(inputStream, context);
                if (validateDB(context)) {
                    sucesso = true;
                }
            }
            if (inputStream != null) {
                inputStream.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sucesso;
    }

    private static void restoreDatabase(InputStream inputStreamNewDB, Context context) {
        CastanhadoraDatabase database = CastanhadoraDatabase.getDatabase(context);
        database.close();

//        deleteRestoreBackupFile(context);
//        backupDatabaseForRestore(context);

        File oldDB = context.getDatabasePath(Constants.NOME_BASE_DADOS);
        if (inputStreamNewDB != null) {
            try {
                DbCastanhadoraUtil.copyFile((FileInputStream) inputStreamNewDB, new FileOutputStream(oldDB));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void backupDatabaseForRestore(Context context) {
        File dbfile = context.getDatabasePath(Constants.NOME_BASE_DADOS);
        File sdir = new File(context.getCacheDir(), CAMINHO_ARQUIVO_BACKUP_ROLLBACK);
        String sfpath = sdir.getPath() + File.separator + NOME_ARQUIVO_BACKUP_ROLLBACK;

        if (!sdir.exists()) {
            sdir.mkdirs();
        }
        File savefile = new File(sfpath);
        if (savefile.exists()) {
            savefile.delete();
        }
        try {
            if (savefile.createNewFile()) {
                int buffersize = 8 * 1024;
                byte[] buffer = new byte[buffersize];
                int bytes_read = buffersize;
                OutputStream savedb = new FileOutputStream(sfpath);
                InputStream indb = new FileInputStream(dbfile);
                while ((bytes_read = indb.read(buffer, 0, buffersize)) > 0) {
                    savedb.write(buffer, 0, bytes_read);
                }
                savedb.flush();
                indb.close();
                savedb.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //TODO fazer mais validacoes do banco. Verificar integridade entre versoes do app (campos novos de genero e parentesco)
    private static boolean validateDB(Context context) {
        CastanhadoraDatabase database = CastanhadoraDatabase.getDatabase(context);
        if (database.safraDao().getSafras().size() > 0) {
//            deleteRestoreBackupFile(context);
            return true;
        }

        return false;
    }

//    private boolean validateDB(Context context) {
//        //One - DB might be corrupt with some wrong data flow.
//        //Two - Restoring using a bad file (Possibly, not a DB file).
//        CastanhadoraDatabase database = CastanhadoraDatabase.getDatabase(context);
//        if (sharedPreferences.getBoolean("restoringDatabase", false)) {
//            sharedPreferences.edit().putBoolean("restoringDatabase", false).apply();
//            //Check if restore has been done properly and delete the backupCheckpoint.
//            //TODO fazer mais validacoes do banco
//            if (database.safraDao().getSafras().size() <= 0) {
//                //reset the file and delete the restoredFile. Alert the user of the same.
//                if (restoreDatabase()) {
//                    deleteRestoreBackupFile();
//                } else {
//                    //File probably deleted or some other issue. Alert here with flushing DB.
//                    flushDB();
//                    resetScreen();
//                    restoreFailureDialog();
//                }
//                return false;
//            }
//        } else {
//            if (database.userDao().getUserCount() <= 0) {
//                //Alert here that no user so DB flushed.
//                //Flush DB, take user to Name screen, delete restore File if present and create a new one.
//                flushDB();
//                resetScreen();
//                deleteRestoreBackupFile(getApplicationContext());
//                backupDatabaseForRestore(this, getApplicationContext());
//                dbIssueDialog();
//                return false;
//            }
//        }
//        return true;
//    }

    private static boolean validFile(Uri fileUri, Context context, InputStream inputStream) {
        ContentResolver cr = context.getContentResolver();
        String mime = cr.getType(fileUri);
        if (!"application/octet-stream".equals(mime)) {
            return false;
        }
        if (!getFileNameFromUri(fileUri, context).toLowerCase().contains("castanhadora")) {
            return false;
        }

        try {
            ParcelFileDescriptor fileDescriptor = context.getContentResolver().openFileDescriptor(fileUri, "r");
            int statSize = (int) fileDescriptor.getStatSize();
            byte[] bytes = new byte[statSize];

            inputStream.read(bytes, 0, bytes.length);
            String string = new String(bytes, Charset.defaultCharset());
            String[] versionStrings = string.split("\\|\\|\\|\\|\\|");
            int dbVersion = 0;
            if (versionStrings.length > 1) {
                dbVersion = Integer.parseInt(versionStrings[1]);
            }
            CastanhadoraDatabase database = CastanhadoraDatabase.getDatabase(context);
            int currentDbVersion = database.safraDao().checkpoint(new SimpleSQLiteQuery("PRAGMA user_version"));
            if (dbVersion < 1 || currentDbVersion != dbVersion) {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }


    public static String getFileNameFromUri(Uri uri, Context context) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }

    public static void deleteRestoreBackupFile(Context context) {
        File sdir = new File(context.getCacheDir(), CAMINHO_ARQUIVO_BACKUP_ROLLBACK);
        String sfpath = sdir.getPath() + File.separator + NOME_ARQUIVO_BACKUP_ROLLBACK;
        File restoreFile = new File(sfpath);
        if (restoreFile.exists()) {
            restoreFile.delete();
        }
    }

    public static void copyFile(FileInputStream fromFile, FileOutputStream toFile) throws IOException {
        FileChannel fromChannel = null;
        FileChannel toChannel = null;
        try {
            fromChannel = fromFile.getChannel();
            toChannel = toFile.getChannel();
            fromChannel.transferTo(0, fromChannel.size(), toChannel);
        } finally {
            try {
                if (fromChannel != null) {
                    fromChannel.close();
                }
            } finally {
                if (toChannel != null) {
                    toChannel.close();
                }
            }
        }
    }
}

