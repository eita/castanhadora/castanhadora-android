package br.coop.eita.castanhadora.ui.activities;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import java.util.ArrayList;

import br.coop.eita.castanhadora.R;
import br.coop.eita.castanhadora.databinding.ActivitySafraRelatoriosInicioBinding;
import br.coop.eita.castanhadora.util.DialogoCastanhadoraGridViewItem;
import br.coop.eita.castanhadora.util.Util;
import br.coop.eita.castanhadora.viewmodels.SafraViewModel;

public class SafraRelatoriosInicioActivity extends AppCompatActivity {

    private SafraViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        ActivitySafraRelatoriosInicioBinding binding = DataBindingUtil.setContentView(this,
                R.layout.activity_safra_relatorios_inicio);

        viewModel = ViewModelProviders.of(this).get(SafraViewModel.class);
        binding.setViewmodel(viewModel);



    }
}
