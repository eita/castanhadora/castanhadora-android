package br.coop.eita.castanhadora.ui.util;

import android.text.TextUtils;
import android.text.format.DateUtils;
import android.widget.EditText;
import android.widget.TextView;

import androidx.databinding.BindingAdapter;
import androidx.databinding.InverseBindingAdapter;
import androidx.databinding.InverseBindingListener;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import br.coop.eita.castanhadora.Castanhadora;
import br.coop.eita.castanhadora.R;
import br.coop.eita.castanhadora.ui.components.DateChooser;
import br.coop.eita.castanhadora.ui.components.ExtendedSpinner;
import faranjit.currency.edittext.CurrencyEditText;

public class BindingAdapters {
    @BindingAdapter("android:text")
    public static void setText(EditText editText, Date value) {
        if (value == null) {
            editText.setText("");
        } else {
            editText.setText(DateUtils.formatDateTime(Castanhadora.APP, value.getTime(),
                    DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_NUMERIC_DATE | DateUtils.FORMAT_SHOW_YEAR));
        }
    }

    @BindingAdapter("android:text")
    public static void setText(EditText editText, BigDecimal value) {
        if (value == null) {
            editText.setText("");
        } else {
            editText.setText(value.toString());
        }
    }

    @BindingAdapter("android:text")
    public static void setText(TextView textView, BigDecimal value) {
        if (value == null) {
            textView.setText("-");
        } else {
            textView.setText(value.toString());
        }
    }

    @BindingAdapter("android:text")
    public static void setText(TextView textView, Date value) {
        if (value == null) {
            textView.setText("-");
        } else {
            String datestr =  DateUtils.formatDateTime(Castanhadora.APP, value.getTime(),
                    DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_NUMERIC_DATE | DateUtils.FORMAT_SHOW_YEAR);
            textView.setText(datestr);
        }
    }

    @BindingAdapter("android:text")
    public static void setText(TextView textView, Boolean value) {
        if (value == null) {
            textView.setText("-");
        } else {
            String datestr = Castanhadora.APP.getResources().getString(value ? R.string.dialog_sim :
                    R.string.dialog_nao);
            textView.setText(datestr);
        }
    }

    @InverseBindingAdapter(attribute = "android:text")
    public static BigDecimal getBigDecimal(EditText editText) {
        String str = editText.getText().toString();
        if (TextUtils.isEmpty(str)) {
            return null;
        } else {
            DecimalFormat fmt = new DecimalFormat("0.0",
                    new DecimalFormatSymbols(new Locale("pt", "BR")));
            fmt.setParseBigDecimal(true);
            try {
                return (BigDecimal) fmt.parse(editText.getText().toString());
            } catch (ParseException e) {
                return null;
            }
        }
    }

    //Artificio, para funcionar corretamente com o CurrencyEditText
    @BindingAdapter("android:text")
    public static void setText(CurrencyEditText editText, BigDecimal value) {
        if (value == null) {
            editText.setText("");
        } else {
            editText.setText(value.multiply(new BigDecimal(100)).toBigInteger().toString());
        }
    }

    @InverseBindingAdapter(attribute = "android:text")
    public static BigDecimal getBigDecimal(CurrencyEditText editText) {
        return editText.getCurrencyBigDecimal();
    }

    @BindingAdapter("android:text")
    public static void setText(EditText editText, Integer value) {
        if (value == null) {
            editText.setText("");
        } else {
            editText.setText(value.toString());
        }
    }

    @InverseBindingAdapter(attribute = "android:text")
    public static Integer getInteger(EditText editText) {
        String value = editText.getText().toString();
        if (TextUtils.isEmpty(value)) {
            return null;
        } else {
            try {
                return Integer.parseInt(value);
            } catch (NumberFormatException ne) {
                return null;
            }
        }
    }

    @BindingAdapter("yearFirst")
    public static void setYearFirst(DateChooser view, Boolean yearFirst) {
        view.setYearFirst(yearFirst);
    }

    @BindingAdapter("date")
    public static void setDate(DateChooser view, Date newDate) {
        Date currentDate = view.getDate();
        if (currentDate != newDate) {
            view.setDate(newDate);
        }
    }

    @InverseBindingAdapter(attribute = "date")
    public static Date getDate(DateChooser view) {
        return view.getDate();
    }

    @BindingAdapter(value = "dateAttrChanged")
    public static void setListener(DateChooser dateChooser, final InverseBindingListener listener) {
        if (listener != null) {
            dateChooser.addDateChangedListener(new DateChooser.OnDateChangedListener() {
                @Override
                public void onDateChanged(Date newDate) {
                    listener.onChange();
                }
            });
        }
    }

    /********************
     * SPINNER ADAPTERS *
     ********************/
    //https://medium.com/fueled-engineering/binding-spinner-in-android-c5fa8c084480
    //https://stackoverflow.com/a/37046661/1216027
    // ** https://github.com/chetdeva/spinner-bindings **
    @BindingAdapter("entries")
    public static void setEntries(ExtendedSpinner spinner, List entries) {
        if (entries != null) {
            spinner.setSpinnerEntries(entries);
        }
    }

    @BindingAdapter("selectedValueAttrChanged")
    public static void setInverseBindingListener(ExtendedSpinner spinner,
                                                 InverseBindingListener listener) {
        spinner.setSpinnerInverseBindingListener(listener);
    }

    @BindingAdapter("selectedIdAttrChanged")
    public static void setInverseBindingListenerId(ExtendedSpinner spinner,
                                                   InverseBindingListener listener) {
        spinner.setSpinnerInverseBindingListener(listener);
    }

    @InverseBindingAdapter(attribute = "selectedValue", event = "selectedValueAttrChanged")
    public static Object getSelectedValue(ExtendedSpinner spinner) {
        return spinner.getSelectedValue();
    }

    @InverseBindingAdapter(attribute = "selectedId", event = "selectedValueAttrChanged")
    public static Integer getSelectedId(ExtendedSpinner spinner) {
        return spinner.getSelectedId();
    }


}