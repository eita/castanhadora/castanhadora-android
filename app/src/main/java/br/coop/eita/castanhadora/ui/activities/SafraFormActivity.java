package br.coop.eita.castanhadora.ui.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RadioButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import java.util.List;

import br.coop.eita.castanhadora.R;
import br.coop.eita.castanhadora.databinding.ActivitySafraFormBinding;
import br.coop.eita.castanhadora.db.CastanhadoraDatabase;
import br.coop.eita.castanhadora.db.DataRepository;
import br.coop.eita.castanhadora.db.models.Safra;
import br.coop.eita.castanhadora.ui.util.Wizardable;
import br.coop.eita.castanhadora.viewmodels.SafraViewModel;
import br.coop.eita.castanhadora.viewmodels.UpfViewModel;

public class SafraFormActivity extends AppCompatActivity implements Wizardable {

    public static final String SAFRA_ID_REPLY = "br.coop.eita.castanhadora.safra_id.REPLY";

    private SafraViewModel viewModel;

    private UpfViewModel upfViewModel;

    private Boolean isWizard;

    private ActivitySafraFormBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        isWizard = getIntent().getBooleanExtra("wizard", false);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null && !isWizard) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        binding = DataBindingUtil.setContentView(this, R.layout.activity_safra_form);

        viewModel = ViewModelProviders.of(this).get(SafraViewModel.class);
        binding.setViewmodel(viewModel);

        upfViewModel = ViewModelProviders.of(this).get(UpfViewModel.class);

        Integer safraId = getIntent().getIntExtra("safra_id", 0);

        Safra safra = viewModel.loadSafra(safraId);
        if (isWizard && viewModel.getSafraAtiva() != null) {
            safra = viewModel.loadSafraAtiva();
        }

        if (safra.getId() > 0) {
            setTitle(R.string.label_safra_editar);
        } else {
            setTitle(R.string.label_safra_nova);
        }

        binding.inputSafraNome.requestFocus();
        binding.setWizard(isWizard);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!isWizard) {
            getMenuInflater().inflate(R.menu.menu_form, menu);
        }
        return super.onCreateOptionsMenu(menu);
    }

    public void onRadioSimulacaoClicked(View view) {
        // Is the home_action_nova_despesa now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio home_action_nova_despesa was clicked
        switch (view.getId()) {
            case R.id.radioSafraSimulacaoNao:
                if (checked) viewModel.getSafra().setSimulacao(false);
                break;
            case R.id.radioSafraSimulacaoSim:
                if (checked) viewModel.getSafra().setSimulacao(true);
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                viewModel.loadSafraAtiva();
                Intent replyIntent = new Intent();
                setResult(RESULT_CANCELED, replyIntent);
                finish();
                return true;
            case R.id.action_save:
                final Boolean isNew = viewModel.getSafra().isNew();
                final SafraFormActivity self = this;
                saveSafra(new AfterSaveListener() {
                    @Override
                    public void doAfterSave(Integer id) {
                        Intent replyIntent2 = new Intent();
                        replyIntent2.putExtra(SAFRA_ID_REPLY, id);
                        if (isNew) {
                            viewModel.ativarSafra(id, new DataRepository.OnSafraAtivaListener() {
                                @Override
                                public void onSuccess() {
                                    //Se não foi usada safra base, adiciona pelo menos o integrante principal
                                    if (viewModel.getSafraBase() == null) {
                                        upfViewModel.saveMainIntegrante(new DataRepository.OnSaveEntityListener() {
                                            @Override
                                            public void onSuccess(Integer... ids) {
                                                setResult(Safra.RESULT_SAFRA_ATIVA_ALTERADA);
                                                Toast.makeText(self, R.string.safra_ativa_criada, Toast.LENGTH_LONG).show();
                                                finish();
                                            }

                                            @Override
                                            public void onFailure(int errorCode,
                                                                  List<String> errorMessages) {
                                                Log.i("ERRO AO GRAVAR","Erro ao gravar integrante (1)");
                                            }
                                        });
                                    } else {
                                        setResult(Safra.RESULT_SAFRA_ATIVA_ALTERADA);
                                        Toast.makeText(self, R.string.safra_ativa_criada, Toast.LENGTH_LONG).show();
                                        finish();
                                    }
                                }
                                @Override
                                public void onFailure() {
                                    Log.i("ERRO AO GRAVAR","Erro ao ativar safra (1)");
                                }
                            });
                        } else {
                            setResult(RESULT_OK, replyIntent2);
                            Toast.makeText(self, R.string.safra_ativa_alterada, Toast.LENGTH_LONG).show();
                            finish();
                        }
                    }
                });

                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClickWizardPrevious(View view) {
        Intent intent = new Intent(this, PrimeiraVezActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        intent.putExtra("wizard", true);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        finish();
    }

    @Override
    public void onClickWizardNext(View view) {
        final SafraFormActivity self = this;
        saveSafra(new AfterSaveListener() {
            @Override
            public void doAfterSave(Integer id) {
                viewModel.ativarSafra(id);
                Intent intent = new Intent(self, UpfDadosPessoaisActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                intent.putExtra("wizard", true);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                finish();
            }
        });
    }

    public void onClickEncerrarSafra(View view) {
        final Safra safra = viewModel.getSafra();
        final SafraFormActivity self = this;
        new AlertDialog.Builder(this).setMessage(
                getResources().getString(R.string.msg_dialog_safra_encerrar,
                        safra.getNome())).setPositiveButton(R.string.dialog_sim,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        final Safra safraAtiva = viewModel.getSafraAtiva();
                        viewModel.encerrarSafraAtiva(new DataRepository.OnSaveEntityListener() {
                            @Override
                            public void onSuccess(Integer... ids) {
                                Toast.makeText(self, getResources().getString(R.string.safra_encerrada, safraAtiva.getNome()), Toast.LENGTH_LONG).show();
                                Intent replyIntent2 = new Intent();
                                setResult(Safra.RESULT_SAFRA_ENCERROU, replyIntent2);
                                finish();
                            }

                            @Override
                            public void onFailure(int errorCode, List<String> errorMessages) {
                                Log.i("ERRO AO GRAVAR","Erro ao encerrar safra (1)");
                            }
                        });
                    }
                }).setNegativeButton(R.string.dialog_nao, null).setTitle(
                R.string.label_dialog_safra_delete_title).show();
    }

    private interface AfterSaveListener {
        void doAfterSave(Integer id);
    }

    public void saveSafra(final AfterSaveListener afterSaveListener) {
        final SafraFormActivity self = this;

        //FIXME não está atualizando por binding o viewmodel; isso aqui é armengue
        //Tem que mexer no ExtendedSpinner e ver o que é que tá acontecendo
        if (!isWizard && viewModel.getSafra().isNew()) {
            Safra safraBase = (Safra) binding.inputSafraBase.getSelectedValue();
            if (safraBase == null || safraBase.isNew()) {
                viewModel.setSafraBase(null);
            } else {
                viewModel.setSafraBase(safraBase);
            }
        }

        viewModel.saveSafra(new DataRepository.OnSaveEntityListener() {
            @Override
            public void onSuccess(Integer... ids) {
                afterSaveListener.doAfterSave(ids[0]);
            }

            @Override
            public void onFailure(int errorCode, List<String> errorMessages) {
                //FIXME precisa conseguir um ajuste mais fino: saber se foi este erro de unique ou outro
                if (errorCode == CastanhadoraDatabase.ERROR_CODE_SQLITE_CONSTRAINT) {
                    Toast.makeText(self, R.string.error_val_safra_nome_duplicado,
                            Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(self, errorMessages.get(0), Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}
