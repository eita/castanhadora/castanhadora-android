package br.coop.eita.castanhadora.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.Bindable;
import androidx.lifecycle.LiveData;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import br.coop.eita.castanhadora.db.DataRepository;
import br.coop.eita.castanhadora.db.models.Equipamento;
import br.coop.eita.castanhadora.db.models.Safra;
import br.coop.eita.castanhadora.db.models.TipoEquipamento;
import br.coop.eita.castanhadora.db.models.derived.EquipamentoETipo;
import br.coop.eita.castanhadora.db.models.derived.TotalGeral;

public class EquipamentoViewModel extends BaseViewModel {
    public EquipamentoViewModel(@NonNull Application application) {
        super(application);
    }

    private Equipamento currentEquipamento;

    private Integer currentEquipamentoId;

    LiveData<List<TipoEquipamento>> tiposEquipamento;

    public LiveData<List<Equipamento>> getEquipamentosLive(int safraId) {
        return dataRepository.getEquipamentosLive(safraId);
    }

    public List<Equipamento> getEquipamentos(int safraId) {
        return dataRepository.getEquipamentos(safraId);
    }

    public LiveData<List<EquipamentoETipo>> getEquipamentosETipos() {
        return dataRepository.getEquipamentosETipos();
    }

    public void deleteEquipamento(Equipamento equipamento) {
        dataRepository.delete(equipamento);
    }

    @Bindable
    public Equipamento getCurrentEquipamento() {
        return currentEquipamento;
    }

    public void setCurrentEquipamento(Equipamento currentEquipamento) {
        this.currentEquipamento = currentEquipamento;
        currentEquipamentoId = currentEquipamento != null ? currentEquipamento.getId() : null;
    }

    public Equipamento loadCurrentEquipamento(Integer equipamentoId) {
        if (equipamentoId != currentEquipamentoId) {
            if (equipamentoId != null && equipamentoId > 0) {
                currentEquipamento = dataRepository.load(Equipamento.class, equipamentoId);
            } else {
                currentEquipamento = dataRepository.newEquipamento();
            }
            currentEquipamentoId = equipamentoId;
        }
        return currentEquipamento;
    }

    public void saveCurrentEquipamento(DataRepository.OnSaveEntityListener listener) {
        dataRepository.save(currentEquipamento, listener);
    }

    public Safra getSafraAtiva() {
        return dataRepository.getSafraAtiva();
    }

    public TotalGeral getTotalGeral() {
        return dataRepository.getTotalGeral();
    }

    public LiveData<List<TipoEquipamento>> getTiposEquipamento() {
        if (tiposEquipamento == null) {
            tiposEquipamento = dataRepository.getTiposEquipamento();
        }
        return tiposEquipamento;
    }

    public TipoEquipamento getDummyTipoEquipamento() {
        TipoEquipamento te =  new TipoEquipamento("teste");
        te.setId(1000);
        return te;
    }

    public List<Integer> getAnos() {
        Integer currentYear = Calendar.getInstance().get(Calendar.YEAR);
        ArrayList<Integer> anos = new ArrayList<>();
        for (int i=1900; i<=currentYear; i++) {
            anos.add(i);
        }
        return anos;
    }
}
