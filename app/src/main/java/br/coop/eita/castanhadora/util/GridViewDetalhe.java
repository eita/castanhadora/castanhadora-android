package br.coop.eita.castanhadora.util;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.GridView;

public class GridViewDetalhe extends GridView {
    public GridViewDetalhe(Context context) {
        super(context);
    }

    public GridViewDetalhe(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public GridViewDetalhe(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public GridViewDetalhe(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int expandSpec = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2,
                        MeasureSpec.AT_MOST);

        super.onMeasure(widthMeasureSpec, expandSpec);
    }
}
