package br.coop.eita.castanhadora.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import br.coop.eita.castanhadora.R;
import br.coop.eita.castanhadora.databinding.ActivityPrimeiraVezBinding;
import br.coop.eita.castanhadora.ui.util.Wizardable;

public class PrimeiraVezActivity  extends AppCompatActivity implements Wizardable {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivityPrimeiraVezBinding binding = DataBindingUtil.setContentView(this,
                R.layout.activity_primeira_vez);
    }

    @Override
    public void onClickWizardPrevious(View view) {

    }

    @Override
    public void onClickWizardNext(View view) {
        Intent intent = new Intent(this, SafraFormActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        intent.putExtra("wizard", true);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        finish();
    }
}
