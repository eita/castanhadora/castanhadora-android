package br.coop.eita.castanhadora.db.dao;

import androidx.room.Dao;

import br.coop.eita.castanhadora.db.models.Municipio;

@Dao
public abstract class MunicipioDao extends BaseIdentifiableDao<Municipio> {

}

