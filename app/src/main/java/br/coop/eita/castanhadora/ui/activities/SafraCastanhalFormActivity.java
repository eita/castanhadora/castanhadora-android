package br.coop.eita.castanhadora.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import br.coop.eita.castanhadora.R;
import br.coop.eita.castanhadora.databinding.ActivitySafraCastanhalFormBinding;
import br.coop.eita.castanhadora.db.DataRepository;
import br.coop.eita.castanhadora.ui.util.Wizardable;
import br.coop.eita.castanhadora.viewmodels.SafraViewModel;

import static br.coop.eita.castanhadora.util.MunicipioUfUtil.extractEstadosMapFromJson;
import static br.coop.eita.castanhadora.util.MunicipioUfUtil.extractMunicipiosMapFromJson;
import static br.coop.eita.castanhadora.util.MunicipioUfUtil.getKeyByValue;
import static br.coop.eita.castanhadora.util.MunicipioUfUtil.sortWithoutAccent;

public class SafraCastanhalFormActivity extends AppCompatActivity implements Wizardable {

    public static final String SAFRA_ID_REPLY = "br.coop.eita.castanhadora.safra_id.REPLY";

    private SafraViewModel viewModel;

    private Boolean isWizard;

    private HashMap<String, Integer> estadosMap;

    private HashMap<Integer, HashMap<String, Integer>> municipiosMapMap;

    private ArrayAdapter<String> municipioAdapter;

    private Integer codigoUf;

    private boolean firstTimeSpinner = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        isWizard = getIntent().getBooleanExtra("wizard", false);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null && !isWizard) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        ActivitySafraCastanhalFormBinding binding = DataBindingUtil.setContentView(this,
                R.layout.activity_safra_castanhal_form);

        viewModel = ViewModelProviders.of(this).get(SafraViewModel.class);
        binding.setViewmodel(viewModel);

        viewModel.loadSafraAtiva();

        estadosMap = extractEstadosMapFromJson(this);
        municipiosMapMap = extractMunicipiosMapFromJson(this);

        ArrayList<String> municipiosList =  new ArrayList<>();
        if (viewModel.getSafra().ufId != null) {
            codigoUf = viewModel.getSafra().ufId;
            Set<String> municipiosSet = municipiosMapMap.get(viewModel.getSafra().ufId).keySet();
            municipiosList.addAll(municipiosSet);
            sortWithoutAccent(municipiosList);
        }
        municipiosList.add(0, "");

        municipioAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, municipiosList){
            @Override
            public boolean isEnabled(int position){
                return position != 0;
            }
        };
        binding.inputMunicipio.setAdapter(municipioAdapter);

        Set<String> estadosSet = estadosMap.keySet();
        ArrayList<String> estadosList =  new ArrayList<>(estadosSet);
        sortWithoutAccent(estadosList);
        estadosList.add(0, "");

        ArrayAdapter<String> estadoAdapter =  new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, estadosList){
            @Override
            public boolean isEnabled(int position){
                return position != 0;
            }
        };

        estadoAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.inputEstado.setAdapter(estadoAdapter);

        binding.inputEstado.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int pos, long l) {
                        if (!firstTimeSpinner) {
                            String item = parent.getItemAtPosition(pos).toString();
                            codigoUf = estadosMap.get(item);

                            Set<String> municipiosSet = municipiosMapMap.get(codigoUf).keySet();
                            ArrayList<String> municipiosList =  new ArrayList<>(municipiosSet);
                            sortWithoutAccent(municipiosList);
                            municipiosList.add(0, "");

                            municipioAdapter.clear();
                            municipioAdapter.addAll(municipiosList);
                            municipioAdapter.notifyDataSetChanged();
                            binding.inputMunicipio.setSelection(0,false);
                            viewModel.getSafra().setUfId(codigoUf);
                            viewModel.getSafra().setMunicipioId(null);
                        } else {
                            firstTimeSpinner = false;
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });
        binding.inputMunicipio.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int pos, long l) {
                        String item = parent.getItemAtPosition(pos).toString();
                        if (!item.equals("")) {
                            Integer municipioId = municipiosMapMap.get(codigoUf).get(item);
                            viewModel.getSafra().setMunicipioId(municipioId);
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {}
                });

        int positionEstado = 0;
        if (viewModel.getSafra().ufId != null) {
            String estadoName = getKeyByValue(estadosMap, viewModel.getSafra().ufId);
            positionEstado = estadoAdapter.getPosition(estadoName);
        }
        binding.inputEstado.setSelection(positionEstado);

        int positionMunicipio = 0;
        if (viewModel.getSafra().ufId != null && viewModel.getSafra().municipioId != null) {
            HashMap<String, Integer> municipiosMap = municipiosMapMap.get(viewModel.getSafra().ufId);
            String municipioName = getKeyByValue(municipiosMap, viewModel.getSafra().municipioId);
            positionMunicipio = municipioAdapter.getPosition(municipioName);
        }
        binding.inputMunicipio.setSelection(positionMunicipio);

        actionBar.setTitle(R.string.label_castanhal_form);
        binding.setWizard(isWizard);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!isWizard) {
            getMenuInflater().inflate(R.menu.menu_form, menu);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                viewModel.reloadSafraAtiva();
                Intent replyIntent = new Intent();
                setResult(RESULT_CANCELED, replyIntent);
                finish();
                return true;
            case R.id.action_save:
                final SafraCastanhalFormActivity self = this;
                saveSafra(new AfterSaveListener() {
                    @Override
                    public void doAfterSave(Integer id) {
                        Intent replyIntent2 = new Intent();
                        replyIntent2.putExtra(SAFRA_ID_REPLY, id);
                        setResult(RESULT_OK, replyIntent2);
                        Toast.makeText(self, R.string.saved_castanhal, Toast.LENGTH_LONG).show();
                        finish();
                    }
                });

                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private interface AfterSaveListener {
        void doAfterSave(Integer id);
    }

    @Override
    public void onClickWizardPrevious(View view) {
        Intent intent = new Intent(this, UnidadeFormActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        intent.putExtra("wizard", true);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        finish();
    }

    @Override
    public void onClickWizardNext(View view) {
        final SafraCastanhalFormActivity self = this;

        saveSafra(new AfterSaveListener() {
            @Override
            public void doAfterSave(Integer id) {
                Intent intent = new Intent(self, SafraRemuneracaoDesejadaActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                intent.putExtra("wizard", true);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                finish();
            }
        });
    }

    public void saveSafra(final AfterSaveListener listener) {
        final SafraCastanhalFormActivity self = this;

        viewModel.saveSafra(new DataRepository.OnSaveEntityListener() {
            @Override
            public void onSuccess(Integer... ids) {
                listener.doAfterSave(ids[0]);
            }

            @Override
            public void onFailure(int errorCode, List<String> errorMessages) {
                Toast.makeText(self, errorMessages.get(0), Toast.LENGTH_LONG).show();
            }
        });

    }
}
