package br.coop.eita.castanhadora.db.models;

public abstract class BaseIdentifiableModel extends BaseModel {
    public abstract int getId();
    public abstract void setId(int id);

    public boolean isNew() {
        return getId() <= 0;
    }
}
