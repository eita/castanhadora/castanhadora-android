package br.coop.eita.castanhadora.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;

import java.util.List;

import br.coop.eita.castanhadora.db.DataRepository;
import br.coop.eita.castanhadora.db.models.Producao;
import br.coop.eita.castanhadora.db.models.Safra;
import br.coop.eita.castanhadora.db.models.Upf;

public class ProducaoViewModel extends BaseViewModel {
    private Producao producao;

    private Integer producaoTotal;

    private Integer producaoVendida;

    public ProducaoViewModel(@NonNull Application application) {
        super(application);
    }

    public Upf getUpf() {
        return dataRepository.getUpf();
    }

    public Producao getProducao() {
        return producao;
    }

    public void setProducao(Producao producao) {
        this.producao = producao;
    }

    /**
     * Loads or creates produção
     * @param producaoId
     * @return
     */
    public Producao loadProducao(Integer producaoId) {
        producao = null;
        if (producaoId != null && producaoId > 0) {
            producao = dataRepository.load(Producao.class, producaoId);
        } else {
            Safra safraAtiva = getSafraAtiva();
            if (safraAtiva != null) {
                producao = dataRepository.newProducao();
            }
        }
        return producao;    
    }

    public void saveProducao(DataRepository.OnSaveEntityListener listener) {
        dataRepository.save(producao, listener);
    }

    public Safra getSafraAtiva() {
        return dataRepository.getSafraAtiva();
    }

    public void deleteProducao(Producao producao) {
        dataRepository.delete(producao);
    }

    public LiveData<List<Producao>> getProducoes() {
        return dataRepository.getProducoes();
    }

    public LiveData<List<Producao>> getProducoesBySafraIdLive(Integer safraId) {
        return dataRepository.getProducoesBySafraIdLive(safraId);
    }

    public List<Producao> getProducoesBySafraId(Integer safraId) {
        return dataRepository.getProducoesBySafraId(safraId);
    }

    public Integer getProducaoTotal() {
        if (this.producaoTotal == null) {
            this.producaoTotal = dataRepository.getProducaoTotal();
            if (this.producaoTotal == null) {
                this.producaoTotal = 0;
            }
        }
        return this.producaoTotal;
    }

    public Integer getProducaoVendida() {
        if (this.producaoVendida == null) {
            this.producaoVendida = dataRepository.getProducaoVendida();
            if (this.producaoVendida == null) {
                this.producaoVendida = 0;
            }
        }
        return this.producaoVendida;
    }

    public Integer getProducaoEmEstoque() {
        return this.getProducaoTotal() - this.getProducaoVendida();
    }

}
