package br.coop.eita.castanhadora.db.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Query;
import androidx.room.RawQuery;
import androidx.sqlite.db.SupportSQLiteQuery;

import java.util.List;

import br.coop.eita.castanhadora.db.models.Despesa;
import br.coop.eita.castanhadora.db.models.Equipamento;
import br.coop.eita.castanhadora.db.models.Producao;
import br.coop.eita.castanhadora.db.models.Safra;
import br.coop.eita.castanhadora.db.models.TrabalhoUpf;
import br.coop.eita.castanhadora.db.models.TrabalhoUpfIntegrante;
import br.coop.eita.castanhadora.db.models.Upf;
import br.coop.eita.castanhadora.db.models.Venda;
import br.coop.eita.castanhadora.db.models.derived.TotalGeral;

@Dao
public abstract class SafraDao extends BaseIdentifiableDao<Safra> {

    @Query("SELECT * FROM " + Safra.TABLE_NAME + " ORDER BY data_inicio DESC")
    public abstract LiveData<List<Safra>> getSafrasLive();

    @Query("SELECT * FROM " + Safra.TABLE_NAME + " ORDER BY data_inicio DESC")
    public abstract List<Safra> getSafras();

    @Query("SELECT * FROM " + Safra.TABLE_NAME + " WHERE id = :safraId")
    public abstract Safra getSafraById(Integer safraId);

    @Query("SELECT * FROM " + Safra.TABLE_NAME + " WHERE ativa = 1 ORDER BY data_inicio DESC LIMIT 1")
    public abstract Safra getSafraAtiva();

    @Query("SELECT * FROM " + Safra.TABLE_NAME + " WHERE ativa = 1 ORDER BY data_inicio DESC LIMIT 1")
    public abstract LiveData<Safra> getSafraAtivaLive();

    @Query("SELECT COUNT(*) FROM " + Safra.TABLE_NAME)
    public abstract Integer getRowCount();

    @RawQuery(observedEntities = {Safra.class, Despesa.class, Equipamento.class, Producao.class, TrabalhoUpf.class, TrabalhoUpfIntegrante.class, Venda.class})
    public abstract LiveData<TotalGeral> getTotalGeralLive(SupportSQLiteQuery query);

    @RawQuery()
    public abstract TotalGeral getTotalGeral(SupportSQLiteQuery query);

    @RawQuery()
    public abstract int checkpoint(SupportSQLiteQuery query);
}
