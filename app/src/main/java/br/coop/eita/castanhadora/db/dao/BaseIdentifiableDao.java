package br.coop.eita.castanhadora.db.dao;

import androidx.room.RawQuery;
import androidx.room.util.StringUtil;
import androidx.sqlite.db.SimpleSQLiteQuery;

import com.google.common.base.CaseFormat;

import java.lang.reflect.ParameterizedType;

public abstract class BaseIdentifiableDao<T> extends BaseDao<T> {

    public T load(Integer id) {
        SimpleSQLiteQuery query = new SimpleSQLiteQuery(
                "select * from " + getTableName() + " WHERE id = " + id);
        return doFind(query);
    }

}
