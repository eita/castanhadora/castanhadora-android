package br.coop.eita.castanhadora.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;

import java.util.List;

import br.coop.eita.castanhadora.db.DataRepository;
import br.coop.eita.castanhadora.db.models.Safra;
import br.coop.eita.castanhadora.db.models.Upf;
import br.coop.eita.castanhadora.db.models.Venda;
import br.coop.eita.castanhadora.db.models.derived.TotalGeral;

public class VendaViewModel extends BaseViewModel {


    private TotalGeral totalGeral;

    public VendaViewModel(@NonNull Application application) {
        super(application);
    }

    public Safra getSafraAtiva() {
        return dataRepository.getSafraAtiva();
    }

    public Upf getUpf() {
        return dataRepository.getUpf();
    }

    public Venda newVenda() {
        return dataRepository.newVenda();
    }

    public Venda getVendaAtual() {
        return dataRepository.getVendaAtual();
    }

    public void loadVendaAtual(Integer vendaId) { dataRepository.loadVendaAtual(vendaId);}

    public void deleteVenda(Venda venda) {
        dataRepository.delete(venda);
    }

    public LiveData<List<Venda>> getVendas() {
        return dataRepository.getVendas();
    }

    public LiveData<List<Venda>> getVendasBySafraIdLive(Integer safraId) {
        return dataRepository.getVendasBySafraIdLive(safraId);
    }

    public List<Venda> getVendasBySafraId(Integer safraId) {
        return dataRepository.getVendasBySafraId(safraId);
    }

    public TotalGeral getTotalGeral() {
        if (totalGeral == null) {
            totalGeral = dataRepository.getTotalGeral();
        }
        return totalGeral;
    }

    public TotalGeral getCachedTotalGeral() {
        return dataRepository.getCachedTotalGeral();
    }

    public void saveVenda(DataRepository.OnSaveEntityListener listener) {
        dataRepository.save(getVendaAtual(), listener);
    }

    public int getVendaStatus() {
        Float valorUnitario = getVendaAtual().getValorUnitario().floatValue();

        if (valorUnitario < totalGeral.getPrecoApenasDespesas().floatValue()) {
            return Venda.STATUS_INSUFICIENTE;
        } else if (valorUnitario < totalGeral.getPrecoSemDepreciacao().floatValue()) {
            return Venda.STATUS_PAGA_DESPESAS;
        } else if (valorUnitario < totalGeral.getPrecoSugerido().floatValue()) {
            return Venda.STATUS_PAGA_DESPESAS_E_REMUNERACAO;
        } else {
            return Venda.STATUS_PAGA_TUDO;
        }
    }


}
