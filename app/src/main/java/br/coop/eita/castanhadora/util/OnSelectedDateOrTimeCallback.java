package br.coop.eita.castanhadora.util;

import java.util.Date;

public interface OnSelectedDateOrTimeCallback {
    public void onSelect(Date newDate);
}