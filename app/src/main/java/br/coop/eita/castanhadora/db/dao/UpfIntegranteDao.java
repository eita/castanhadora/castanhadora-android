package br.coop.eita.castanhadora.db.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Query;
import androidx.room.Transaction;

import java.util.List;

import br.coop.eita.castanhadora.db.models.Safra;
import br.coop.eita.castanhadora.db.models.UpfIntegrante;
import br.coop.eita.castanhadora.db.models.derived.EquipamentoETipo;
import br.coop.eita.castanhadora.db.models.derived.UpfIntegranteTrabalha;

@Dao
public abstract class UpfIntegranteDao extends BaseIdentifiableDao<UpfIntegrante> {

    @Query("SELECT * FROM " + UpfIntegrante.TABLE_NAME + " WHERE safra_id = :safraId")
    public abstract LiveData<List<UpfIntegrante>> getIntegrantesLive(Integer safraId);

    @Query("SELECT * FROM " + UpfIntegrante.TABLE_NAME + " WHERE safra_id = :safraId")
    public abstract List<UpfIntegrante> getIntegrantes(Integer safraId);

    @Query("SELECT i.*, CASE WHEN ti.upf_integrante_id IS NULL THEN 0 ELSE 1 END as trabalha FROM upf_integrante i LEFT JOIN trabalho_upf_integrante ti on ti.upf_integrante_id = i.id AND ti.trabalho_upf_id = :trabalhoUpfId WHERE i.safra_id = :safraId")
    public abstract LiveData<List<UpfIntegranteTrabalha>> getIntegrantesETrabalho(Integer trabalhoUpfId, Integer safraId);

    @Query("SELECT i.*, 0 as trabalha FROM upf_integrante i WHERE i.safra_id = :safraId")
    public abstract LiveData<List<UpfIntegranteTrabalha>> getIntegrantesETrabalho(Integer safraId);

    @Query("SELECT COUNT(*) FROM "+ UpfIntegrante.TABLE_NAME + " WHERE safra_id = :safraId")
    public abstract Integer getCountUpfIntegrantes(Integer safraId);

    @Query("SELECT * FROM " + UpfIntegrante.TABLE_NAME + " WHERE main_integrante = 1 and safra_id = :safraId")
    public abstract UpfIntegrante getMainIntegrante(Integer safraId);

}
