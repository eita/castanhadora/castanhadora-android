package br.coop.eita.castanhadora.db.models;

import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.databinding.Bindable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

import java.util.Date;

import br.coop.eita.castanhadora.BR;

@Entity(tableName = Upf.TABLE_NAME)
public class Upf extends BaseIdentifiableModel {
    public static final String TABLE_NAME = "upf";

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "nome_comunidade")
    private String nomeComunidade;

    @ColumnInfo(name = "nome_usuario")
    private String nomeUsuario;

    @ColumnInfo(name = "data_nascimento_usuario")
    private Date dataNascimentoUsuario;

    @ColumnInfo(name = "genero_usuario")
    private String generoUsuario;

    @ColumnInfo(name = "unidade_area")
    public String unidadeArea;

    @ColumnInfo(name = "unidade_valor")
    public String unidadeValor;

    @ColumnInfo(name = "cooperativa")
    public String cooperativa;

    @ColumnInfo(name = "compartilhar_safra")
    public Boolean compartilharSafra;

    public Upf() {
        compartilharSafra = false;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNomeComunidade() {
        return nomeComunidade;
    }

    public void setNomeComunidade(String nomeComunidade) {
        this.nomeComunidade = nomeComunidade;
    }

    @Bindable
    public String getNomeUsuario() {
        return nomeUsuario;
    }

    public void setNomeUsuario(String nomeUsuario) {
        this.nomeUsuario = nomeUsuario;
        notifyPropertyChanged(BR.nomeUsuario);
    }

    public String getCooperativa() {
        return cooperativa;
    }

    public void setCooperativa(String cooperativa) {
        this.cooperativa = cooperativa;
    }

    public Date getDataNascimentoUsuario() {
        return dataNascimentoUsuario;
    }

    public void setDataNascimentoUsuario(Date dataNascimentoUsuario) {
        this.dataNascimentoUsuario = dataNascimentoUsuario;
    }

    @NonNull
    @Bindable
    public String getGeneroUsuario() {
        return generoUsuario;
    }

    public void setGeneroUsuario(String generoUsuario) {
        this.generoUsuario = generoUsuario;
        notifyPropertyChanged(BR.generoUsuario);
    }

    public String getUnidadeArea() {
        return unidadeArea;
    }

    public void setUnidadeArea(String unidadeArea) {
        this.unidadeArea = unidadeArea;
    }

    public String getUnidadeValor() {
        return unidadeValor;
    }

    public void setUnidadeValor(String unidadeValor) {
        this.unidadeValor = unidadeValor;
    }

    public Boolean dadosUsuarioCompletos() {
        return dataNascimentoUsuario != null && !TextUtils.isEmpty(nomeUsuario);
    }

    public Boolean unidadesCompletas() {
        return !TextUtils.isEmpty(unidadeArea) && !TextUtils.isEmpty(unidadeValor);
    }

    public Boolean getCompartilharSafra() {
        return compartilharSafra;
    }

    public void setCompartilharSafra(Boolean compartilharSafra) {
        this.compartilharSafra = compartilharSafra;
    }
}
