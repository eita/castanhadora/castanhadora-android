package br.coop.eita.castanhadora.db.models.derived;

import androidx.room.Embedded;

import br.coop.eita.castanhadora.db.models.UpfIntegrante;

public class UpfIntegranteTrabalha {
    @Embedded
    private UpfIntegrante upfIntegrante;

    private Boolean trabalha;

    public UpfIntegrante getUpfIntegrante() {
        return upfIntegrante;
    }

    public void setUpfIntegrante(UpfIntegrante upfIntegrante) {
        this.upfIntegrante = upfIntegrante;
    }

    public Boolean getTrabalha() {
        return trabalha;
    }

    public void setTrabalha(Boolean trabalha) {
        this.trabalha = trabalha;
    }
}
