package br.coop.eita.castanhadora.db.models;

import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.Bindable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.coop.eita.castanhadora.BR;
import br.coop.eita.castanhadora.Castanhadora;
import br.coop.eita.castanhadora.R;

import static androidx.room.ForeignKey.CASCADE;


@Entity(tableName = UpfIntegrante.TABLE_NAME, foreignKeys = @ForeignKey(onDelete = CASCADE, entity = Safra.class, parentColumns = "id", childColumns = "safra_id"))
public class UpfIntegrante extends BaseIdentifiableModel {
    public static final String TABLE_NAME = "upf_integrante";

    @PrimaryKey(autoGenerate = true)
    private int id;

    @NonNull
    @ColumnInfo(name = "nome")
    private String nome;

    @NonNull
    @ColumnInfo(name = "data_nascimento")
    private Date dataNascimento;

    @ColumnInfo(name = "safra_id", index = true)
    public int safraId;

    @ColumnInfo(name = "main_integrante")
    public boolean mainIntegrante;

    @ColumnInfo(name = "parentesco")
    public String parentesco;

    @ColumnInfo(name = "genero")
    public String genero;

    public UpfIntegrante() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @NonNull
    @Bindable
    public String getNome() {
        return nome;
    }

    public void setNome(@NonNull String nome) {
        this.nome = nome;
        notifyPropertyChanged(BR.nome);
        Log.i("TEST", "changed integranteupf nome");
    }

    @NonNull
    @Bindable
    public Date getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(@NonNull Date dataNascimento) {
        this.dataNascimento = dataNascimento;
        notifyPropertyChanged(BR.dataNascimento);
    }

    public int getSafraId() {
        return safraId;
    }

    public void setSafraId(int safraId) {
        this.safraId = safraId;
    }

    public boolean isMainIntegrante() {
        return mainIntegrante;
    }

    public void setMainIntegrante(boolean mainIntegrante) {
        this.mainIntegrante = mainIntegrante;
    }

    @NonNull
    @Bindable
    public String getParentesco() {
        return parentesco;
    }

    public void setParentesco(@NonNull String parentesco) {
        this.parentesco = parentesco;
        notifyPropertyChanged(BR.parentesco);
    }

    @NonNull
    @Bindable
    public String getGenero() {
        return genero;
    }

    public void setGenero(@NonNull String genero) {
        this.genero = genero;
        notifyPropertyChanged(BR.genero);
    }

    @Override
    public List<String> validate() {
        ArrayList<String> validationErrors = new ArrayList<String>();
        if (TextUtils.isEmpty(this.nome)) {
            validationErrors.add(Castanhadora.APP.getResources().getString(
                    R.string.error_val_upf_integrante_nome_empty));
        }
        if (this.dataNascimento == null) {
            validationErrors.add(Castanhadora.APP.getResources().getString(
                    R.string.error_val_upf_integrante_data_nascimento_empty));
        }
        return validationErrors;
    }

    public UpfIntegrante duplicar(Integer novaSafraId) {
        UpfIntegrante novo = new UpfIntegrante();
        novo.setSafraId(novaSafraId);
        novo.setMainIntegrante(mainIntegrante);
        novo.setNome(nome);
        novo.setDataNascimento(dataNascimento);
        novo.setParentesco(parentesco);
        novo.setGenero(genero);
        return novo;
    }
}
