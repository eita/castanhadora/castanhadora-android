package br.coop.eita.castanhadora.ui.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import java.util.List;

import br.coop.eita.castanhadora.R;
import br.coop.eita.castanhadora.databinding.ActivityEquipamentosListBinding;
import br.coop.eita.castanhadora.db.models.Equipamento;
import br.coop.eita.castanhadora.db.models.Safra;
import br.coop.eita.castanhadora.ui.adapters.EquipamentoAdapter;
import br.coop.eita.castanhadora.viewmodels.EquipamentoViewModel;

public class EquipamentosListActivity extends AppCompatActivity {

    public static final int CRIAR_EQUIPAMENTO_ID_REPLY = 1;
    public static final int EDITAR_EQUIPAMENTO_ID_REPLY = 2;    

    private EquipamentoViewModel viewModel;

    private EquipamentoAdapter equipamentoAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final ActivityEquipamentosListBinding binding = DataBindingUtil.setContentView(this,
                R.layout.activity_equipamentos_list);

        viewModel = ViewModelProviders.of(this).get(EquipamentoViewModel.class);
        binding.setViewmodel(viewModel);

        //LiveData<List<EquipamentoETipo>> eqtipos = viewModel.getEquipamentosETipos();
        LiveData<List<Equipamento>> equipamentos = viewModel.getEquipamentosLive(viewModel.getSafraAtiva().getId());

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(R.string.equipamentos);
        }

        final EquipamentosListActivity self = this;

        Safra safra  = viewModel.getSafraAtiva();
        final Integer ano = safra.getAno();

        equipamentos.observe(this, new Observer<List<Equipamento>>() {
            @Override
            public void onChanged(List<Equipamento> eqs) {
                if (eqs != null) {
                    if (eqs.size() == 0) {
                        Toast.makeText(self, R.string.list_sem_equipamentos, Toast.LENGTH_LONG).show();
                        finish();
                    }
                    equipamentoAdapter = new EquipamentoAdapter(getApplicationContext(), eqs);
                    binding.equipamentoList.setAdapter(equipamentoAdapter);
                    equipamentoAdapter.setActivity(self);
                    equipamentoAdapter.notifyDataSetChanged();
                }
            }
        });
    }

    public void openNewEquipamentoForm(View view) {
        Intent intent = new Intent(EquipamentosListActivity.this, EquipamentoFormActivity.class);
        startActivityForResult(intent, CRIAR_EQUIPAMENTO_ID_REPLY);
    }

    public void openEditEquipamentoForm(Equipamento equipamento) {
        Intent intent = new Intent(EquipamentosListActivity.this, EquipamentoFormActivity.class).putExtra(
                "equipamento_id", equipamento.getId());
        startActivityForResult(intent, EDITAR_EQUIPAMENTO_ID_REPLY);
    }

    public void openDeleteEquipamentoDialog(final Equipamento eqtipo) {
        new AlertDialog.Builder(this)
                .setMessage(getResources().getString(R.string.msg_dialog_equipamento_remover,eqtipo.getNomeCompleto()))
                .setPositiveButton(R.string.dialog_sim, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        viewModel.deleteEquipamento(eqtipo);
                    }
                })
                .setNegativeButton(R.string.dialog_nao, null)
                .setTitle(R.string.label_dialog_equipamento_delete_title)
                .show();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent replyIntent = new Intent();
                setResult(RESULT_CANCELED, replyIntent);
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


}
