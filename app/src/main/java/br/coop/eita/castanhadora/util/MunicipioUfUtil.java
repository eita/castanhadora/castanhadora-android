package br.coop.eita.castanhadora.util;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public class MunicipioUfUtil {


    public static HashMap<String, Integer> extractEstadosMapFromJson(Context context) {
        String json;
        JSONObject jsonObj;
        HashMap<String, Integer> estadosMap = new HashMap<>();

        try {
            InputStream is = context.getAssets().open("estados.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
            jsonObj = new JSONObject(json);

            JSONArray m_jArry = jsonObj.getJSONArray("estados");

            for (int i = 0; i < m_jArry.length(); i++) {
                JSONObject jo_inside = m_jArry.getJSONObject(i);
                String nome = jo_inside.getString("nome");
                Integer codigo_uf = jo_inside.getInt("codigo_uf");
                estadosMap.put(nome, codigo_uf);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        } catch (JSONException ex) {
            ex.printStackTrace();
            return null;
        }
        return estadosMap;
    }

    public static HashMap<Integer, HashMap<String, Integer>> extractMunicipiosMapFromJson(Context context) {
        String json;
        JSONObject jsonObj;
        HashMap<Integer, HashMap<String, Integer>> municipiosPorUfMap = new HashMap<>();

        try {
            InputStream is = context.getAssets().open("municipios.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
            jsonObj = new JSONObject(json);

            JSONArray m_jArry = jsonObj.getJSONArray("municipios");

            for (int i = 0; i < m_jArry.length(); i++) {
                JSONObject jo_inside = m_jArry.getJSONObject(i);
                Integer codigo_uf = jo_inside.getInt("codigo_uf");
                String nome = jo_inside.getString("nome");
                Integer codigo_ibge = jo_inside.getInt("codigo_ibge");

                if (municipiosPorUfMap.containsKey(codigo_uf)) {
                    municipiosPorUfMap.get(codigo_uf).put(nome, codigo_ibge);
                } else {
                    HashMap<String, Integer> tempMunicipioMap = new HashMap<>();
                    tempMunicipioMap.put(nome, codigo_ibge);
                    municipiosPorUfMap.put(codigo_uf, tempMunicipioMap);
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        } catch (JSONException ex) {
            ex.printStackTrace();
            return null;
        }
        return municipiosPorUfMap;
    }

    public static void sortWithoutAccent(ArrayList<String> strs){
        Collections.sort(strs, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                o1 = Normalizer.normalize(o1, Normalizer.Form.NFD);
                o2 = Normalizer.normalize(o2, Normalizer.Form.NFD);
                return o1.compareTo(o2);
            }
        });
    }

    public static <T, E> T getKeyByValue(Map<T, E> map, E value) {
        for (Map.Entry<T, E> entry : map.entrySet()) {
            if (value.equals(entry.getValue())) {
                return entry.getKey();
            }
        }
        return null;
    }
}
