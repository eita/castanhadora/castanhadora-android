package br.coop.eita.castanhadora.ui.activities;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import br.coop.eita.castanhadora.R;
import br.coop.eita.castanhadora.databinding.ActivityReportBasicBinding;
import br.coop.eita.castanhadora.databinding.ActivitySafraCastanhalFormBinding;
import br.coop.eita.castanhadora.db.models.Safra;
import br.coop.eita.castanhadora.viewmodels.ReportViewModel;
import br.coop.eita.castanhadora.viewmodels.SafraViewModel;

public class ReportBasicActivity extends AppCompatActivity {

    private ReportViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        ActivityReportBasicBinding binding = DataBindingUtil.setContentView(this,
                R.layout.activity_report_basic);

        viewModel = ViewModelProviders.of(this).get(ReportViewModel.class);
        binding.setViewmodel(viewModel);

        Integer safraId = getIntent().getIntExtra("safra_id", 0);

        Safra safra = viewModel.loadSafra(safraId);

        actionBar.setTitle(R.string.basic_report_title);


    }
}
