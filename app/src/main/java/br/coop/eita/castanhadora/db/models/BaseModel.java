package br.coop.eita.castanhadora.db.models;

import androidx.databinding.BaseObservable;

import java.util.ArrayList;
import java.util.List;


public class BaseModel extends BaseObservable {

    /**
     * Validates errors and return list with errors
     * @return list with errors
     */
    public List<String> validate() {
        return new ArrayList<String>();
    }
}
