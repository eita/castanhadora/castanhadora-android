package br.coop.eita.castanhadora.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import java.math.BigDecimal;
import java.util.List;

import br.coop.eita.castanhadora.R;
import br.coop.eita.castanhadora.databinding.ActivitySafraRemuneracaoDesejadaBinding;
import br.coop.eita.castanhadora.db.DataRepository;
import br.coop.eita.castanhadora.db.models.Safra;
import br.coop.eita.castanhadora.ui.util.Wizardable;
import br.coop.eita.castanhadora.viewmodels.SafraViewModel;

public class SafraRemuneracaoDesejadaActivity extends AppCompatActivity implements Wizardable {

    public static final String SAFRA_ID_REPLY = "br.coop.eita.castanhadora.safra_id.REPLY";

    private SafraViewModel viewModel;

    private Boolean isWizard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        isWizard = getIntent().getBooleanExtra("wizard", false);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null && !isWizard) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        ActivitySafraRemuneracaoDesejadaBinding binding = DataBindingUtil.setContentView(this,
                R.layout.activity_safra_remuneracao_desejada);

        viewModel = ViewModelProviders.of(this).get(SafraViewModel.class);
        binding.setViewmodel(viewModel);

        Safra safra = viewModel.loadSafraAtiva();

        actionBar.setTitle(R.string.label_remuneracao_desejada);

        binding.setWizard(isWizard);

        binding.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SafraRemuneracaoDesejadaActivity.this, VideoTutorialActivity.class);
                intent.putExtra("videoPath", R.raw.meta_remuneracao);
                intent.putExtra("videoTitle", R.string.safra_totais_remuneracao_almejada);
                startActivity(intent);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!isWizard) {
            getMenuInflater().inflate(R.menu.menu_form, menu);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                viewModel.reloadSafraAtiva();
                Intent replyIntent = new Intent();
                setResult(RESULT_CANCELED, replyIntent);
                finish();
                return true;
            case R.id.action_save:
                final SafraRemuneracaoDesejadaActivity self = this;
                saveSafra(new AfterSaveListener() {
                    @Override
                    public void doAfterSave(Integer id) {
                        Intent replyIntent2 = new Intent();
                        replyIntent2.putExtra(SAFRA_ID_REPLY, id);
                        setResult(RESULT_OK, replyIntent2);
                        Toast.makeText(self, R.string.saved_remuneracao_almejada, Toast.LENGTH_LONG).show();
                        finish();
                    }
                });

                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private interface AfterSaveListener {
        void doAfterSave(Integer id);
    }

    @Override
    public void onClickWizardPrevious(View view) {
        Intent intent = new Intent(this, SafraCastanhalFormActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        intent.putExtra("wizard", true);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        finish();
    }

    @Override
    public void onClickWizardNext(View view) {
        final SafraRemuneracaoDesejadaActivity self = this;
        saveSafra(new AfterSaveListener() {
            @Override
            public void doAfterSave(Integer id) {
                Intent intent = new Intent(self, PermissaoCompartilharNuvemActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                intent.putExtra("wizard", true);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                finish();
            }
        });
    }

    public void saveSafra(final AfterSaveListener listener) {
        final SafraRemuneracaoDesejadaActivity self = this;

        viewModel.saveSafra(new DataRepository.OnSaveEntityListener() {
            @Override
            public void onSuccess(Integer... ids) {
                listener.doAfterSave(ids[0]);
            }

            @Override
            public void onFailure(int errorCode, List<String> errorMessages) {
                Toast.makeText(self, errorMessages.get(0), Toast.LENGTH_LONG).show();
            }
        });

    }
}
