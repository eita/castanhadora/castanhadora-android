package br.coop.eita.castanhadora.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.Bindable;
import androidx.lifecycle.LiveData;

import java.math.BigDecimal;
import java.util.List;

import br.coop.eita.castanhadora.Castanhadora;
import br.coop.eita.castanhadora.R;
import br.coop.eita.castanhadora.db.DataRepository;
import br.coop.eita.castanhadora.db.models.CategoriaDespesa;
import br.coop.eita.castanhadora.db.models.Despesa;
import br.coop.eita.castanhadora.db.models.Safra;

public class DespesaViewModel extends BaseViewModel {

    private Despesa currentDespesa;

    private Integer currentDespesaId;

    private LiveData<List<CategoriaDespesa>> categoriasDespesa;

    public DespesaViewModel(@NonNull Application application) {
        super(application);
    }

    public Safra getSafraAtiva() {
        return dataRepository.getSafraAtiva();
    }

    public LiveData<List<CategoriaDespesa>> getCategoriasDespesa() {
        if (categoriasDespesa == null) {
            categoriasDespesa = dataRepository.getCategoriasDespesa();
        }
        return categoriasDespesa;
    }

    @Bindable
    public Despesa getCurrentDespesa() {
        return currentDespesa;
    }

    public void setCurrentDespesa(Despesa currentDespesa) {
        this.currentDespesa = currentDespesa;
        currentDespesaId = currentDespesa != null ? currentDespesa.getId() : null;
    }

    public Despesa loadCurrentDespesa(Integer despesaId) {
        if (despesaId != currentDespesaId) {
            if (despesaId != null && despesaId > 0) {
                currentDespesa = dataRepository.load(Despesa.class, despesaId);
            }
            currentDespesaId = despesaId;
        }
        return currentDespesa;
    }

    public Despesa newDespesa(String tipo) {
        currentDespesa = dataRepository.newDespesa(tipo);
        return currentDespesa;
    }

    public void saveCurrentDespesa(DataRepository.OnSaveEntityListener listener) {
        dataRepository.save(currentDespesa, listener);
    }

    public void deleteDespesa(Despesa despesa) {
        dataRepository.delete(despesa);
    }

    public LiveData<List<Despesa>> getDespesas() {
        return dataRepository.getDespesas();
    }

    public LiveData<List<Despesa>> getDespesasBySafraIdLive(Integer safraId) {
        return dataRepository.getDespesasBySafraIdLive(safraId);
    }

    public List<Despesa> getDespesasBySafraId(Integer safraId) {
        return dataRepository.getDespesasBySafraId(safraId);
    }

    public BigDecimal getTotalDespesaParaTipo(String tipo) {
        return dataRepository.getTotalDespesaParaTipo(tipo);
    }

    public String getIntroText() {
        String labelDespesa = Castanhadora.APP.getResources().getString(Castanhadora.APP.getResources().getIdentifier("label_despesa_" + currentDespesa.getTipo(), "string", Castanhadora.APP.getPackageName()));
        return Castanhadora.APP.getResources().getString(R.string.intro_despesa_generico, labelDespesa.toLowerCase());
    }

    public String getHintValue() {
        return Castanhadora.APP.getResources().getString(Castanhadora.APP.getResources().getIdentifier("hint_despesa_valor_" + currentDespesa.getTipo(), "string", Castanhadora.APP.getPackageName()));
    }
}
