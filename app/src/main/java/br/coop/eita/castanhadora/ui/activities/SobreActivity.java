package br.coop.eita.castanhadora.ui.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import br.coop.eita.castanhadora.R;
import br.coop.eita.castanhadora.databinding.ActivitySobreBinding;
import br.coop.eita.castanhadora.databinding.ActivityUnidadeFormBinding;
import br.coop.eita.castanhadora.ui.fragments.SelectTipoLancamentoDialogFragment;

public class SobreActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivitySobreBinding binding = DataBindingUtil.setContentView(this,
                R.layout.activity_sobre);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        setTitle(getResources().getString(R.string.action_sobre));
        /*Date buildDate = BuildConfig.BUILD_TIME;
        binding.corpo6.setText("Versão Beta-2.2 (15) lançada em "+buildDate.toString());*/
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void abreCaderno(View view) {
        String url = "https://app.rios.org.br/index.php/s/94c4csTnLyRcz8T";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }

    public void abreVideoSemearIntro(View view) {
        Intent intent = new Intent(SobreActivity.this, VideoTutorialActivity.class);
        intent.putExtra("videoPath", R.raw.semear_video_intro);
        intent.putExtra("videoTitle", R.string.video_title_rede_semear);
        startActivity(intent);
    }
}
