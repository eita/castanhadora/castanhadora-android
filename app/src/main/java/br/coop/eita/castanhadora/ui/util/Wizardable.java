package br.coop.eita.castanhadora.ui.util;

import android.view.View;

public interface Wizardable {

    void onClickWizardPrevious(View view);
    void onClickWizardNext(View view);

}
