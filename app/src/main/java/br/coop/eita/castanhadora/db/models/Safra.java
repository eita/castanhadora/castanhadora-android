package br.coop.eita.castanhadora.db.models;

import android.content.Context;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.databinding.Bindable;
import androidx.databinding.library.baseAdapters.BR;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import org.w3c.dom.Text;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.UUID;

import br.coop.eita.castanhadora.Castanhadora;
import br.coop.eita.castanhadora.R;

@Entity(tableName = Safra.TABLE_NAME, foreignKeys = @ForeignKey(entity = Upf.class, parentColumns = "id", childColumns = "upf_id"), indices = {@Index(value = {"nome"}, unique = true)})
public class Safra extends BaseIdentifiableModel {
    public static final String TABLE_NAME = "safra";
    public static final int RESULT_SAFRA_ENCERROU = 1001;
    public static final int RESULT_SAFRA_ATIVA_ALTERADA = 1002;
    public static final int RESULT_SAFRA_ATIVA_APAGADA = 1003;


    @PrimaryKey(autoGenerate = true)
    private int id;

    @NonNull
    @ColumnInfo(name = "nome")
    private String nome;

    @NonNull
    @ColumnInfo(name = "data_inicio")
    private Date dataInicio;

    @ColumnInfo(name = "data_final")
    private Date dataFinal;

    @ColumnInfo(name = "ativa")
    private Boolean ativa;

    @ColumnInfo(name = "remuneracao_desejada_por_dia")
    private BigDecimal remuneracaoDesejadaPorDia = BigDecimal.ZERO;

    @ColumnInfo(name = "castanhal_tamanho")
    private BigDecimal castanhalTamanho;

    @ColumnInfo(name = "castanhal_numero_castanheiras")
    private Integer castanhalNumeroCastanheiras;

    @ColumnInfo(name = "castanhal_numero_familias")
    private Integer castanhalNumeroFamilias;

    @ColumnInfo(name = "upf_id", index = true)
    public int upfId;

    @ColumnInfo(name = "municipio_id", index = true)
    public Integer municipioId;

    @ColumnInfo(name = "uf_id", index = true)
    public Integer ufId;

    @ColumnInfo(name = "simulacao", index = true)
    public Boolean simulacao;

    public Safra() {
        simulacao = false;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @NonNull
    @Bindable
    public String getNome() {
        return nome;
    }

    public void setNome(@NonNull String nome) {
        this.nome = nome;
        notifyPropertyChanged(BR.nome);
    }

    @NonNull
    @Bindable
    public Date getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(@NonNull Date dataInicio) {
        this.dataInicio = dataInicio;
        notifyPropertyChanged(BR.dataInicio);
        if (TextUtils.isEmpty(nome) && dataInicio != null) {
            Calendar cal = GregorianCalendar.getInstance();
            cal.setTime(dataInicio);
            setNome(Integer.toString(cal.get(Calendar.YEAR)));
        }
    }

    @Bindable
    public Date getDataFinal() {
        return dataFinal;
    }

    public void setDataFinal(Date dataFinal) {
        this.dataFinal = dataFinal;
        Log.i("Teste", "Alterada data final" + "");
        notifyPropertyChanged(BR.dataFinal);
    }

    public BigDecimal getRemuneracaoDesejadaPorDia() {
        return remuneracaoDesejadaPorDia;
    }

    public void setRemuneracaoDesejadaPorDia(BigDecimal remuneracaoDesejadaPorDia) {
        this.remuneracaoDesejadaPorDia = remuneracaoDesejadaPorDia;
    }

    public BigDecimal getCastanhalTamanho() {
        return castanhalTamanho;
    }

    public void setCastanhalTamanho(BigDecimal castanhalTamanho) {
        this.castanhalTamanho = castanhalTamanho;
    }

    public Integer getCastanhalNumeroCastanheiras() {
        return castanhalNumeroCastanheiras;
    }

    public void setCastanhalNumeroCastanheiras(Integer castanhalNumeroCastanheiras) {
        this.castanhalNumeroCastanheiras = castanhalNumeroCastanheiras;
    }


    public Integer getCastanhalNumeroFamilias() {
        return castanhalNumeroFamilias;
    }

    public void setCastanhalNumeroFamilias(Integer castanhalNumeroFamilias) {
        this.castanhalNumeroFamilias = castanhalNumeroFamilias;
    }

    public int getUpfId() {
        return upfId;
    }

    public void setUpfId(int upfId) {
        this.upfId = upfId;
    }

    public Boolean getAtiva() {
        return ativa;
    }

    public void setAtiva(Boolean ativa) {
        this.ativa = ativa;
    }

    public Integer getMunicipioId() {
        return municipioId;
    }

    public void setMunicipioId(Integer municipioId) {
        this.municipioId = municipioId;
    }

    public Integer getUfId() {
        return ufId;
    }

    public void setUfId(Integer ufId) {
        this.ufId = ufId;
    }

    public Boolean getSimulacao() {
        return simulacao;
    }

    public void setSimulacao(Boolean simulacao) {
        this.simulacao = simulacao;
    }

    public void mudarNome() {
        String str = UUID.randomUUID().toString();
        this.setNome(str);
        Log.i("TEST", "nome alterado para:" + str);
    }

    @Override
    public List<String> validate() {
        ArrayList<String> validationErrors = new ArrayList<String>();
        if (TextUtils.isEmpty(this.nome)) {
            validationErrors.add(
                    Castanhadora.APP.getResources().getString(R.string.error_val_safra_nome_empty));
        }
        if (this.dataInicio == null) {
            validationErrors.add(Castanhadora.APP.getResources().getString(
                    R.string.error_val_safra_data_inicio_empty));
        } else if (this.dataFinal != null && dataFinal.before(dataInicio)) {
            validationErrors.add(Castanhadora.APP.getResources().getString(
                    R.string.error_val_safra_data_final_menor));
        }
        return validationErrors;
    }

    public Integer getAno() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(getDataInicio());
        return cal.get(Calendar.YEAR);
    }

    public Boolean castanhalCompleto() {
        return castanhalTamanho != null && castanhalTamanho.floatValue() > 0 &&
                castanhalNumeroCastanheiras != null && castanhalNumeroCastanheiras > 0 &&
                castanhalNumeroFamilias != null && castanhalNumeroFamilias > 0;
    }

    @Bindable
    public Integer getPeriodo() {
        Calendar hoje = new GregorianCalendar();
        Calendar inicio = new GregorianCalendar();
        hoje.setTime(new Date());
        inicio.setTime(dataInicio);

        int yearsInBetween = hoje.get(Calendar.YEAR) - inicio.get(Calendar.YEAR);
        int monthsDiff = hoje.get(Calendar.MONTH) - inicio.get(Calendar.MONTH);
        return (yearsInBetween * 12) + monthsDiff;
    }


    public Safra duplicar(String nome, Date dataInicio, Date dataFinal) {
        Safra nova = new Safra();
        nova.setCastanhalNumeroFamilias(castanhalNumeroFamilias);
        nova.setCastanhalNumeroCastanheiras(castanhalNumeroCastanheiras);
        nova.setCastanhalTamanho(castanhalTamanho);
        nova.setNome(nome);
        nova.setDataInicio(dataInicio);
        nova.setDataFinal(dataFinal);
        nova.setRemuneracaoDesejadaPorDia(remuneracaoDesejadaPorDia);
        nova.setUpfId(upfId);
        return nova;
    }

    @Override
    public String toString() {
        return nome;
    }

    public String getTituloCard() {
        Context context = Castanhadora.APP.getApplicationContext();
        return context.getResources().getString(R.string.safra_inicio_titulo, nome);
    }

    public String getSubtituloCard() {
        Context context = Castanhadora.APP.getApplicationContext();
        String subTituloCard;
        String dataInicio = DateUtils.formatDateTime(Castanhadora.APP, getDataInicio().getTime(),
                DateUtils.FORMAT_SHOW_YEAR | DateUtils.FORMAT_NO_MONTH_DAY);
        if (getDataFinal() == null) {
            subTituloCard = context.getString(R.string.label_safra_em_andamento, dataInicio,
                    getPeriodo().toString());
        } else {
            String dataFinal = DateUtils.formatDateTime(Castanhadora.APP, getDataFinal().getTime(),
                    DateUtils.FORMAT_SHOW_YEAR | DateUtils.FORMAT_NO_MONTH_DAY);
            if (getDataFinal().after(Calendar.getInstance().getTime())) {
                subTituloCard = context.getString(R.string.label_safra_em_andamento_com_data_final,
                        dataInicio, getPeriodo().toString(), dataFinal);
            } else {
                Calendar cal = Calendar.getInstance();
                cal.setTime(getDataInicio());
                Integer anoInicio = cal.get(Calendar.YEAR);
                String mesInicio = DateUtils.formatDateTime(Castanhadora.APP,
                        getDataInicio().getTime(),
                        DateUtils.FORMAT_NO_YEAR | DateUtils.FORMAT_NO_MONTH_DAY);
                cal.setTime(getDataFinal());
                Integer anoFinal = cal.get(Calendar.YEAR);
                String mesFinal = DateUtils.formatDateTime(Castanhadora.APP,
                        getDataFinal().getTime(),
                        DateUtils.FORMAT_NO_YEAR | DateUtils.FORMAT_NO_MONTH_DAY);

                if (anoInicio == anoFinal) {
                    subTituloCard = context.getString(R.string.label_safra_encerrada_mesmo_ano,
                            mesInicio, mesFinal, anoInicio.toString());
                } else {
                    subTituloCard = context.getString(R.string.label_safra_encerrada_outro_ano,
                            dataInicio, dataFinal);
                }
            }
        }
        return subTituloCard;
    }

    public Boolean necessitaWizard() {
        // TODO: entender o que se queria com isso...
        return false;
        // return remuneracaoDesejadaPorDia == null || remuneracaoDesejadaPorDia.floatValue() < 0.01;
    }
}
