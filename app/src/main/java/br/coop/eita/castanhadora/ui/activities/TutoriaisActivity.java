package br.coop.eita.castanhadora.ui.activities;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import br.coop.eita.castanhadora.R;
import br.coop.eita.castanhadora.databinding.ActivityTutoriaisBinding;

public class TutoriaisActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(R.string.label_videos_tutoriais);
        }

        String[] titulos = getResources().getStringArray(R.array.titulos_videos_tutoriais);
        String[] subtitulos = getResources().getStringArray(R.array.subtitulos_videos_tutoriais);

        ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_2, android.R.id.text1, titulos) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                TextView text1 = (TextView) view.findViewById(android.R.id.text1);
                TextView text2 = (TextView) view.findViewById(android.R.id.text2);

                text1.setText(titulos[position]);
                text2.setText(subtitulos[position]);

                text2.setTextColor(Color.GRAY);
                return view;
            }
        };



        final ActivityTutoriaisBinding binding = DataBindingUtil.setContentView(this,
                R.layout.activity_tutoriais);
        binding.videosList.setAdapter( adapter );

        binding.videosList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                switch (position) {
                    case 0:
                        openVideoTutorialDespesasActivity(view);
                        break;
                    case 1:
                        openVideoTutorialEquipamentoActivity(view);
                        break;
                    case 2:
                        openVideoTutorialProducaoActivity(view);
                        break;
                    case 3:
                        openVideoTutorialMetaRemuneracaoActivity(view);
                        break;
                    case 4:
                        openVideoTutorialTrabalhoFamiliaActivity(view);
                        break;
                    case 5:
                        openVideoTutorialDiariasActivity(view);
                        break;
                    case 6:
                        openVideoTutorialEncerrarSafraActivity(view);
                        break;
                    case 7:
                        openYoutubeLink(view);
                        break;
                    case 8:
                        openVideoIntroRedeSemeara(view);
                        break;
                }
            }
        });
    }

    public void openVideoTutorialDiariasActivity(View view) {
        Intent intent = new Intent(TutoriaisActivity.this, VideoTutorialActivity.class);
        intent.putExtra("videoPath", R.raw.diarias);
        intent.putExtra("videoTitle", R.string.label_despesa_diarias);
        startActivity(intent);
    }

    public void openVideoTutorialEncerrarSafraActivity(View view) {
        Intent intent = new Intent(TutoriaisActivity.this, VideoTutorialActivity.class);
        intent.putExtra("videoPath", R.raw.encerrar_safra);
        intent.putExtra("videoTitle", R.string.safra_encerrar);
        startActivity(intent);
    }

    public void openVideoTutorialMetaRemuneracaoActivity(View view) {
        Intent intent = new Intent(TutoriaisActivity.this, VideoTutorialActivity.class);
        intent.putExtra("videoPath", R.raw.meta_remuneracao);
        intent.putExtra("videoTitle", R.string.safra_totais_remuneracao_almejada);
        startActivity(intent);
    }

    public void openVideoTutorialProducaoActivity(View view) {
        Intent intent = new Intent(TutoriaisActivity.this, VideoTutorialActivity.class);
        intent.putExtra("videoPath",  R.raw.producao);
        intent.putExtra("videoTitle", R.string.safra_totais_producao);
        startActivity(intent);
    }

    public void openVideoTutorialDespesasActivity(View view) {
        Intent intent = new Intent(TutoriaisActivity.this, VideoTutorialActivity.class);
        intent.putExtra("videoPath", R.raw.despesas);
        intent.putExtra("videoTitle", R.string.safra_totais_despesas);
        startActivity(intent);
    }

    public void openVideoTutorialEquipamentoActivity(View view) {
        Intent intent = new Intent(TutoriaisActivity.this, VideoTutorialActivity.class);
        intent.putExtra("videoPath", R.raw.equipamento);
        intent.putExtra("videoTitle", R.string.label_equipamento_equipamento);
        startActivity(intent);
    }

    public void openVideoTutorialTrabalhoFamiliaActivity(View view) {
        Intent intent = new Intent(TutoriaisActivity.this, VideoTutorialActivity.class);
        intent.putExtra("videoPath", R.raw.trabalho_familia);
        intent.putExtra("videoTitle", R.string.label_trabalho_upf_new_title);
        startActivity(intent);
    }

    public void openVideoIntroRedeSemeara(View view) {
        Intent intent = new Intent(TutoriaisActivity.this, VideoTutorialActivity.class);
        intent.putExtra("videoPath", R.raw.semear_video_intro);
        intent.putExtra("videoTitle", R.string.video_title_rede_semear);
        startActivity(intent);
    }

    public void openYoutubeLink(View view) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/watch?v=TTHCUlm4j9k"));
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(this, SafraInicioActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
        }
        return super.onOptionsItemSelected(item);
    }

}
