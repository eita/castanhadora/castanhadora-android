package br.coop.eita.castanhadora.ui.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.List;

import br.coop.eita.castanhadora.R;
import br.coop.eita.castanhadora.databinding.ActivitySafraVendasListBinding;
import br.coop.eita.castanhadora.db.models.Venda;
import br.coop.eita.castanhadora.ui.adapters.VendaAdapter;
import br.coop.eita.castanhadora.viewmodels.VendaViewModel;

public class SafraVendasListActivity extends AppCompatActivity {

    public static final int EDITAR_VENDA_ID_REPLY = 1;

    private VendaViewModel viewModel;

    private VendaAdapter vendaAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final ActivitySafraVendasListBinding binding = DataBindingUtil.setContentView(this,
                R.layout.activity_safra_vendas_list);

        viewModel = ViewModelProviders.of(this).get(VendaViewModel.class);
        binding.setViewmodel(viewModel);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(R.string.label_vendas);
        }

        LiveData<List<Venda>> vendas = viewModel.getVendas();

        final SafraVendasListActivity self = this;

        vendas.observe(this, new Observer<List<Venda>>() {
            @Override
            public void onChanged(List<Venda> vends) {
                if (vends != null) {
                    if (vends.size() == 0) {
                        Toast.makeText(self, R.string.list_sem_venda, Toast.LENGTH_LONG).show();
                        finish();
                    }
                    vendaAdapter = new VendaAdapter(getApplicationContext(), vends);
                    binding.vendaList.setAdapter(vendaAdapter);
                    vendaAdapter.setUnidadeQtdeCastanha(viewModel.getUpf().unidadeValor);
                    vendaAdapter.setActivity(self);
                    vendaAdapter.notifyDataSetChanged();
                }
            }
        });

    }

    public void openEditForm(Venda venda) {
        Intent intent = new Intent(SafraVendasListActivity.this, SafraVendaFormActivity.class).putExtra(
                "venda_id", venda.getId());
        startActivityForResult(intent, EDITAR_VENDA_ID_REPLY);
    }

    public void openSummaryScreen(Venda venda) {
        Intent intent = new Intent(SafraVendasListActivity.this, SafraVendaSummaryActivity.class)
                .putExtra("venda_id", venda.getId());
        startActivityForResult(intent, EDITAR_VENDA_ID_REPLY);
    }

    public void openDeleteDialog(final Venda venda) {
        new AlertDialog.Builder(this)
                .setMessage(getResources().getString(R.string.msg_dialog_venda_remover,venda.getDescricao()))
                .setPositiveButton(R.string.dialog_sim, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        viewModel.deleteVenda(venda);
                    }
                })
                .setNegativeButton(R.string.dialog_nao, null)
                .setTitle(R.string.label_dialog_venda_delete_title)
                .show();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent replyIntent = new Intent();
                setResult(RESULT_CANCELED, replyIntent);
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
