package br.coop.eita.castanhadora.db;


import android.content.Context;
import android.os.Environment;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SimpleSQLiteQuery;
import androidx.sqlite.db.SupportSQLiteDatabase;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;

import br.coop.eita.castanhadora.db.converters.BigDecimalConverter;
import br.coop.eita.castanhadora.db.converters.DateConverter;
import br.coop.eita.castanhadora.db.dao.CategoriaDespesaDao;
import br.coop.eita.castanhadora.db.dao.DespesaDao;
import br.coop.eita.castanhadora.db.dao.TrabalhoUpfDao;
import br.coop.eita.castanhadora.db.dao.EquipamentoDao;
import br.coop.eita.castanhadora.db.dao.TrabalhoUpfIntegranteDao;
import br.coop.eita.castanhadora.db.dao.UpfIntegranteDao;
import br.coop.eita.castanhadora.db.dao.MunicipioDao;
import br.coop.eita.castanhadora.db.dao.ProducaoDao;
import br.coop.eita.castanhadora.db.dao.SafraDao;
import br.coop.eita.castanhadora.db.dao.TipoEquipamentoDao;
import br.coop.eita.castanhadora.db.dao.UfDao;
import br.coop.eita.castanhadora.db.dao.UpfDao;
import br.coop.eita.castanhadora.db.dao.VendaDao;
import br.coop.eita.castanhadora.db.dao.VersaoBancoDadosDao;
import br.coop.eita.castanhadora.db.models.CategoriaDespesa;
import br.coop.eita.castanhadora.db.models.Despesa;
import br.coop.eita.castanhadora.db.models.TrabalhoUpf;
import br.coop.eita.castanhadora.db.models.TrabalhoUpfIntegrante;
import br.coop.eita.castanhadora.db.models.Equipamento;
import br.coop.eita.castanhadora.db.models.UpfIntegrante;
import br.coop.eita.castanhadora.db.models.Municipio;
import br.coop.eita.castanhadora.db.models.Producao;
import br.coop.eita.castanhadora.db.models.Safra;
import br.coop.eita.castanhadora.db.models.TipoEquipamento;
import br.coop.eita.castanhadora.db.models.Uf;
import br.coop.eita.castanhadora.db.models.Upf;
import br.coop.eita.castanhadora.db.models.Venda;
import br.coop.eita.castanhadora.db.models.VersaoBancoDados;

@Database(entities = {CategoriaDespesa.class, TrabalhoUpf.class, TrabalhoUpfIntegrante.class, Equipamento.class, Despesa.class, UpfIntegrante.class, Municipio.class, Producao.class, Safra.class, TipoEquipamento.class, Uf.class, Upf.class, Venda.class, VersaoBancoDados.class}, version = 5, exportSchema = true)
@TypeConverters({DateConverter.class, BigDecimalConverter.class})
public abstract class CastanhadoraDatabase extends RoomDatabase {

    private static final String DB_NAME = "castanhadora_db";

    public static final int ERROR_CODE_SQLITE_CONSTRAINT = 1;
    public static final int ERROR_CODE_VALIDATION = 2;
    public static final int ERROR_CODE_NULL_MODEL = 3;

    public abstract CategoriaDespesaDao categoriaDespesaDao();

    public abstract TrabalhoUpfDao trabalhoUpfDao();

    public abstract EquipamentoDao equipamentoDao();

    public abstract DespesaDao despesaDao();

    public abstract UpfIntegranteDao upfIntegranteDao();

    public abstract MunicipioDao municipioDao();

    public abstract ProducaoDao producaoDao();

    public abstract SafraDao safraDao();

    public abstract TipoEquipamentoDao tipoEquipamentoDao();

    public abstract UfDao ufDao();

    public abstract UpfDao upfDao();

    public abstract VendaDao vendaDao();

    public abstract TrabalhoUpfIntegranteDao trabalhoUpfIntegranteDao();

    public abstract VersaoBancoDadosDao versaoBancoDadosDao();

    private static volatile CastanhadoraDatabase INSTANCE;

    private HashMap<Class, Object> daos;

    public CastanhadoraDatabase() {
        super();
        daos = new HashMap<Class, Object>();
        daos.put(CategoriaDespesa.class, categoriaDespesaDao());
        daos.put(TrabalhoUpf.class, trabalhoUpfDao());
        daos.put(Equipamento.class, equipamentoDao());
        daos.put(Despesa.class, despesaDao());
        daos.put(UpfIntegrante.class, upfIntegranteDao());
        daos.put(Municipio.class, municipioDao());
        daos.put(Producao.class, producaoDao());
        daos.put(Safra.class, safraDao());
        daos.put(TipoEquipamento.class, tipoEquipamentoDao());
        daos.put(Uf.class, ufDao());
        daos.put(Upf.class, upfDao());
        daos.put(Venda.class, vendaDao());
        daos.put(VersaoBancoDados.class, versaoBancoDadosDao());
    }

    @VisibleForTesting
    static final Migration MIGRATION_1_2 = new Migration(1, 2) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL(
                    "ALTER TABLE upf_integrante ADD COLUMN parentesco TEXT DEFAULT ''");
            database.execSQL(
                    "ALTER TABLE upf_integrante ADD COLUMN genero TEXT DEFAULT ''");
        }
    };

    @VisibleForTesting
    static final Migration MIGRATION_2_3 = new Migration(2, 3) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL(
                    "ALTER TABLE upf ADD COLUMN genero_usuario TEXT DEFAULT ''");
        }
    };

    @VisibleForTesting
    static final Migration MIGRATION_3_4 = new Migration(3, 4) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("CREATE TABLE `versao_banco_dados` (`id` INTEGER, "
                    + "`versao` TEXT, PRIMARY KEY(`id`))");
        }
    };

    @VisibleForTesting
    static final Migration MIGRATION_4_5 = new Migration(4, 5) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE upf ADD COLUMN cooperativa TEXT DEFAULT ''");
            database.execSQL("ALTER TABLE safra ADD COLUMN municipio_id TEXT DEFAULT ''");
            database.execSQL("ALTER TABLE safra ADD COLUMN uf_id TEXT DEFAULT ''");
        }
    };

    @VisibleForTesting
    static final Migration MIGRATION_5_6 = new Migration(5, 6) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE safra ADD COLUMN simulacao INTEGER DEFAULT 0");
            database.execSQL("ALTER TABLE upf ADD COLUMN compartilhar_nuvem INTEGER DEFAULT 0");
        }
    };

    public static CastanhadoraDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (CastanhadoraDatabase.class) {
                if (INSTANCE == null) {
                    Log.i("CastanhadoraDatabase", "instanciar/criar banco de dados");
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            CastanhadoraDatabase.class, DB_NAME)
                            .addMigrations(MIGRATION_1_2)
                            .addMigrations(MIGRATION_2_3)
                            .addMigrations(MIGRATION_3_4)
                            .addMigrations(MIGRATION_4_5)
                            .addMigrations(MIGRATION_5_6)
                            .allowMainThreadQueries()
                            .build();
                    INSTANCE.populateInitialData();
                }
            }
        }
        return INSTANCE;
    }

    private void populateInitialData() {

        runInTransaction(new Runnable() {
            @Override
            public void run() {
                Integer count = upfDao().count();
                if (count == 0) {

                    Log.i("CastanhadoraDatabase",
                            "populateInitialData, porque nao tinham registros");

                    Upf upf = new Upf();
                    upf.setGeneroUsuario("");
                    upfDao().insert(upf);

                    tipoEquipamentoDao().insert(new TipoEquipamento("motosserra"));
                    tipoEquipamentoDao().insert(new TipoEquipamento("roçadeira"));
                    tipoEquipamentoDao().insert(new TipoEquipamento("carro"));

                    categoriaDespesaDao().insert(new CategoriaDespesa("Diversos"));
                    categoriaDespesaDao().insert(new CategoriaDespesa("Diversos2"));
                    categoriaDespesaDao().insert(new CategoriaDespesa("Diversos3"));

                    Integer version = safraDao().checkpoint(new SimpleSQLiteQuery("PRAGMA user_version"));
                    String version_str = "|||||" + version.toString() + "|||||";
                    versaoBancoDadosDao().insert(new VersaoBancoDados(version_str));
                } else {
                    Log.i("CastanhadoraDatabase",
                            "populateInitialData, banco de dados já inicializado");
                }
            }
        });

    }

    public Object getDaoForType(Class<?> t) {
        return daos.get(t);
    }

    public static boolean backUpDataBase(Context context) {
        boolean result = true;

        // Source path in the application database folder
        String appDbPath = context.getDatabasePath(DB_NAME).getAbsolutePath();

        // Destination Path to the sdcard app folder
        String sdFolder = "/sdcard/" + DB_NAME;


        InputStream myInput = null;
        OutputStream myOutput = null;
        try {
            //Open your local db as the input stream
            myInput = new FileInputStream(appDbPath);
            //Open the empty db as the output stream
            myOutput = new FileOutputStream(sdFolder);

            //transfer bytes from the inputfile to the outputfile
            byte[] buffer = new byte[1024];
            int length;
            while ((length = myInput.read(buffer)) > 0) {
                myOutput.write(buffer, 0, length);
            }
        } catch (IOException e) {
            result = false;
            e.printStackTrace();
        } finally {
            try {
                //Close the streams
                if (myOutput != null) {
                    myOutput.flush();
                    myOutput.close();
                }
                if (myInput != null) {
                    myInput.close();
                }
            } catch (IOException e) { }
        }

        return result;
    }

}