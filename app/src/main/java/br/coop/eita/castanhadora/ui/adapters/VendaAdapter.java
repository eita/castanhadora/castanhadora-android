package br.coop.eita.castanhadora.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import androidx.databinding.DataBindingUtil;

import java.util.List;

import br.coop.eita.castanhadora.R;
import br.coop.eita.castanhadora.databinding.ListitemVendaBinding;
import br.coop.eita.castanhadora.db.models.Venda;
import br.coop.eita.castanhadora.ui.activities.SafraVendasListActivity;

public class VendaAdapter extends ArrayAdapter<Venda> {
    private SafraVendasListActivity activity;
    private String unidadeQtdeCastanha;

    public VendaAdapter(Context context, List<Venda> objects) {
        super(context, 0, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ListitemVendaBinding binding;

        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.listitem_venda, null);
            binding = DataBindingUtil.bind(convertView);
            convertView.setTag(binding);
        } else {
            binding = (ListitemVendaBinding) convertView.getTag();
        }

        final Venda venda = getItem(position);
        final VendaAdapter self = this;

        binding.setVenda(venda);

        binding.primaryText.setText(venda.getTituloCard(unidadeQtdeCastanha));

        binding.summaryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.openSummaryScreen(venda);
            }
        });

        binding.editBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.openEditForm(venda);
            }
        });

        binding.removeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.openDeleteDialog(venda);
            }
        });

        return binding.getRoot();
    }

    public void setActivity(SafraVendasListActivity activity) {
        this.activity = activity;
    }

    public void setUnidadeQtdeCastanha(String unidadeQtdeCastanha) {
        this.unidadeQtdeCastanha = unidadeQtdeCastanha;
    }
}
