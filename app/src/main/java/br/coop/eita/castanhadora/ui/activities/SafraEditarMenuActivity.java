package br.coop.eita.castanhadora.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import br.coop.eita.castanhadora.R;
import br.coop.eita.castanhadora.databinding.ActivitySafraEditarMenuBinding;
import br.coop.eita.castanhadora.databinding.ActivitySafraInicioBinding;
import br.coop.eita.castanhadora.viewmodels.SafraViewModel;

public class SafraEditarMenuActivity extends AppCompatActivity {

    private SafraViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActivitySafraEditarMenuBinding binding = DataBindingUtil.setContentView(this,
                R.layout.activity_safra_editar_menu);

        viewModel = ViewModelProviders.of(this).get(SafraViewModel.class);
        binding.setViewmodel(viewModel);
    }
}
