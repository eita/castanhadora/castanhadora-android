package br.coop.eita.castanhadora.db.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Query;

import java.util.List;

import br.coop.eita.castanhadora.db.models.Equipamento;
import br.coop.eita.castanhadora.db.models.Venda;

@Dao
public abstract class VendaDao extends BaseIdentifiableDao<Venda> {

    @Query("SELECT * FROM " + Venda.TABLE_NAME + " WHERE safra_id = :safraId")
    public abstract LiveData<List<Venda>> getVendasLive(Integer safraId);

    @Query("SELECT * FROM " + Venda.TABLE_NAME + " WHERE safra_id = :safraId")
    public abstract List<Venda> getVendas(Integer safraId);

}
