package br.coop.eita.castanhadora.db.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Query;

import java.math.BigDecimal;
import java.util.List;

import br.coop.eita.castanhadora.db.models.Equipamento;
import br.coop.eita.castanhadora.db.models.TipoEquipamento;
import br.coop.eita.castanhadora.db.models.derived.EquipamentoETipo;

@Dao
public abstract class EquipamentoDao extends BaseIdentifiableDao<Equipamento> {

    @Query("SELECT * FROM " + Equipamento.TABLE_NAME + " WHERE safra_id = :safraId")
    public abstract LiveData<List<Equipamento>> getEquipamentosLive(Integer safraId);

    @Query("SELECT * FROM " + Equipamento.TABLE_NAME + " WHERE safra_id = :safraId")
    public abstract List<Equipamento> getEquipamentos(Integer safraId);

    @Query("SELECT equipamento.*, tipo_equipamento.nome as tipoEquipamento FROM equipamento INNER JOIN tipo_equipamento on equipamento.tipo_equipamento_id = tipo_equipamento.id WHERE equipamento.safra_id = :safraId")
    public abstract LiveData<List<EquipamentoETipo>> getEquipamentosETipos(Integer safraId);


}
