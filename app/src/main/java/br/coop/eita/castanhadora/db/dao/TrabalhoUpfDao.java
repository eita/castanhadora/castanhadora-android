package br.coop.eita.castanhadora.db.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Query;

import java.math.BigDecimal;
import java.util.List;

import br.coop.eita.castanhadora.db.models.TrabalhoUpf;
import br.coop.eita.castanhadora.db.models.TrabalhoUpfIntegrante;
import br.coop.eita.castanhadora.db.models.UpfIntegrante;
import br.coop.eita.castanhadora.db.models.derived.TrabalhoEIntegrantesUpf;

@Dao
public abstract class TrabalhoUpfDao extends BaseIdentifiableDao<TrabalhoUpf> {

    @Query("SELECT * FROM " + TrabalhoUpf.TABLE_NAME + " WHERE safra_id = :safraId")
    public abstract LiveData<List<TrabalhoUpf>> getTrabalhos(Integer safraId);

    @Query("SELECT IFNULL(sum(a.DIAS),0) FROM " + TrabalhoUpf.TABLE_NAME + " a JOIN " + TrabalhoUpfIntegrante.TABLE_NAME + " b ON b.trabalho_upf_id = a.id WHERE a.safra_id = :safraId")
    public abstract BigDecimal getTotalDias(Integer safraId);

    @Query("SELECT a.*, GROUP_CONCAT(c.nome, '|||') AS integrantes FROM " + TrabalhoUpf.TABLE_NAME + " a JOIN " + TrabalhoUpfIntegrante.TABLE_NAME + " b ON b.trabalho_upf_id = a.id JOIN " + UpfIntegrante.TABLE_NAME + " c ON b.upf_integrante_id = c.id WHERE a.safra_id = :safraId GROUP BY a.id ORDER BY a.data DESC")
    public abstract LiveData<List<TrabalhoEIntegrantesUpf>> getTrabalhosEIntegrantesUpfLive(Integer safraId);

    @Query("SELECT a.*, GROUP_CONCAT(c.nome, '|||') AS integrantes FROM " + TrabalhoUpf.TABLE_NAME + " a JOIN " + TrabalhoUpfIntegrante.TABLE_NAME + " b ON b.trabalho_upf_id = a.id JOIN " + UpfIntegrante.TABLE_NAME + " c ON b.upf_integrante_id = c.id WHERE a.safra_id = :safraId GROUP BY a.id ORDER BY a.data DESC")
    public abstract List<TrabalhoEIntegrantesUpf> getTrabalhosEIntegrantesUpf(Integer safraId);

    public void deleteTrabalho(TrabalhoUpf obj) {
        deleteTrabalhoUpfIntegrantes(obj.getId());
        delete(obj);
    }

    @Query("DELETE FROM " + TrabalhoUpfIntegrante.TABLE_NAME + " WHERE trabalho_upf_id = :trabalhoId")
    public abstract void deleteTrabalhoUpfIntegrantes(Integer trabalhoId);

}

