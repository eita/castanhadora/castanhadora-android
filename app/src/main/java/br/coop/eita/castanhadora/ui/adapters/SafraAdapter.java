package br.coop.eita.castanhadora.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import androidx.databinding.DataBindingUtil;

import java.util.List;

import br.coop.eita.castanhadora.R;
import br.coop.eita.castanhadora.databinding.ListitemDespesaBinding;
import br.coop.eita.castanhadora.databinding.ListitemSafraBinding;
import br.coop.eita.castanhadora.db.models.Despesa;
import br.coop.eita.castanhadora.db.models.Safra;
import br.coop.eita.castanhadora.ui.activities.SafraDespesasListActivity;
import br.coop.eita.castanhadora.ui.activities.SafraListActivity;
import br.coop.eita.castanhadora.viewmodels.SafraViewModel;

public class SafraAdapter extends ArrayAdapter<Safra> {
    private List<Safra> safras;
    private SafraListActivity activity;
    private SafraViewModel viewModel;

    public SafraAdapter(Context context, List<Safra> objects, SafraViewModel viewModel) {
        super(context, 0, objects);
        this.viewModel = viewModel;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ListitemSafraBinding binding;

        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.listitem_safra, null);
            binding = DataBindingUtil.bind(convertView);
            convertView.setTag(binding);
        } else {
            binding = (ListitemSafraBinding) convertView.getTag();
        }

        final Safra safra = getItem(position);
        final SafraAdapter self = this;

        binding.setSafra(safra);
        binding.setSafraAtiva(viewModel.getSafraAtiva());


        /*binding.editBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.openEditForm(safra);
            }
        });*/

        binding.removeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.openDeleteDialog(safra);
            }
        });

        binding.selecionarBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.ativarSafra(safra);
            }
        });

        binding.shareSummaryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.shareSafraClick(safra);
            }
        });

        return binding.getRoot();
    }

    public void setActivity(SafraListActivity activity) {
        this.activity = activity;
    }
}
