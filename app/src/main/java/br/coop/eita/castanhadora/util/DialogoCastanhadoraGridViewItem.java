package br.coop.eita.castanhadora.util;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

import br.coop.eita.castanhadora.BR;

public class DialogoCastanhadoraGridViewItem extends BaseObservable {
    private String label;
    private String value;
    private Boolean textRed;

    public DialogoCastanhadoraGridViewItem(String label, String value) {
        this.label = label;
        this.value = value;
        this.textRed = false;
    }

    public DialogoCastanhadoraGridViewItem(String label, String value, Boolean textRed) {
        this.label = label;
        this.value = value;
        this.textRed = textRed;
    }

    @Bindable
    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
        notifyPropertyChanged(BR.label);
    }

    @Bindable
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
        notifyPropertyChanged(BR.value);
    }

    public Boolean isTextRed() {
        return textRed;
    }

    public void setTextRed(Boolean textRed) {
        this.textRed = textRed;
    }
}
