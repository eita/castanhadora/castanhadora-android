package br.coop.eita.castanhadora.ui.activities;

import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.MediaController;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import br.coop.eita.castanhadora.R;
import br.coop.eita.castanhadora.databinding.ActivityVideoTutorialBinding;

public class VideoTutorialActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        int videoPath = getIntent().getIntExtra("videoPath", 0);
        int videoTitle = getIntent().getIntExtra("videoTitle", 0);

        ActivityVideoTutorialBinding binding = DataBindingUtil.setContentView(this,
                R.layout.activity_video_tutorial);
        Uri uri = Uri.parse("android.resource://"+ getPackageName() + "/" + videoPath);
        binding.videoView.setVideoURI(uri);

        MediaController mediaController = new MediaController(this);
        mediaController.setBackgroundColor(Color.rgb(205, 205, 205));
        mediaController.setAnchorView(binding.videoView);
        binding.videoView.setMediaController(mediaController);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(videoTitle);
        }

        binding.videoView.seekTo(1);
        binding.videoView.start();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
