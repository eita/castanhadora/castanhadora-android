package br.coop.eita.castanhadora.ui.activities;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import java.math.BigDecimal;
import java.util.ArrayList;

import br.coop.eita.castanhadora.R;
import br.coop.eita.castanhadora.databinding.ActivitySafraVendaSimulacaoBinding;
import br.coop.eita.castanhadora.db.models.Venda;
import br.coop.eita.castanhadora.db.models.derived.TotalGeral;
import br.coop.eita.castanhadora.util.Util;
import br.coop.eita.castanhadora.viewmodels.VendaViewModel;
import br.coop.eita.castanhadora.db.models.Upf;


public class SafraVendaSimulacaoActivity extends AppCompatActivity {

    private VendaViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final ActivitySafraVendaSimulacaoBinding binding = DataBindingUtil.setContentView(this,
                R.layout.activity_safra_venda_simulacao);

        viewModel = ViewModelProviders.of(this).get(VendaViewModel.class);

        Venda venda = viewModel.getVendaAtual();

        TotalGeral totalGeral = viewModel.getTotalGeral();

        Upf upf = viewModel.getUpf();

        setTitle(getTitleString(venda.getQuantidade(), venda.getValorUnitario(),
                upf.getUnidadeValor()));


        TotalGeral.DadosSimulacaoVenda dadosSimulacaoVenda = totalGeral.simularVenda(
                venda.getValorUnitario(), venda.getQuantidade());

        /*****************************************************
         * SIMULAÇÂO                                         *
         *****************************************************/
        ArrayList<String> informacoesSimulacao = new ArrayList<>();

        //Cobre custos totais de despesas?
        Integer simulacaoCoberturaDespesas = dadosSimulacaoVenda.getSimulacaoCoberturaDespesas();
        if (simulacaoCoberturaDespesas == 0) {
            informacoesSimulacao.add(getResources().getString(R.string.venda_sim_producao_nao));
        } else if (simulacaoCoberturaDespesas == 100) {
            informacoesSimulacao.add(getResources().getString(R.string.venda_sim_producao_sim));
        } else {
            informacoesSimulacao.add(getResources().getString(R.string.venda_sim_producao_parcial,
                    simulacaoCoberturaDespesas.toString() + "%"));
        }

        //Cobre custos totais de remuneração familiar?
        if (Util.deveInserirRemuneracao(viewModel.getSafraAtiva())) {
            Integer simulacaoCoberturaRemuneracao = dadosSimulacaoVenda.getSimulacaoCoberturaRemuneracao();
            if (simulacaoCoberturaRemuneracao == 0) {
                informacoesSimulacao.add(getResources().getString(R.string.venda_sim_remuneracao_nao));
            } else if (simulacaoCoberturaRemuneracao == 100) {
                informacoesSimulacao.add(getResources().getString(R.string.venda_sim_remuneracao_sim));
            } else {
                informacoesSimulacao.add(
                        getResources().getString(R.string.venda_sim_remuneracao_parcial,
                                simulacaoCoberturaRemuneracao.toString() + "%"));
            }
        }

        //Cobre custos totais de depreciação?
        Integer simulacaoCoberturaDepreciacao = dadosSimulacaoVenda.getSimulacaoCoberturaDepreciacao();
        if (simulacaoCoberturaDepreciacao == 0) {
            informacoesSimulacao.add(getResources().getString(R.string.venda_sim_depreciacao_nao));
        } else if (simulacaoCoberturaDepreciacao == 100) {
            informacoesSimulacao.add(getResources().getString(R.string.venda_sim_depreciacao_sim));
        } else {
            informacoesSimulacao.add(
                    getResources().getString(R.string.venda_sim_depreciacao_parcial,
                            simulacaoCoberturaDepreciacao.toString() + "%"));
        }

        //Tem excedente?
        BigDecimal simulacaoExcedente = dadosSimulacaoVenda.getSimulacaoExcedente();
        if (simulacaoExcedente.equals(BigDecimal.ZERO)) {
            informacoesSimulacao.add(getResources().getString(R.string.venda_sim_excedente_nao));
        } else {
            informacoesSimulacao.add(getResources().getString(R.string.venda_sim_excedente_sim,
                    Util.formatMoney(simulacaoExcedente)));
        }

        binding.simulacaoInformacoes.setText(TextUtils.join("\n", informacoesSimulacao));
        binding.simulacaoSubtitulo.setText(
                getResources().getString(R.string.venda_simulacao_subtitulo,
                        Util.formatNumber(venda.getQuantidade()), upf.getUnidadeValor(),
                        Util.formatMoney(venda.getValorUnitario())));


        /*****************************************************
         * PROJEÇÂO                                          *
         *****************************************************/
        switch (viewModel.getVendaStatus()) {
            case Venda.STATUS_INSUFICIENTE:
                binding.projecaoCard.setCardBackgroundColor(
                        getResources().getColor(R.color.red100));
                break;
            case Venda.STATUS_PAGA_DESPESAS:
            case Venda.STATUS_PAGA_DESPESAS_E_REMUNERACAO:
                binding.projecaoCard.setCardBackgroundColor(
                        getResources().getColor(R.color.yellow100));
                break;
            case Venda.STATUS_PAGA_TUDO:
                binding.projecaoCard.setCardBackgroundColor(
                        getResources().getColor(R.color.green100));
                break;
        }

        ArrayList<String> informacoesProjecao = new ArrayList<>();

        //Cobre custos totais de despesas?
        Integer projecaoCoberturaDespesas = dadosSimulacaoVenda.getProjecaoCoberturaDespesas();
        if (projecaoCoberturaDespesas == 0) {
            informacoesProjecao.add(
                    getResources().getString(R.string.venda_sim_projecao_producao_nao));
        } else if (projecaoCoberturaDespesas == 100) {
            informacoesProjecao.add(
                    getResources().getString(R.string.venda_sim_projecao_producao_sim));
        } else {
            informacoesProjecao.add(
                    getResources().getString(R.string.venda_sim_projecao_producao_parcial,
                            projecaoCoberturaDespesas.toString() + "%"));
        }

        //Cobre custos totais de remuneração familiar?
        if (Util.deveInserirRemuneracao(viewModel.getSafraAtiva())) {
            Integer projecaoCoberturaRemuneracao = dadosSimulacaoVenda.getProjecaoCoberturaRemuneracao();
            if (projecaoCoberturaRemuneracao == 0) {
                informacoesProjecao.add(
                        getResources().getString(R.string.venda_sim_projecao_remuneracao_nao));
            } else if (projecaoCoberturaRemuneracao == 100) {
                informacoesProjecao.add(
                        getResources().getString(R.string.venda_sim_projecao_remuneracao_sim));
            } else {
                informacoesProjecao.add(
                        getResources().getString(R.string.venda_sim_projecao_remuneracao_parcial,
                                projecaoCoberturaRemuneracao.toString() + "%"));
            }
        }

        //Cobre custos totais de depreciação?
        Integer projecaoCoberturaDepreciacao = dadosSimulacaoVenda.getProjecaoCoberturaDepreciacao();
        if (projecaoCoberturaDepreciacao == 0) {
            informacoesProjecao.add(
                    getResources().getString(R.string.venda_sim_projecao_depreciacao_nao));
        } else if (projecaoCoberturaDepreciacao == 100) {
            informacoesProjecao.add(
                    getResources().getString(R.string.venda_sim_projecao_depreciacao_sim));
        } else {
            informacoesProjecao.add(
                    getResources().getString(R.string.venda_sim_projecao_depreciacao_parcial,
                            projecaoCoberturaDepreciacao.toString() + "%"));
        }

        //Tem excedente?
        BigDecimal projecaoExcedente = dadosSimulacaoVenda.getProjecaoExcedente();
        if (projecaoExcedente.equals(BigDecimal.ZERO)) {
            informacoesProjecao.add(
                    getResources().getString(R.string.venda_sim_projecao_excedente_nao));
        } else {
            informacoesProjecao.add(
                    getResources().getString(R.string.venda_sim_projecao_excedente_sim,
                            Util.formatMoney(projecaoExcedente)));
        }

        binding.projecaoInformacoes.setText(TextUtils.join("\n", informacoesProjecao));
        binding.projecaoSubtitulo.setText(
                getResources().getString(R.string.venda_projecao_subtitulo,
                        Util.formatMoney(venda.getValorUnitario())));

        BigDecimal novoValorPrevisto = dadosSimulacaoVenda.getNovoValorPrevisto();
        if (novoValorPrevisto == null) {
            binding.previsaoTitulo.setText(
                    getResources().getString(R.string.venda_sim_previsao_vende_todo_o_estoque));
        } else if (novoValorPrevisto.floatValue() <= 0) {
            binding.previsaoTitulo.setText(
                    getResources().getString(R.string.venda_sim_previsao_novas_vendas_completo));
        } else {
            binding.previsaoTitulo.setText(
                    getResources().getString(R.string.venda_sim_previsao_novas_vendas,
                            Util.formatMoney(novoValorPrevisto)));
        }
    }

    public void fechar(View view) {
        finish();
    }

    private String getTitleString(Integer quantidade, BigDecimal valorUnitario,
                                  String unidadeValor) {
        if (quantidade == 1) {
            return getResources().getString(R.string.title_activity_venda_simulacao_unitario,
                    Util.unidadeValorSingular(unidadeValor), Util.formatMoney(valorUnitario));
        } else {
            return getResources().getString(R.string.title_activity_venda_simulacao, quantidade,
                    unidadeValor, Util.formatMoney(valorUnitario),
                    Util.unidadeValorSingular(unidadeValor));
        }
    }
}
