package br.coop.eita.castanhadora.db.models;

import android.content.Context;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.Bindable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.coop.eita.castanhadora.BR;
import br.coop.eita.castanhadora.Castanhadora;
import br.coop.eita.castanhadora.R;
import br.coop.eita.castanhadora.util.Util;

import static androidx.room.ForeignKey.CASCADE;

@Entity(tableName = Venda.TABLE_NAME, foreignKeys = {@ForeignKey(onDelete = CASCADE, entity = Safra.class, parentColumns = "id", childColumns = "safra_id"),})
public class Venda extends BaseIdentifiableModel {
    public static final String TABLE_NAME = "venda";

    public static final int STATUS_INSUFICIENTE = 1;
    public static final int STATUS_PAGA_DESPESAS = 2;
    public static final int STATUS_PAGA_DESPESAS_E_REMUNERACAO = 3;
    public static final int STATUS_PAGA_TUDO = 4;

    @PrimaryKey(autoGenerate = true)
    private int id;

    @NonNull
    @ColumnInfo(name = "quantidade")
    private Integer quantidade;

    @NonNull
    @ColumnInfo(name = "valor_unitario")
    private BigDecimal valorUnitario;

    @NonNull
    @ColumnInfo(name = "data")
    private Date data;

    @ColumnInfo(name = "observacoes")
    private String observacoes;

    @ColumnInfo(name = "safra_id", index = true)
    public int safraId;

    @ColumnInfo(name = "comprador_id", index = true)
    public int compradorId;

    //Comprador serializado?
    @ColumnInfo(name = "comprador")
    public String comprador;

    @ColumnInfo(name = "quitacao_aviamento")
    public Boolean quitacaoAviamento;

    public Venda() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @NonNull
    @Bindable
    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(@NonNull Integer quantidade) {
        this.quantidade = quantidade;
        notifyPropertyChanged(BR.valorTotal);
    }

    public void setQuantidade(@NonNull Integer quantidade, Boolean updateBind) {
        this.quantidade = quantidade;
        if (updateBind) {
            notifyPropertyChanged(BR.quantidade);
        }
    }

    @NonNull
    @Bindable
    public BigDecimal getValorUnitario() {
        return valorUnitario;
    }

    public void setValorUnitario(@NonNull BigDecimal valorUnitario) {
        this.valorUnitario = valorUnitario;
        notifyPropertyChanged(BR.valorTotal);
    }

    public void setValorUnitario(@NonNull BigDecimal valorUnitario, Boolean updateBind) {
        this.valorUnitario = valorUnitario;
        if (updateBind) {
            notifyPropertyChanged(BR.valorUnitario);
        }
    }
    @NonNull
    public Date getData() {
        return data;
    }

    public void setData(@NonNull Date data) {
        this.data = data;
    }

    public String getObservacoes() {
        return observacoes;
    }

    public void setObservacoes(String observacoes) {
        this.observacoes = observacoes;
    }

    public String getComprador() {
        return comprador;
    }

    public void setComprador(String comprador) {
        this.comprador = comprador;
    }

    public Boolean getQuitacaoAviamento() {
        return quitacaoAviamento;
    }

    public void setQuitacaoAviamento(Boolean quitacaoAviamento) {
        this.quitacaoAviamento = quitacaoAviamento;
    }

    public int getSafraId() {
        return safraId;
    }

    public void setSafraId(int safraId) {
        this.safraId = safraId;
    }

    public int getCompradorId() {
        return compradorId;
    }

    public void setCompradorId(int compradorId) {
        this.compradorId = compradorId;
    }

    public String getDescricao() {
        String dData = DateUtils.formatDateTime(Castanhadora.APP, getData().getTime(),
                DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_NUMERIC_DATE | DateUtils.FORMAT_SHOW_YEAR);
        return dData + " - R$ " + getValorUnitario().multiply(new BigDecimal(quantidade));
    }

    @Bindable
    public BigDecimal getValorTotal() {
        if (valorUnitario != null && quantidade != null) {
            return valorUnitario.multiply(new BigDecimal(quantidade));
        }
        return null;
    }

    public List<String> validateDadosBasicos() {
        ArrayList<String> validationErrors = new ArrayList<String>();
        if (this.valorUnitario == null || valorUnitario.floatValue() < 0.01) {
            validationErrors.add(Castanhadora.APP.getResources().getString(
                    R.string.error_val_valor_unitario_empty));
        }
        if (this.quantidade == null || quantidade == 0) {
            validationErrors.add(Castanhadora.APP.getResources().getString(
                    R.string.error_val_venda_quantidade_empty));
        }
        return validationErrors;
    }

    @Override
    public List<String> validate() {
        List<String> validationErrors = validateDadosBasicos();
        if (this.data == null) {
            validationErrors.add(Castanhadora.APP.getResources().getString(
                    R.string.error_val_venda_data_empty));
        }
        if (TextUtils.isEmpty(this.comprador)) {
            validationErrors.add(Castanhadora.APP.getResources().getString(
                    R.string.error_val_venda_comprador_empty));
        }

        return validationErrors;
    }

    public String getTituloCard(String unidadeQtdeCastanha) {
        Context context = Castanhadora.APP.getApplicationContext();
        return context.getString(R.string.venda_titulo_card, getQuantidade().toString(), unidadeQtdeCastanha, Util.formatMoney(getValorTotal()));
    }

    public String getSubtituloCard() {
        String dData = DateUtils.formatDateTime(Castanhadora.APP, getData().getTime(),
                DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_NUMERIC_DATE | DateUtils.FORMAT_SHOW_YEAR);
        Context context = Castanhadora.APP.getApplicationContext();
        return context.getString(R.string.venda_subtitulo_card, getComprador(), dData);
    }

    public String getDescricaoCard() {
        return observacoes;
    }
}
