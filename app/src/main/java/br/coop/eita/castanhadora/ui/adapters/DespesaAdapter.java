package br.coop.eita.castanhadora.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import androidx.databinding.DataBindingUtil;

import java.util.List;

import br.coop.eita.castanhadora.R;
import br.coop.eita.castanhadora.databinding.ListitemDespesaBinding;
import br.coop.eita.castanhadora.databinding.ListitemEquipamentoBinding;
import br.coop.eita.castanhadora.db.models.Despesa;
import br.coop.eita.castanhadora.ui.activities.SafraDespesasListActivity;
import br.coop.eita.castanhadora.ui.activities.SafraProducoesListActivity;

public class DespesaAdapter extends ArrayAdapter<Despesa> {
    private List<Despesa> despesas;
    private SafraDespesasListActivity activity;

    public DespesaAdapter(Context context, List<Despesa> objects) {
        super(context, 0, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ListitemDespesaBinding binding;

        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.listitem_despesa, null);
            binding = DataBindingUtil.bind(convertView);
            convertView.setTag(binding);
        } else {
            binding = (ListitemDespesaBinding) convertView.getTag();
        }

        final Despesa despesa = getItem(position);
        final DespesaAdapter self = this;

        binding.setDespesa(despesa);

        binding.mediaImage.setImageResource(despesa.getIcone());

        binding.editBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.openEditForm(despesa);
            }
        });

        binding.removeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.openDeleteDialog(despesa);
            }
        });

        return binding.getRoot();
    }

    public void setActivity(SafraDespesasListActivity activity) {
        this.activity = activity;
    }
}
