package br.coop.eita.castanhadora.db.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Query;

import java.util.List;

import br.coop.eita.castanhadora.db.models.CategoriaDespesa;
import br.coop.eita.castanhadora.db.models.Equipamento;

@Dao
public abstract class CategoriaDespesaDao extends BaseIdentifiableDao<CategoriaDespesa> {

    @Query("SELECT * FROM " + CategoriaDespesa.TABLE_NAME)
    public abstract LiveData<List<CategoriaDespesa>> getAll();

}
