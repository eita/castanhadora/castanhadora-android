package br.coop.eita.castanhadora.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import java.util.ArrayList;
import java.util.List;

import br.coop.eita.castanhadora.R;
import br.coop.eita.castanhadora.databinding.ActivitySafraProducaoFormBinding;
import br.coop.eita.castanhadora.db.DataRepository;
import br.coop.eita.castanhadora.db.models.Producao;
import br.coop.eita.castanhadora.util.DialogoCastanhadoraGridViewItem;
import br.coop.eita.castanhadora.util.Util;
import br.coop.eita.castanhadora.viewmodels.ProducaoViewModel;

public class SafraProducaoFormActivity extends AppCompatActivity {

    public static final String PRODUCAO_ID_REPLY = "br.coop.eita.castanhadora.producao_id.REPLY";

    private ProducaoViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        ActivitySafraProducaoFormBinding binding = DataBindingUtil.setContentView(this,
                R.layout.activity_safra_producao_form);

        viewModel = ViewModelProviders.of(this).get(ProducaoViewModel.class);
        binding.setViewmodel(viewModel);

        Integer producaoId = getIntent().getIntExtra("producao_id", 0);

        Producao producao = viewModel.loadProducao(producaoId);

        actionBar.setTitle(
                producao.getId() > 0 ? R.string.label_producao_editar : R.string.label_producao_nova);

        ArrayList<DialogoCastanhadoraGridViewItem> valores = new ArrayList<>();

        Integer producaoTotal = viewModel.getProducaoTotal();
        Integer producaoEmEstoque = viewModel.getProducaoEmEstoque();

        String unidade = viewModel.getUpf().getUnidadeValor();

        valores.add(new DialogoCastanhadoraGridViewItem(
                getResources().getString(R.string.producao_total),
                Util.formatNumber(producaoTotal) + " " + unidade));

        valores.add(new DialogoCastanhadoraGridViewItem(
                getResources().getString(R.string.producao_em_estoque),
                Util.formatNumber(producaoEmEstoque) + " " + unidade));


        binding.painelSintese.setValores(valores);

        binding.producaoQuantidade.requestFocus();

        binding.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SafraProducaoFormActivity.this, VideoTutorialActivity.class);
                intent.putExtra("videoPath",  R.raw.producao);
                intent.putExtra("videoTitle", R.string.safra_totais_producao);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_form, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent replyIntent = new Intent();
                setResult(RESULT_CANCELED, replyIntent);
                finish();
                return true;
            case R.id.action_save:
                final SafraProducaoFormActivity self = this;

                viewModel.saveProducao(new DataRepository.OnSaveEntityListener() {
                    @Override
                    public void onSuccess(Integer... ids) {
                        Intent replyIntent2 = new Intent();
                        replyIntent2.putExtra(PRODUCAO_ID_REPLY, ids[0]);
                        setResult(RESULT_OK, replyIntent2);
                        Toast.makeText(self, R.string.saved_producao, Toast.LENGTH_LONG).show();
                        finish();
                    }

                    @Override
                    public void onFailure(int errorCode, List<String> errorMessages) {
                        Toast.makeText(self, errorMessages.get(0), Toast.LENGTH_LONG).show();
                    }
                });

                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
