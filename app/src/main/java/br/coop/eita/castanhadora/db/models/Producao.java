package br.coop.eita.castanhadora.db.models;

import android.text.TextUtils;
import android.text.format.DateUtils;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

import java.util.Date;

import br.coop.eita.castanhadora.Castanhadora;

import static androidx.room.ForeignKey.CASCADE;

@Entity(tableName = Producao.TABLE_NAME, foreignKeys = @ForeignKey(onDelete = CASCADE, entity = Safra.class, parentColumns = "id", childColumns = "safra_id"))
public class Producao extends BaseIdentifiableModel {
    public static final String TABLE_NAME = "producao";

    @PrimaryKey(autoGenerate = true)
    private int id;

    @NonNull
    @ColumnInfo(name = "data")
    private Date data;

    @NonNull
    @ColumnInfo(name = "quantidade")
    private Integer quantidade;

    @ColumnInfo(name = "safra_id", index = true)
    public int safraId;

    @ColumnInfo(name = "observacoes")
    private String observacoes;

    public Producao() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @NonNull
    public Date getData() {
        return data;
    }

    public void setData(@NonNull Date data) {
        this.data = data;
    }

    @NonNull
    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(@NonNull Integer quantidade) {
        this.quantidade = quantidade;
    }

    public int getSafraId() {
        return safraId;
    }

    public void setSafraId(int safraId) {
        this.safraId = safraId;
    }

    public String getObservacoes() {
        return observacoes;
    }

    public void setObservacoes(String observacoes) {
        this.observacoes= observacoes;
    }

    public String getDescricao() {
        String dData = DateUtils.formatDateTime(Castanhadora.APP, getData().getTime(),
                DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_NUMERIC_DATE | DateUtils.FORMAT_SHOW_YEAR);
        return dData;
    }

    public String getTituloCard(String unidadeQtdeCastanhas) {
        return quantidade + " " + unidadeQtdeCastanhas;
    }

    public String getSubtituloCard() {
        String dData = DateUtils.formatDateTime(Castanhadora.APP, getData().getTime(),
                DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_NUMERIC_DATE | DateUtils.FORMAT_SHOW_YEAR);
        return dData;
    }

    public String getDescricaoCard() {
        return (TextUtils.isEmpty(observacoes)) ? "" : observacoes;
    }

}
