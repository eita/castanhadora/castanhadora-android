package br.coop.eita.castanhadora.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import androidx.databinding.DataBindingUtil;

import java.util.List;

import br.coop.eita.castanhadora.R;
import br.coop.eita.castanhadora.databinding.ListitemEquipamentoBinding;
import br.coop.eita.castanhadora.db.models.Equipamento;
//import br.coop.eita.castanhadora.db.models.derived.EquipamentoETipo;
import br.coop.eita.castanhadora.ui.activities.EquipamentosListActivity;

public class EquipamentoAdapter extends ArrayAdapter<Equipamento> {
    private List<Equipamento> equipamentos;
    private EquipamentosListActivity activity;

    public EquipamentoAdapter(Context context, List<Equipamento> objects) {
        super(context, 0, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ListitemEquipamentoBinding binding;

        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.listitem_equipamento, null);
            binding = DataBindingUtil.bind(convertView);
            convertView.setTag(binding);
        } else {
            binding = (ListitemEquipamentoBinding) convertView.getTag();
        }

        final Equipamento equipamento = getItem(position);
        final EquipamentoAdapter self = this;

        binding.setEqtipo(equipamento);

        binding.editBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.openEditEquipamentoForm(equipamento);
            }
        });

        binding.removeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.openDeleteEquipamentoDialog(equipamento);
            }
        });

        return binding.getRoot();
    }

    public void setActivity(EquipamentosListActivity activity) {
        this.activity = activity;
    }
}
