package br.coop.eita.castanhadora.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import java.util.List;

import br.coop.eita.castanhadora.R;
import br.coop.eita.castanhadora.databinding.ActivityUpfDadosPessoaisBinding;
import br.coop.eita.castanhadora.db.DataRepository;
import br.coop.eita.castanhadora.ui.adapters.UpfIntegranteAdapter;
import br.coop.eita.castanhadora.ui.util.Wizardable;
import br.coop.eita.castanhadora.viewmodels.UpfViewModel;

public class UpfDadosPessoaisActivity extends AppCompatActivity implements Wizardable {

    public static final int CRIAR_INTEGRANTE_UPF_ID_REPLY = 1;
    public static final int EDITAR_INTEGRANTE_UPF_ID_REPLY = 2;


    private UpfViewModel mUpfViewModel;
    private UpfIntegranteAdapter upfIntegranteAdapter;

    Boolean isWizard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        isWizard = getIntent().getBooleanExtra("wizard", false);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null && !isWizard) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        final ActivityUpfDadosPessoaisBinding binding = DataBindingUtil.setContentView(this,
                R.layout.activity_upf_dados_pessoais);

        mUpfViewModel = ViewModelProviders.of(this).get(UpfViewModel.class);

        binding.setViewmodel(mUpfViewModel);

        //gênero
        ArrayAdapter<CharSequence> generoAdapter = ArrayAdapter.createFromResource(this,
                R.array.upf_integrante_genero, android.R.layout.simple_spinner_item);
        generoAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.inputGenero.setAdapter(generoAdapter);
        binding.inputGenero.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int pos, long l) {
                        String item = parent.getItemAtPosition(pos).toString();
                        mUpfViewModel.getUpf().setGeneroUsuario(item);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });
        Integer positionGenero = generoAdapter.getPosition(
                mUpfViewModel.getUpf().getGeneroUsuario());
        positionGenero = positionGenero == null ? 0 : positionGenero;
        binding.inputGenero.setSelection(positionGenero);

        binding.setWizard(isWizard);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!isWizard) {
            getMenuInflater().inflate(R.menu.menu_form, menu);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                mUpfViewModel.reloadUpf();
                finish();
                return true;
            case R.id.action_save:
                final UpfDadosPessoaisActivity self = this;
                saveUpf(new AfterSaveListener() {
                    @Override
                    public void doAfterSave(Integer id) {
                        mUpfViewModel.saveMainIntegrante(new DataRepository.OnSaveEntityListener() {
                            @Override
                            public void onSuccess(Integer... ids) {
                                Intent replyIntent2 = new Intent();
                                setResult(RESULT_OK, replyIntent2);
                                Toast.makeText(self, R.string.saved_dados_pessoais, Toast.LENGTH_LONG).show();
                                finish();
                            }

                            @Override
                            public void onFailure(int errorCode, List<String> errorMessages) {
                                Toast.makeText(self, R.string.error_db_could_not_save, Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                });
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private interface AfterSaveListener {
        void doAfterSave(Integer id);
    }

    @Override
    public void onClickWizardPrevious(View view) {
        Intent intent = new Intent(this, SafraFormActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        intent.putExtra("wizard", true);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        finish();
    }

    @Override
    public void onClickWizardNext(View view) {
        final UpfDadosPessoaisActivity self = this;
        saveUpf(new AfterSaveListener() {
            @Override
            public void doAfterSave(Integer id) {
                mUpfViewModel.saveMainIntegrante(new DataRepository.OnSaveEntityListener() {
                    @Override
                    public void onSuccess(Integer... ids) {
                        Intent intent = new Intent(self, UpfIntegranteListActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                        intent.putExtra("wizard", true);
                        startActivity(intent);
                        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                        finish();
                    }

                    @Override
                    public void onFailure(int errorCode, List<String> errorMessages) {
                        Toast.makeText(self, R.string.error_db_could_not_save, Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }

    public void saveUpf(final AfterSaveListener afterSaveListener) {
        final UpfDadosPessoaisActivity self = this;
        if (mUpfViewModel.getUpf().dadosUsuarioCompletos()) {
            mUpfViewModel.saveUpf(new DataRepository.OnSaveEntityListener() {
                @Override
                public void onSuccess(Integer... ids) {
                    afterSaveListener.doAfterSave(ids[0]);
                }

                @Override
                public void onFailure(int errorCode, List<String> errorMessages) {
                    Toast.makeText(self, errorMessages.get(0), Toast.LENGTH_LONG).show();
                }
            });
        } else {
            Toast.makeText(self, R.string.error_val_upf_dados_pessoais_empty, Toast.LENGTH_LONG).show();
        }
    }
}
