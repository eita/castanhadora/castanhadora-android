package br.coop.eita.castanhadora.ui.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.provider.Settings;
import android.util.Log;

import androidx.core.content.FileProvider;
import androidx.core.content.res.ResourcesCompat;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.ApolloClient;
import com.apollographql.apollo.api.CustomTypeAdapter;
import com.apollographql.apollo.api.CustomTypeValue;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;


import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.coop.eita.castanhadora.R;
import br.coop.eita.castanhadora.db.models.Despesa;
import br.coop.eita.castanhadora.db.models.Equipamento;
import br.coop.eita.castanhadora.db.models.Producao;
import br.coop.eita.castanhadora.db.models.Safra;
import br.coop.eita.castanhadora.db.models.TrabalhoUpf;
import br.coop.eita.castanhadora.db.models.TrabalhoUpfIntegrante;
import br.coop.eita.castanhadora.db.models.Upf;
import br.coop.eita.castanhadora.db.models.UpfIntegrante;
import br.coop.eita.castanhadora.db.models.Venda;
import br.coop.eita.castanhadora.db.models.derived.TotalGeral;
import br.coop.eita.castanhadora.db.models.derived.TrabalhoEIntegrantesUpf;
import br.coop.eita.castanhadora.db.models.derived.UpfIntegranteTrabalha;
import br.coop.eita.castanhadora.util.Util;
import br.coop.eita.castanhadora.viewmodels.DespesaViewModel;
import br.coop.eita.castanhadora.viewmodels.EquipamentoViewModel;
import br.coop.eita.castanhadora.viewmodels.ProducaoViewModel;
import br.coop.eita.castanhadora.viewmodels.TrabalhoUpfViewModel;
import br.coop.eita.castanhadora.viewmodels.UpfIntegranteViewModel;
import br.coop.eita.castanhadora.viewmodels.VendaViewModel;
import castanhadora.UpdateCreateUPFMutation;
import castanhadora.type.CustomType;
import castanhadora.type.DespesaInput;
import castanhadora.type.EquipamentoInput;
import castanhadora.type.ProducaoInput;
import castanhadora.type.SafraInput;
import castanhadora.type.TrabalhoUpfInput;
import castanhadora.type.TrabalhoUpfIntegranteInput;
import castanhadora.type.UpfInput;
import castanhadora.type.UpfIntegranteInput;
import castanhadora.type.VendaInput;

import static android.content.Context.LOCATION_SERVICE;

public class SafraSummaryBuilder {
    private Context context;
    private TotalGeral totalGeral;
    private Upf upf;
    private Safra safra;
    private Boolean includeValues;

    public SafraSummaryBuilder(Context context, TotalGeral totalGeral, Upf upf, Safra safra) {
        this.context = context;
        this.totalGeral = totalGeral;
        this.upf = upf;
        this.safra = safra;
    }

    public void setIncludeValues(Boolean includeValues) {
        this.includeValues = includeValues;
    }


    private BitmapDrawable getSummaryDrawable(Boolean includeValues) {
        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inScaled = false;

        Float ratioX = 2.747f;
        Float ratioY = 2.747f;

        Bitmap bm = BitmapFactory.decodeResource(context.getResources(), R.drawable.resumo_safra_2, opts)
                .copy(Bitmap.Config.ARGB_8888, true);

        Typeface tf = Typeface.create("Helvetica", Typeface.BOLD);

        Paint paint1 = new Paint();
        paint1.setStyle(Paint.Style.FILL);
        paint1.setColor(context.getResources().getColor(R.color.wheatLight));
        paint1.setTypeface(tf);
        paint1.setTextAlign(Paint.Align.LEFT);
        paint1.setTextSize(60);

        Typeface tf3 = ResourcesCompat.getFont(context, R.font.yanone_kaffeesatz_bold);

        Paint paint3 = new Paint();
        paint3.setStyle(Paint.Style.FILL);
        paint3.setColor(Color.WHITE);
        paint3.setTypeface(tf3);
        paint3.setTextAlign(Paint.Align.LEFT);
        paint3.setTextSize(42);

        Paint paint4 = new Paint();
        paint4.setStyle(Paint.Style.FILL);
        paint4.setColor(Color.WHITE);
        paint4.setTypeface(tf3);
        paint4.setTextAlign(Paint.Align.LEFT);
        paint4.setTextSize(38);

        Paint paint5 = new Paint();
        paint5.setStyle(Paint.Style.FILL);
        paint5.setColor(Color.WHITE);
        paint5.setTypeface(tf3);
        paint5.setTextAlign(Paint.Align.RIGHT);
        paint5.setTextSize(38);

        Paint paint6 = new Paint();
        paint6.setStyle(Paint.Style.FILL);
        paint6.setColor(ResourcesCompat.getColor(context.getResources(), R.color.wheatDark, context.getTheme()));

        //Rect textRect = new Rect();
        //paint1.getTextBounds(text, 0, text.length(), textRect);

        Canvas canvas = new Canvas(bm);

        Integer posX = (int) (310 / ratioX);

        canvas.drawText(context.getResources().getString(R.string.safra_summary_title, safra.getAno().toString()), posX, (int) (520 / ratioY), paint1);
        drawUserName(canvas, ratioX, ratioY);
        canvas.drawText(getProducaoMessage(totalGeral), posX, (int) (1100/ratioY), paint3);

        //Gastos
        if (totalGeral.getValorTotalVendido()
                .subtract(totalGeral.getValorDepreciacao().add(totalGeral.getValorTotalDespesas()))
                .floatValue() >= 0
        ) {
            canvas.drawText(context.getResources().getString(R.string.safra_summary_gastos_cobertos), posX, (int) (1940/ratioY), paint3);
            canvas.drawBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.saldo_ok), (int) (2055/ratioX), (int) (1750/ratioY), paint1);
        } else {
            canvas.drawRect((int) (267/ratioX), (int) (1815/ratioY), (int) (1935/ratioX), (int) (1976/ratioY), paint6);
            canvas.drawBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.saldo_nok), (int) (2055/ratioX), (int) (1770/ratioY), paint1);
            canvas.drawText(context.getResources().getString(R.string.safra_summary_gastos_nao_cobertos), posX, (int) (1940/ratioY), paint3);
        }

        //Remuneração
        if (Util.deveInserirRemuneracao(safra)) {
            if (totalGeral.getValorTotalVendido().subtract(totalGeral.getValorTotalAAlcancar()).floatValue() >= 0) {
                canvas.drawText(context.getResources().getString(R.string.safra_summary_meta_remuneracao_coberta), posX, (int) (2340/ratioY), paint3);
                canvas.drawBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.saldo_ok), (int) (2295/ratioX), (int) (2165/ratioY), paint1);
            } else {
                canvas.drawRect((int) (261/ratioX), (int) (2213/ratioY), (int) (2175/ratioX), (int) (2373/ratioY), paint6);
                canvas.drawBitmap(BitmapFactory.decodeResource(context.getResources(), R.drawable.saldo_nok), (int) (2295/ratioX), (int) (2165/ratioY), paint1);
                canvas.drawText(context.getResources().getString(R.string.safra_summary_meta_remuneracao_nao_coberta), posX, (int) (2340/ratioY), paint3);
            }
        } else {
            canvas.drawText(context.getResources().getString(R.string.safra_summary_meta_remuneracao_nao_contabilizada), posX, (int) (2340/ratioY), paint3);
        }

        //Porcentagem: vendido e estoque
        Integer baseProdLeft = (int) (277/ratioX);
        Integer baseProdRight = (int) (2576/ratioX);
        Integer baseProdWidth = baseProdRight - baseProdLeft;
        Integer estoqueWidth = totalGeral.getQtdeProduzido() > 0 ?
                baseProdWidth * totalGeral.getQtdeEstoque() / totalGeral.getQtdeProduzido() :
                0;

        Integer textMargin = (int) (40/ratioX);

        if (totalGeral.getQtdeProduzido() - totalGeral.getQtdeVendido() > 0) {
            canvas.drawRect(baseProdLeft + baseProdWidth - estoqueWidth, (int) (1201/ratioY), (int) (2576/ratioX), (int) (1329/ratioY), paint6);
            canvas.drawText(totalGeral.getQtdeEstoque().toString(), baseProdRight - textMargin, (int) (1300/ratioY), paint5);
        }

        if (totalGeral.getQtdeVendido() > 0) {
            String textoVendido = totalGeral.getQtdeVendido().toString();
            if (includeValues) {
                textoVendido += " (" + Util.formatMoney(totalGeral.getValorTotalVendido()) + ")";
            }
            canvas.drawText(textoVendido, baseProdLeft + textMargin, (int) (1300/ratioY), paint4);
        }


        return new BitmapDrawable(context.getResources(), bm);
    }

    private String getProducaoMessage(TotalGeral totalGeral) {
        String message;
        if (totalGeral.getQtdeProduzido() == 0) {
            message = context.getResources().getString(R.string.safra_summary_producao0);
        /*} else if (totalGeral.getQtdeProduzido() == 1) {
            message = context.getResources().getString(R.string.safra_summary_producao, totalGeral.getQtdeProduzido()); */
        } else {
            message = context.getResources().getString(R.string.safra_summary_producao, totalGeral.getQtdeProduzido(), upf.getUnidadeValor());
        }
        return message;
    }

    private static int convertToPixels(Context context, int nDP) {
        final float conversionScale = context.getResources().getDisplayMetrics().density;
        return (int) ((nDP * conversionScale) + 0.5f);
    }

    private void drawUserName(Canvas canvas, Float ratioX, Float ratioY) {
        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(context.getResources().getColor(R.color.wheatLight));
        paint.setTypeface(Typeface.create("Helvetica", Typeface.BOLD));
        paint.setTextAlign(Paint.Align.LEFT);
        paint.setTextSize(35);

        Paint mPaint = new Paint();
        mPaint.setColor(Color.rgb(19, 80, 38));
        mPaint.setStrokeWidth(48);
        mPaint.setStyle(Paint.Style.STROKE);
        Path path = new Path();

        String nomeUsuario = upf.getNomeUsuario();
        int nextPos = paint.breakText(nomeUsuario.toCharArray(), 0, nomeUsuario.length(), 380, null);

        if (nextPos < nomeUsuario.length()) {
            int index = nomeUsuario.indexOf(" ");
            while (nomeUsuario.indexOf(" ", index + 1) <= nextPos) {
                index = nomeUsuario.indexOf(" ", index + 1);
            }

            path.moveTo((int) (280/ratioX), (int) (700 / ratioY));
            path.lineTo((int) (280/ratioX) + 20 + paint.measureText(nomeUsuario.substring(0, index)), (int) (700 / ratioY));
            canvas.drawPath(path, mPaint);
            canvas.drawTextOnPath(nomeUsuario.substring(0, index), path, 10, 12, paint);

            Path pathLine2 = new Path();
            pathLine2.moveTo((int) (280/ratioX), (int) (830 / ratioY));
            pathLine2.lineTo((int) (280/ratioX) + 20 + paint.measureText(nomeUsuario.substring(index + 1)), (int) (830 / ratioY));
            canvas.drawPath(pathLine2, mPaint);
            canvas.drawTextOnPath(nomeUsuario.substring(index + 1), pathLine2, 10, 12, paint);

        } else {
            path.moveTo((int) (280/ratioX), (int) (700 / ratioY));
            path.lineTo((int) (280/ratioX) + 20 + paint.measureText(nomeUsuario), (int) (700 / ratioY));
            canvas.drawPath(path, mPaint);
            canvas.drawTextOnPath(nomeUsuario, path, 10, 12, paint);
        }

    }

    public static String shortenName(String name, int maxLength) {
        if (name == null || name.length() <= maxLength) {
            return name;
        }

        return name.substring(0, maxLength) + ".";
    }

    /**
     * Saves the image as PNG to the app's cache directory.
     *
     * @param image Bitmap to save.
     * @return Uri of the saved file or null
     */
    private Uri saveImage(Bitmap image) {
        //TODO - Should be processed in another thread
        File imagesFolder = new File(context.getCacheDir(), "images");
        Uri uri = null;
        try {
            imagesFolder.mkdirs();
            File file = new File(imagesFolder, "shared_image.png");

            FileOutputStream stream = new FileOutputStream(file);
            image.compress(Bitmap.CompressFormat.PNG, 90, stream);
            stream.flush();
            stream.close();
            uri = FileProvider.getUriForFile(context, "br.coop.eita.castanhadora", file);

        } catch (IOException e) {
            Log.d("CASTANHADORA", "IOException while trying to write file for sharing: " + e.getMessage());
        }
        return uri;
    }

    public Uri getImageUri() {
        Bitmap bitmap = getSummaryDrawable(includeValues).getBitmap();
        return saveImage(bitmap);
    }

    /**
     * Shares the PNG image from Uri.
     *
     * @param uri Uri of image to share.
     */
    public void shareImageUri(Uri uri) {
        Intent intent = new Intent(android.content.Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_STREAM, uri);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.setType("image/png");
        context.startActivity(intent);
    }

    public void sincSafra(EquipamentoViewModel equipamentoViewModel, DespesaViewModel despesaViewModel,
                          VendaViewModel vendaViewModel, ProducaoViewModel producaoViewModel, TrabalhoUpfViewModel trabalhoUpfViewModel,
                          UpfIntegranteViewModel upfIntegranteViewModel) {

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        CustomTypeAdapter<Date> dateCustomTypeAdapter = new CustomTypeAdapter<Date>() {
            @Override
            public Date decode(@NotNull CustomTypeValue<?> customTypeValue) {
                try {
                    return format.parse(customTypeValue.toString());
                } catch (ParseException e) {
                    e.printStackTrace();
                    return null;
                }
            }

            @NotNull
            @Override
            public CustomTypeValue<?> encode(Date date) {
                    return CustomTypeValue.GraphQLString.fromRawValue(format.format(date));

            }
        };

        ApolloClient apolloClient = ApolloClient.builder()
                .serverUrl("https://castanhadora.eita.coop.br/graphql/")
//                .serverUrl("https://llagocarvalho2.pythonanywhere.com/graphql/")
                .addCustomTypeAdapter(CustomType.DATE, dateCustomTypeAdapter)
                .build();

        List<EquipamentoInput> equipamentosInput = new ArrayList<>();
        List<DespesaInput> despesasInput = new ArrayList<>();
        List<VendaInput> vendasInput = new ArrayList<>();
        List<ProducaoInput> producoesInput = new ArrayList<>();
        List<TrabalhoUpfInput> trabalhosUpfInput = new ArrayList<>();
        List<UpfIntegranteInput> upfIntegrantesInput = new ArrayList<>();
        List<TrabalhoUpfIntegranteInput> trabalhoUpfIntegrantesInput = new ArrayList<>();

        for (Equipamento equipamento : equipamentoViewModel.getEquipamentos(safra.getId())) {

            EquipamentoInput equipamentoInput = EquipamentoInput.builder()
                    .id(equipamento.getId())
                    .tipo(equipamento.getTipo())
                    .marcaEModelo(equipamento.getMarcaEModelo())
                    .anoFabricacao(equipamento.getAnoFabricacao())
                    .anoCompra(equipamento.getAnoCompra())
                    .quantidade(equipamento.getQuantidade())
                    .valorCompra(equipamento.getValorCompra() != null ? equipamento.getValorCompra().doubleValue() : null)
                    .durabilidadeEstimadaAnos(equipamento.getDurabilidadeEstimadaAnos())
                    .usado(equipamento.getUsado())
                    .safraId(equipamento.getSafraId())
                    .build();
            equipamentosInput.add(equipamentoInput);
        }

        List<Despesa> despesas = despesaViewModel.getDespesasBySafraId(safra.getId());
        for (Despesa despesa : despesas != null ? despesas : new ArrayList<Despesa>()) {
            DespesaInput despesaInput = DespesaInput.builder()
                    .id(despesa.getId())
                    .tipo(despesa.getTipo())
                    .valor(despesa.getValor() != null ? despesa.getValor().doubleValue() : null)
                    .data(despesa.getData())
                    .observacoes(despesa.getObservacoes())
                    .aviamento(despesa.getAviamento())
                    .diariaDias(despesa.getDiariaDias() != null ? despesa.getDiariaDias().doubleValue() : null)
                    .diariaQuantidadeDiaristas(despesa.getDiariaQuantidadeDiaristas())
                    .diariaValorDiaria(despesa.getDiariaValorDiaria() != null ? despesa.getDiariaValorDiaria().doubleValue() : null)
                    .safraId(despesa.getSafraId())
                    .build();
            despesasInput.add(despesaInput);
        }

        List<Venda> vendas = vendaViewModel.getVendasBySafraId(safra.getId());
        for (Venda venda : vendas != null ? vendas : new ArrayList<Venda>()) {
            VendaInput vendaInput = VendaInput.builder()
                    .id(venda.getId())
                    .quantidade(venda.getQuantidade())
                    .valorUnitario(venda.getValorUnitario() != null ? venda.getValorUnitario().doubleValue() : null)
                    .data(venda.getData())
                    .observacoes(venda.getObservacoes())
                    .safraId(venda.getSafraId())
                    .compradorId(venda.getCompradorId())
                    .comprador(venda.getComprador())
                    .quitacaoAviamento(venda.getQuitacaoAviamento())
                    .build();
            vendasInput.add(vendaInput);
        }

        List<Producao> producoes = producaoViewModel.getProducoesBySafraId(safra.getId());
        for (Producao producao : producoes != null ? producoes : new ArrayList<Producao>()) {

            ProducaoInput producaoInput = ProducaoInput.builder()
                    .id(producao.getId())
                    .data(producao.getData())
                    .quantidade(producao.getQuantidade())
                    .safraId(producao.getSafraId())
                    .observacoes(producao.getObservacoes())
                    .build();
            producoesInput.add(producaoInput);
        }

        List<TrabalhoEIntegrantesUpf> trabalhoEIntegrantesUpfs = trabalhoUpfViewModel.getTrabalhosEIntegrantesUpfBySafraId(safra.getId());
        for (TrabalhoEIntegrantesUpf trabalhoEIntegrantesUpf : trabalhoEIntegrantesUpfs != null ? trabalhoEIntegrantesUpfs : new ArrayList<TrabalhoEIntegrantesUpf>()) {
            TrabalhoUpf trabalhoUpf = trabalhoEIntegrantesUpf.getTrabalhoUpf();
            TrabalhoUpfInput trabalhoUpfInput = TrabalhoUpfInput.builder()
                    .id(trabalhoUpf.getId())
                    .data(trabalhoUpf.getData())
                    .dias(trabalhoUpf.getDias() != null ? trabalhoUpf.getDias().doubleValue() : null)
                    .remuneracaoDesejada(trabalhoUpf.getRemuneracaoDesejada() != null ? trabalhoUpf.getRemuneracaoDesejada().doubleValue() : null)
                    .observacoes(trabalhoUpf.getObservacoes())
                    .safraId(trabalhoUpf.getSafraId())
                    .build();
            trabalhosUpfInput.add(trabalhoUpfInput);

            List<TrabalhoUpfIntegrante> TrabalhoUpfIntegrantes = trabalhoUpfViewModel.getIntegrantesByTrabalhoId(trabalhoUpf.getId());
            for (TrabalhoUpfIntegrante trabalhoUpfIntegrante : TrabalhoUpfIntegrantes != null ? TrabalhoUpfIntegrantes : new ArrayList<TrabalhoUpfIntegrante>()) {
                TrabalhoUpfIntegranteInput trabalhoUpfIntegranteInput = TrabalhoUpfIntegranteInput.builder()
                        .id(null)
                        .trabalhoUpfId(trabalhoUpfIntegrante.getTrabalhoUpfId())
                        .upfIntegranteId(trabalhoUpfIntegrante.getUpfIntegranteId())
                        .build();

                trabalhoUpfIntegrantesInput.add(trabalhoUpfIntegranteInput);
            }
        }

        for (UpfIntegrante upfIntegrante : upfIntegranteViewModel.getUpfIntegrantes(safra.getId())) {

            UpfIntegranteInput upfIntegranteInput = UpfIntegranteInput.builder()
                    .id(upfIntegrante.getId())
                    .nome(upfIntegrante.getNome())
                    .dataNascimento(upfIntegrante.getDataNascimento())
                    .safraId(upfIntegrante.getSafraId())
                    .mainIntegrante(upfIntegrante.isMainIntegrante())
                    .parentesco(upfIntegrante.getParentesco())
                    .genero(upfIntegrante.getGenero())
                    .build();
            upfIntegrantesInput.add(upfIntegranteInput);
        }
        Location location = getLocation();
        Double latitude = null;
        Double longitude = null;
        if (location != null) {
            latitude = location.getLatitude();
            longitude = location.getLongitude();
        }

        SafraInput safraInput = SafraInput.builder()
                .id(safra.getId())
                .nome(safra.getNome())
                .dataInicio(safra.getDataInicio())
                .dataFinal(safra.getDataFinal())
                .ativa(safra.getAtiva())
                .remuneracaoDesejadaPorDia(safra.getRemuneracaoDesejadaPorDia() != null ? safra.getRemuneracaoDesejadaPorDia().doubleValue() : null)
                .castanhalTamanho(safra.getCastanhalTamanho() != null ? safra.getCastanhalTamanho().doubleValue() : null)
                .castanhalNumeroCastanheiras(safra.getCastanhalNumeroCastanheiras())
                .castanhalNumeroFamilias(safra.getCastanhalNumeroFamilias())
                .upfId(safra.getUpfId())
                .municipioId(safra.getMunicipioId())
                .ufId(safra.getUfId())
                .simulacao(safra.getSimulacao())
                .localizacaoLat(latitude)
                .localizacaoLong(longitude)
                .despesaList(despesasInput)
                .equipamentoList(equipamentosInput)
                .vendaList(vendasInput)
                .producaoList(producoesInput)
                .trabalhoUpfList(trabalhosUpfInput)
                .upfIntegranteList(upfIntegrantesInput)
                .trabalhoUpfIntegranteList(trabalhoUpfIntegrantesInput)
                .build();

        Activity activity = (Activity) context;

        SharedPreferences prefs=activity.getSharedPreferences("api_pref",Context.MODE_PRIVATE);
        String mobileKey = prefs.getString("mobile_key","");

        UpfInput upfInput = UpfInput.builder()
                .mobileKey(mobileKey)
                .id(upf.getId())
                .nomeComunidade(upf.getNomeComunidade())
                .nomeUsuario(upf.getNomeUsuario())
                .dataNascimentoUsuario(upf.getDataNascimentoUsuario())
                .generoUsuario(upf.getGeneroUsuario())
                .unidadeArea(upf.getUnidadeArea())
                .unidadeValor(upf.getUnidadeValor())
                .cooperativa(upf.getCooperativa())
                .safra(safraInput)
                .build();


        apolloClient.mutate(new UpdateCreateUPFMutation(upfInput)).enqueue(new ApolloCall.Callback<UpdateCreateUPFMutation.Data>() {
            @Override
            public void onResponse(@NotNull Response<UpdateCreateUPFMutation.Data> response) {
                SharedPreferences preferences = activity.getSharedPreferences("api_pref", Context.MODE_PRIVATE);
                if (response.getData().createUpf() != null && response.getData().createUpf().mobileKey() != null) {
                    preferences.edit().putString("mobile_key", response.getData().createUpf().mobileKey()).apply();
                }

                activity.runOnUiThread(new Runnable() {
                    public void run() {
                        new androidx.appcompat.app.AlertDialog.Builder(activity)
                                .setMessage(R.string.dialog_share_safra_thanks)
                                .setNegativeButton(R.string.label_dialog_ok, null)
                                .setTitle(R.string.dialog_share_safra_thanks_title)
                                .show();
                    }
                });
                Log.e("Apollo", "Launch site: " + response.getData());
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
                activity.runOnUiThread(new Runnable() {
                    public void run() {
                        new androidx.appcompat.app.AlertDialog.Builder(activity)
                                .setMessage(R.string.dialog_share_safra_fail)
                                .setNegativeButton(R.string.label_dialog_ok, null)
                                .setTitle(R.string.dialog_share_safra_fail_title)
                                .show();
                    }
                });
                Log.e("Apollo", "Error", e);
            }
        });
    }

    //Get location
    public Location getLocation(){
        LocationManager locationManager = (LocationManager) context.getSystemService(LOCATION_SERVICE);
        Location myLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if (myLocation == null)
        {
            myLocation = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);
        }
        return myLocation;
    }
}
