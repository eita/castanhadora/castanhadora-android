package br.coop.eita.castanhadora.ui.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import java.util.List;

import br.coop.eita.castanhadora.R;
import br.coop.eita.castanhadora.databinding.ActivitySafrasListBinding;
import br.coop.eita.castanhadora.databinding.HeaderSafraListBinding;
import br.coop.eita.castanhadora.db.DataRepository;
import br.coop.eita.castanhadora.db.models.Safra;
import br.coop.eita.castanhadora.ui.adapters.SafraAdapter;
import br.coop.eita.castanhadora.ui.util.SafraSummaryBuilder;
import br.coop.eita.castanhadora.viewmodels.DespesaViewModel;
import br.coop.eita.castanhadora.viewmodels.EquipamentoViewModel;
import br.coop.eita.castanhadora.viewmodels.ProducaoViewModel;
import br.coop.eita.castanhadora.viewmodels.SafraViewModel;
import br.coop.eita.castanhadora.viewmodels.TrabalhoUpfViewModel;
import br.coop.eita.castanhadora.viewmodels.UpfIntegranteViewModel;
import br.coop.eita.castanhadora.viewmodels.VendaViewModel;

public class SafraListActivity extends AppCompatActivity {

    public static final int CRIAR_SAFRA_ACTIVITY_REQUEST_CODE = 1;
    public static final int EDITAR_SAFRA_ACTIVITY_REQUEST_CODE = 2;


    private SafraViewModel viewModel;

    private SafraAdapter safraAdapter;

    private SafraSummaryBuilder safraSummaryBuilder;

    Boolean parentSafraInicio = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final ActivitySafrasListBinding binding = DataBindingUtil.setContentView(this,
                R.layout.activity_safras_list);

        viewModel = ViewModelProviders.of(this).get(SafraViewModel.class);
        binding.setViewmodel(viewModel);

        parentSafraInicio = getIntent().getBooleanExtra("parent_safra", false);
        Integer safraIdFromEncerrar = getIntent().getIntExtra("safra_id_from_encerrar", -1);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            if (parentSafraInicio) {
                actionBar.setDisplayHomeAsUpEnabled(true);
            }
            actionBar.setTitle(R.string.label_safras);
        }

        LiveData<List<Safra>> safras = viewModel.getSafrasLive();

        final SafraListActivity self = this;

        HeaderSafraListBinding headerSafraListBinding = DataBindingUtil.inflate(getLayoutInflater(),
                R.layout.header_safra_list, binding.safraList, false);

        binding.safraList.addHeaderView(headerSafraListBinding.getRoot());

        headerSafraListBinding.btnSafraCriar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SafraListActivity.this, SafraFormActivity.class);
                startActivityForResult(intent, CRIAR_SAFRA_ACTIVITY_REQUEST_CODE);
            }
        });

        safras.observe(this, new Observer<List<Safra>>() {
            @Override
            public void onChanged(List<Safra> ssafras) {
                if (ssafras != null) {
                    safraAdapter = new SafraAdapter(getApplicationContext(), ssafras, viewModel);
                    binding.safraList.setAdapter(safraAdapter);
                    safraAdapter.setActivity(self);
                    safraAdapter.notifyDataSetChanged();
                }
            }
        });

        if (safraIdFromEncerrar > 0) {
            askWantToShare(viewModel.getSafraById(safraIdFromEncerrar));
        }

    }

    public void openEditForm(Safra safra) {
        Intent intent;

        intent = new Intent(this, SafraFormActivity.class);

        intent.putExtra("safra_id", safra.getId());
        startActivityForResult(intent, EDITAR_SAFRA_ACTIVITY_REQUEST_CODE);
    }

    public void openDeleteDialog(final Safra safra) {
        if (viewModel.getSafraAtiva() != null && viewModel.getSafraAtiva().getId() == safra.getId()) {
            Toast.makeText(this, R.string.nao_pode_deletar_safra_ativa, Toast.LENGTH_LONG).show();
            return;
        }
        final SafraListActivity self = this;
        new AlertDialog.Builder(this).setMessage(
                getResources().getString(R.string.msg_dialog_safra_remover,
                        safra.getNome())).setPositiveButton(R.string.dialog_sim,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        viewModel.deleteSafra(safra, new DataRepository.OnDeleteEntityListener() {
                            @Override
                            public void onSuccess(Integer... ids) {
                                Toast.makeText(self, R.string.deleted_safra, Toast.LENGTH_LONG).show();
                            }

                            @Override
                            public void onFailure(int errorCode, List<String> errorMessages) {
                                Toast.makeText(self, R.string.deleted_safra_failure, Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                }).setNegativeButton(R.string.dialog_nao, null).setTitle(
                R.string.label_dialog_safra_delete_title).show();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent replyIntent = new Intent();
                setResult(RESULT_CANCELED, replyIntent);
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void ativarSafra(Safra safra) {
        viewModel.ativarSafra(safra.getId(), new DataRepository.OnSafraAtivaListener() {
            @Override
            public void onSuccess() {
                finalizaEInicializaSafraInicio();
            }

            @Override
            public void onFailure() {

            }
        });
    }

    public void shareSafraClick(final Safra safra) {
        if (viewModel.getUpf().getCompartilharSafra()) {
            shareSafraSummary(safra);
        } else {
            changeSharePermissonConfig();
        }
    }

    public void changeSharePermissonConfig() {
        final SafraListActivity self = this;
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        Intent intent = new Intent(self, PermissaoCompartilharNuvemActivity.class);
                        startActivity(intent);
                }
            }
        };

        androidx.appcompat.app.AlertDialog.Builder alertDialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(this);
        alertDialogBuilder.setMessage(getString(R.string.dialog_sem_permissao_compartilhar))
                .setPositiveButton(getString(R.string.dialog_alterar_configuracao), dialogClickListener)
                .setNegativeButton(getString(R.string.label_dialog_cancel), dialogClickListener)
                .show();
    }

    public void shareSafraSummary(final Safra safra) {
        safraSummaryBuilder = new SafraSummaryBuilder(this, viewModel.getTotalGeral(safra.getId()), viewModel.getUpf(), safra);
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        safraSummaryBuilder.setIncludeValues(true);
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        safraSummaryBuilder.setIncludeValues(false);
                        break;
                    case DialogInterface.BUTTON_NEUTRAL:
                        return;
                }
                sincronizarNuvem();
                safraSummaryBuilder.shareImageUri(safraSummaryBuilder.getImageUri());
            }
        };

        androidx.appcompat.app.AlertDialog.Builder alertDialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(this);
        alertDialogBuilder
                .setTitle(getString(R.string.dialog_share_safra_title))
                .setMessage(getString(R.string.dialog_share_safra) + "\n\n" + getString(R.string.dialog_share_safra2) + "\n\n" + getString(R.string.dialog_include_value))
                .setNeutralButton(getString(R.string.label_dialog_cancel), dialogClickListener)
                .setPositiveButton(getString(R.string.dialog_share_include_value_positive), dialogClickListener)
                .setNegativeButton(getString(R.string.dialog_share_include_value_negative), dialogClickListener)
                .show();
    }

    public void askWantToShare(final Safra safra) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        shareSafraClick(safra);
                        break;
                    case DialogInterface.BUTTON_NEGATIVE:
                        return;
                }
            }
        };

        if (viewModel.getUpf().getCompartilharSafra()) {
            androidx.appcompat.app.AlertDialog.Builder alertDialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(this);
        alertDialogBuilder
                .setTitle(getString(R.string.safra_encerrada_sem_nome))
                .setMessage(getString(R.string.dialog_ask_share_safra))
                .setPositiveButton(getString(R.string.dialog_ask_share_safra_sim), dialogClickListener)
                .setNegativeButton(getString(R.string.dialog_ask_share_safra_nao), dialogClickListener)
                .show();
        } else {
            androidx.appcompat.app.AlertDialog.Builder alertDialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(this);
            alertDialogBuilder
                    .setTitle(getString(R.string.safra_encerrada_sem_nome))
                    .setMessage(getString(R.string.dialog_safra_encerrada_msg))
                    .setNegativeButton(getString(R.string.label_dialog_ok), null)
                    .show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Safra.RESULT_SAFRA_ATIVA_ALTERADA) {
            finalizaEInicializaSafraInicio();
        }
    }

    protected void finalizaEInicializaSafraInicio() {
        if (!parentSafraInicio) {
            Intent intent = new Intent(this, SafraInicioActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            finish();
            startActivity(intent);
        } else {
            Intent intent = new Intent();
            setResult(Safra.RESULT_SAFRA_ATIVA_ALTERADA, intent);
            finish();
        }
    }

    private void sincronizarNuvem() {
        EquipamentoViewModel equipamentoViewModel = ViewModelProviders.of(this).get(EquipamentoViewModel.class);
        DespesaViewModel despesaViewModel = ViewModelProviders.of(this).get(DespesaViewModel.class);
        VendaViewModel vendaViewModel = ViewModelProviders.of(this).get(VendaViewModel.class);
        ProducaoViewModel producaoViewModel = ViewModelProviders.of(this).get(ProducaoViewModel.class);
        TrabalhoUpfViewModel trabalhoUpfViewModel = ViewModelProviders.of(this).get(TrabalhoUpfViewModel.class);
        UpfIntegranteViewModel upfIntegranteViewModel = ViewModelProviders.of(this).get(UpfIntegranteViewModel.class);
        safraSummaryBuilder.sincSafra(equipamentoViewModel, despesaViewModel, vendaViewModel, producaoViewModel, trabalhoUpfViewModel, upfIntegranteViewModel);
    }
}
