package br.coop.eita.castanhadora.db.models;

import android.text.TextUtils;
import android.text.format.DateUtils;

import androidx.annotation.NonNull;
import androidx.databinding.Bindable;
import androidx.databinding.library.baseAdapters.BR;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import br.coop.eita.castanhadora.Castanhadora;
import br.coop.eita.castanhadora.R;
import br.coop.eita.castanhadora.db.models.derived.TotalGeral;
import br.coop.eita.castanhadora.util.Util;

import static androidx.room.ForeignKey.CASCADE;

@Entity(tableName = Equipamento.TABLE_NAME, foreignKeys = {@ForeignKey(onDelete = CASCADE, entity = Safra.class, parentColumns = "id", childColumns = "safra_id"), @ForeignKey(entity = TipoEquipamento.class, parentColumns = "id", childColumns = "tipo_equipamento_id"),})
public class Equipamento extends BaseIdentifiableModel {
    public static final String TABLE_NAME = "equipamento";

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "tipo_str")
    private String tipo;

    @NonNull
    @ColumnInfo(name = "marca_e_modelo")
    private String marcaEModelo;

    @NonNull
    @ColumnInfo(name = "ano_fabricacao")
    private Integer anoFabricacao;

    @ColumnInfo(name = "ano_compra")
    private Integer anoCompra;

    @NonNull
    @ColumnInfo(name = "quantidade")
    private Integer quantidade;

    @NonNull
    @ColumnInfo(name = "valorCompra")
    private BigDecimal valorCompra;

    @NonNull
    @ColumnInfo(name = "durabilidade_estimada_anos")
    private Integer durabilidadeEstimadaAnos;

    @NonNull
    @ColumnInfo(name = "usado")
    private Boolean usado;

    @NonNull
    @ColumnInfo(name = "safra_id", index = true)
    public int safraId;

    @ColumnInfo(name = "tipo_equipamento_id", index = true)
    public Integer tipoEquipamentoId;

    public Equipamento() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @NonNull
    public String getMarcaEModelo() {
        return marcaEModelo;
    }

    public void setMarcaEModelo(@NonNull String marcaEModelo) {
        this.marcaEModelo = marcaEModelo;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(@NonNull String tipo) {
        this.tipo = tipo;
    }

    @NonNull
    @Bindable
    public Integer getAnoFabricacao() {
        return anoFabricacao;
    }

    public void setAnoFabricacao(@NonNull Integer anoFabricacao) {
        this.anoFabricacao = anoFabricacao;
        notifyPropertyChanged(BR.anoFabricacao);
    }

    public Integer getAnoCompra() {
        return anoCompra;
    }

    public void setAnoCompra(Integer anoCompra) {
        this.anoCompra = anoCompra;
    }

    @NonNull
    public Integer getQuantidade() {
        if (quantidade == null) {
            quantidade = 1;
        }
        return quantidade;
    }

    public void setQuantidade(@NonNull Integer quantidade) {
        if (quantidade == null) {
            quantidade = 1;
        }
        this.quantidade = quantidade;
    }

    @NonNull
    public BigDecimal getValorCompra() {
        return valorCompra;
    }

    public void setValorCompra(@NonNull BigDecimal valorCompra) {
        this.valorCompra = valorCompra;
    }

    @NonNull
    public Integer getDurabilidadeEstimadaAnos() {
        return durabilidadeEstimadaAnos;
    }

    public void setDurabilidadeEstimadaAnos(@NonNull Integer durabilidadeEstimadaAnos) {
        this.durabilidadeEstimadaAnos = durabilidadeEstimadaAnos;
    }

    public int getSafraId() {
        return safraId;
    }

    public void setSafraId(int safraId) {
        this.safraId = safraId;
    }

    @Bindable
    public Integer getTipoEquipamentoId() {
        return tipoEquipamentoId;
    }

    public void setTipoEquipamentoId(Integer tipoEquipamentoId) {
        this.tipoEquipamentoId = tipoEquipamentoId;
        notifyPropertyChanged(BR.tipoEquipamentoId);
    }

    public String getNomeCompleto() {
        //return getQuantidade().toString() + " " + getTipo() + " (" + getMarcaEModelo() + ")";
        return getTipo() + " (" + getMarcaEModelo() + ")";
    }

    public String getDescricaoCard() {

        String descricaoCard = "Produto " + (getUsado() == true ? "usado" : "novo") + " comprado em " + getAnoCompra().toString() + ".";
        if (getUsado() == true) {
            descricaoCard = descricaoCard + " Data de fabricação: " + getAnoFabricacao() + ".";
        }
        String depreciacaoFormatada = Util.formatMoney(getValorDepreciacaoAnual(Calendar.getInstance().get(Calendar.YEAR)));
        descricaoCard = " Sua vida útil é estimada em " + getDurabilidadeEstimadaAnos() + " anos (desde a fabricação), e portanto o valor de depreciação deste equipamento nesta safra é de " + depreciacaoFormatada + ".";
        return descricaoCard;
    }

    public String getSubtituloCard() {
        return Util.formatMoney(getValorCompra());
    }

    @NonNull
    @Bindable
    public Boolean getUsado() {
        return (usado == null) ? false : usado;
    }

    public void setUsado(@NonNull Boolean usado) {
        this.usado = (usado == null) ? false : usado;
        notifyPropertyChanged(BR.usado);
    }

    public String getIdadeAnos() {
        Integer anos = getAnoCompra() - getAnoFabricacao();
        return anos > 0 ? anos.toString() + " anos" : "novo";
    }

    @Override
    public List<String> validate() {
        ArrayList<String> validationErrors = new ArrayList<String>();
        if (TextUtils.isEmpty(this.marcaEModelo)) {
            validationErrors.add(
                    Castanhadora.APP.getResources().getString(R.string.error_val_equipamento_marca_e_modelo_empty));
        }
        if (this.anoFabricacao == null || this.anoFabricacao == 0) {
            validationErrors.add(
                    Castanhadora.APP.getResources().getString(R.string.error_val_equipamento_ano_fabricacao_empty));
        }
        if (this.quantidade == null || this.quantidade == 0) {
            validationErrors.add(
                    Castanhadora.APP.getResources().getString(R.string.error_val_equipamento_quantidade_empty));
        }
        if (this.valorCompra == null || this.valorCompra.floatValue() < 0.01) {
            validationErrors.add(
                    Castanhadora.APP.getResources().getString(R.string.error_val_equipamento_valor_compra_empty));
        }
        if (this.durabilidadeEstimadaAnos == null || this.durabilidadeEstimadaAnos == 0) {
            validationErrors.add(
                    Castanhadora.APP.getResources().getString(R.string.error_val_equipamento_durabilidade_empty));
        }
        /*if (this.tipoEquipamentoId == 0) {
            validationErrors.add(
                    Castanhadora.APP.getResources().getString(R.string.error_val_equipamento_tipo_empty));
        }*/ // TODO: fazer a lista de tipos de equipamento e ter novamente um dropdown. Por enquanto, equipamentoId não está sendo preenchido
        if (this.safraId == 0) {
            validationErrors.add(
                    Castanhadora.APP.getResources().getString(R.string.error_val_equipamento_safra_empty));
        }


        return validationErrors;
    }

    public BigDecimal getValorDepreciacaoAnual(Integer ano) {
        Integer anosPassados = ano - anoCompra + 1;
        if (anosPassados <= 0 || anosPassados > durabilidadeEstimadaAnos) {
            return new BigDecimal(0);
        }

        return durabilidadeEstimadaAnos > 0 ? valorCompra.divide(new BigDecimal(durabilidadeEstimadaAnos)) : new BigDecimal(0);
    }

    public Equipamento duplicar(Integer novaSafraId) {
        Equipamento novo = new Equipamento();
        novo.setSafraId(novaSafraId);
        novo.setAnoCompra(anoCompra);
        novo.setAnoFabricacao(anoFabricacao);
        novo.setUsado(usado);
        novo.setDurabilidadeEstimadaAnos(durabilidadeEstimadaAnos);
        novo.setMarcaEModelo(marcaEModelo);
        novo.setQuantidade(quantidade);
        novo.setTipo(tipo);
        novo.setTipoEquipamentoId(tipoEquipamentoId);
        novo.setValorCompra(valorCompra);
        return novo;
    }
}
