package br.coop.eita.castanhadora.db.models;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;

@Entity(tableName = TrabalhoUpfIntegrante.TABLE_NAME, primaryKeys = {"trabalho_upf_id", "upf_integrante_id"}, foreignKeys = {@ForeignKey(entity = TrabalhoUpf.class, parentColumns = "id", childColumns = "trabalho_upf_id"), @ForeignKey(entity = UpfIntegrante.class, parentColumns = "id", childColumns = "upf_integrante_id")})
public class TrabalhoUpfIntegrante extends BaseModel  {
    public static final String TABLE_NAME = "trabalho_upf_integrante";

    @ColumnInfo(name = "trabalho_upf_id", index = true)
    public int trabalhoUpfId;

    @ColumnInfo(name = "upf_integrante_id", index = true)
    public int upfIntegranteId;

    public TrabalhoUpfIntegrante() {
    }

    public TrabalhoUpfIntegrante(int trabalhoUpfId, int upfIntegranteId) {
        this.trabalhoUpfId = trabalhoUpfId;
        this.upfIntegranteId = upfIntegranteId;
    }

    public int getTrabalhoUpfId() {
        return trabalhoUpfId;
    }

    public void setTrabalhoUpfId(int trabalhoUpfId) {
        this.trabalhoUpfId = trabalhoUpfId;
    }

    public int getUpfIntegranteId() {
        return upfIntegranteId;
    }

    public void setUpfIntegranteId(int upfIntegranteId) {
        this.upfIntegranteId = upfIntegranteId;
    }
}
