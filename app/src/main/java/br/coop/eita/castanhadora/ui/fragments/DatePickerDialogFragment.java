package br.coop.eita.castanhadora.ui.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Build;
import android.os.Bundle;
import android.widget.DatePicker;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

import java.util.Calendar;
import java.util.Date;

import br.coop.eita.castanhadora.util.OnSelectedDateOrTimeCallback;

public class DatePickerDialogFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    public Date initDate;

    public OnSelectedDateOrTimeCallback callback;

    public Boolean yearFirst;

    public DatePickerDialogFragment(Date initDate, Boolean yearFirst, OnSelectedDateOrTimeCallback callback) {
        this.initDate = initDate;
        this.callback = callback;
        this.yearFirst = yearFirst;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        Date startDate = initDate;
        if (startDate == null) startDate = new Date();

        final Calendar cal = Calendar.getInstance();
        cal.setTime(startDate);


        // Use the current date as the default date in the picker
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);

        // Create a new instance of DatePickerDialog and return it
        DatePickerDialog dialog = new DatePickerDialog(getActivity(), this, year, month, day);

        if (yearFirst) {
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                //Pre Lollipop
                dialog.getDatePicker().getTouchables().get(1).performClick();
            } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                //Post Lollipop
                dialog.getDatePicker().getTouchables().get(0).performClick();
            }
        }

        return dialog;
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int dayOfMonth) {
        if (datePicker.isShown()) {
            Date roDate = initDate;
            if (roDate == null) roDate = new Date();

            Calendar cal = Calendar.getInstance();
            cal.setTime(roDate);

            cal.set(Calendar.YEAR,year);
            cal.set(Calendar.MONTH, month);
            cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);

            if (callback != null) {
                callback.onSelect(cal.getTime());
            }
        }
    }
}