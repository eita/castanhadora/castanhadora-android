package br.coop.eita.castanhadora.db.models.derived;

import android.content.Context;
import android.text.TextUtils;
import android.text.format.DateUtils;

import androidx.databinding.BaseObservable;
import androidx.room.Embedded;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import br.coop.eita.castanhadora.Castanhadora;
import br.coop.eita.castanhadora.R;
import br.coop.eita.castanhadora.db.models.Safra;
import br.coop.eita.castanhadora.db.models.TrabalhoUpf;
import br.coop.eita.castanhadora.util.Util;

public class TrabalhoEIntegrantesUpf extends BaseObservable {

    @Embedded
    private TrabalhoUpf trabalhoUpf;

    private String integrantes;

    public TrabalhoUpf getTrabalhoUpf() {
        return trabalhoUpf;
    }

    public void setTrabalhoUpf(TrabalhoUpf trabalhoUpf) {
        this.trabalhoUpf = trabalhoUpf;
    }

    public String getIntegrantes() {
        return integrantes;
    }

    public void setIntegrantes(String integrantes) {
        this.integrantes = integrantes;
    }

    public int getId() {
        return trabalhoUpf.getId();
    }

    public String getDescricao() {
        return trabalhoUpf.getDescricao();
    }

    public List<String > getIntegrantesList() {
        return Arrays.asList(integrantes.split("\\|\\|\\|"));
    }

    public BigDecimal getRemuneracaoAlmejada(BigDecimal valorRemuneracaoFamiliarSafra) {
        BigDecimal qtdeIntegrantes = new BigDecimal(getIntegrantesList().size());
        BigDecimal qtdeDias = trabalhoUpf.getDias();
        return qtdeIntegrantes.multiply(qtdeDias.multiply(valorRemuneracaoFamiliarSafra));
    }

    public String getTituloCard(BigDecimal valorRemuneracaoFamiliarSafra) {
        BigDecimal remuneracaoAlmejada = getRemuneracaoAlmejada(valorRemuneracaoFamiliarSafra);
        String dataFormatada = DateUtils.formatDateTime(Castanhadora.APP, trabalhoUpf.getData().getTime(),
                DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_NUMERIC_DATE | DateUtils.FORMAT_SHOW_YEAR);
        return (remuneracaoAlmejada.compareTo(BigDecimal.ZERO) > 0)
            ? Util.formatMoney(remuneracaoAlmejada) + " - " + dataFormatada
            : dataFormatada;
    }

    public String getSubTituloCard() {
        Context context = Castanhadora.APP.getApplicationContext();
        String qtdeIntegrantesStr = new Integer(getIntegrantesList().size()).toString();
        return context.getString(R.string.trabalho_upf_card_subtitulo, trabalhoUpf.getDias().toString(), qtdeIntegrantesStr);
    }

    public String getDescricaoCard() {
        Context context = Castanhadora.APP.getApplicationContext();
        String listaIntegrantesStr = TextUtils.join(", ", getIntegrantesList());
        String descricaoCard = context.getString(R.string.trabalho_upf_card_descricao, listaIntegrantesStr);
        if (!TextUtils.isEmpty(trabalhoUpf.observacoes)) {
            descricaoCard = descricaoCard + " " + context.getString(R.string.descricao_card_observacao, trabalhoUpf.observacoes);
        }
        return descricaoCard;
    }
}
