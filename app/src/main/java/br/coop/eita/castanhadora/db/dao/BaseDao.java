package br.coop.eita.castanhadora.db.dao;

import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.RawQuery;
import androidx.room.Update;
import androidx.sqlite.db.SimpleSQLiteQuery;

import com.google.common.base.CaseFormat;

import java.lang.reflect.ParameterizedType;
import java.util.List;

import br.coop.eita.castanhadora.db.models.Safra;

public abstract class BaseDao<T> {

    @Insert
    public abstract long insert(T obj);

    @Insert
    public abstract long[] insert(T... objs);

    @Insert
    public abstract long[] insert(List<T> objs);

    @Update
    public abstract void update(T obj);

    @Update
    public abstract void update(T... objs);

    @Update
    public abstract void update(List<T> objs);

    @Delete
    public abstract void delete(T obj);

    @Delete
    public abstract void delete(T... objs);

    @Delete
    public abstract void delete(List<T> objs);


    @RawQuery
    protected abstract T doFind(SimpleSQLiteQuery query);

    @RawQuery
    protected abstract int doCount(SimpleSQLiteQuery query);

    public String getTableName() {
        // Below is based on your inheritance chain
        Class clazz = (Class) ((ParameterizedType) getClass().getSuperclass().getGenericSuperclass()).getActualTypeArguments()[0];
        String tableName = CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE,
                clazz.getSimpleName());
        return tableName;
    }

    public int count() {
        SimpleSQLiteQuery query = new SimpleSQLiteQuery("select count(*) from " + getTableName());
        return doCount(query);
    }

    public T first() {
        SimpleSQLiteQuery query = new SimpleSQLiteQuery(
                "select * from " + getTableName() + " LIMIT 1");
        return doFind(query);
    }

}
