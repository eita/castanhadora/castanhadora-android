package br.coop.eita.castanhadora.ui.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.List;

import br.coop.eita.castanhadora.R;
import br.coop.eita.castanhadora.databinding.ActivitySafraTrabalhosUpfListBinding;
import br.coop.eita.castanhadora.db.models.TrabalhoUpf;
import br.coop.eita.castanhadora.db.models.derived.TrabalhoEIntegrantesUpf;
import br.coop.eita.castanhadora.ui.adapters.TrabalhoEIntegrantesUpfAdapter;
import br.coop.eita.castanhadora.viewmodels.TrabalhoUpfViewModel;

public class SafraTrabalhosUpfListActivity extends AppCompatActivity {

    public static final int EDITAR_TRABALHO_ID_REPLY = 1;

    private TrabalhoUpfViewModel viewModel;

    private TrabalhoEIntegrantesUpfAdapter trabalhoEIntegrantesUpfAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final ActivitySafraTrabalhosUpfListBinding binding = DataBindingUtil.setContentView(this,
                R.layout.activity_safra_trabalhos_upf_list);

        viewModel = ViewModelProviders.of(this).get(TrabalhoUpfViewModel.class);
        binding.setViewmodel(viewModel);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(R.string.label_trabalhos);
        }

        LiveData<List<TrabalhoEIntegrantesUpf>> trabalhos = viewModel.getTrabalhosEIntegrantesUpf();

        final SafraTrabalhosUpfListActivity self = this;

        trabalhos.observe(this, new Observer<List<TrabalhoEIntegrantesUpf>>() {
            @Override
            public void onChanged(List<TrabalhoEIntegrantesUpf> trabs) {
                if (trabs != null) {
                    if (trabs.size() == 0) {
                        Toast.makeText(self, R.string.list_sem_trabalhos, Toast.LENGTH_LONG).show();
                        finish();
                    }
                    trabalhoEIntegrantesUpfAdapter = new TrabalhoEIntegrantesUpfAdapter(getApplicationContext(), trabs);
                    binding.trabalhoList.setAdapter(trabalhoEIntegrantesUpfAdapter);
                    trabalhoEIntegrantesUpfAdapter.setRemuneracaoAlmejadaSafra(viewModel.getSafraAtiva().getRemuneracaoDesejadaPorDia());
                    trabalhoEIntegrantesUpfAdapter.setTituloCard();
                    trabalhoEIntegrantesUpfAdapter.setActivity(self);
                    trabalhoEIntegrantesUpfAdapter.notifyDataSetChanged();
                }
            }
        });

    }

    public void openEditForm(TrabalhoUpf trabalhoUpf) {
        Intent intent = new Intent(SafraTrabalhosUpfListActivity.this, SafraTrabalhoUpfFormActivity.class).putExtra(
                "trabalho_upf_id", trabalhoUpf.getId());
        startActivityForResult(intent, EDITAR_TRABALHO_ID_REPLY);
    }

    public void openDeleteDialog(final TrabalhoUpf trabalhoUpf) {
        new AlertDialog.Builder(this)
                .setMessage(getResources().getString(R.string.msg_dialog_trabalho_upf_remover, trabalhoUpf.getDescricao()))
                .setPositiveButton(R.string.dialog_sim, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        viewModel.deleteTrabalhoUpf(trabalhoUpf);
                    }
                })
                .setNegativeButton(R.string.dialog_nao, null)
                .setTitle(R.string.label_dialog_trabalho_upf_delete_title)
                .show();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent replyIntent = new Intent();
                setResult(RESULT_CANCELED, replyIntent);
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
