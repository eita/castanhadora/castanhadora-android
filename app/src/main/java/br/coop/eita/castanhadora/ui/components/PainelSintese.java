package br.coop.eita.castanhadora.ui.components;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import java.util.ArrayList;

import br.coop.eita.castanhadora.databinding.ComponentPainelSinteseBinding;
import br.coop.eita.castanhadora.ui.adapters.DialogoCastanhadoraSafraInicioAdapter;
import br.coop.eita.castanhadora.util.DialogoCastanhadoraGridViewItem;

public class PainelSintese extends ConstraintLayout {
    ComponentPainelSinteseBinding mBinding;

    public PainelSintese(@NonNull Context context) {
        this(context, null);
    }

    public PainelSintese(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public PainelSintese(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        LayoutInflater inflater = LayoutInflater.from(context);
        mBinding = ComponentPainelSinteseBinding.inflate(inflater, this, true);
    }

    public void setValores(ArrayList<DialogoCastanhadoraGridViewItem> valores) {
        DialogoCastanhadoraSafraInicioAdapter dialogoCastanhadoraSafraInicioAdapter = new DialogoCastanhadoraSafraInicioAdapter(
                getContext(), valores);
        mBinding.dialogoCastanhadoraGridView.setAdapter(dialogoCastanhadoraSafraInicioAdapter);

    }

    public void enableSituacaoAtualButton () {
        mBinding.homeActionSituacaoAtual.setVisibility(View.VISIBLE);
    }

}
