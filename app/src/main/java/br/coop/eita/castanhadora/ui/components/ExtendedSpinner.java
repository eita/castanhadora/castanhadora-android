package br.coop.eita.castanhadora.ui.components;

import android.content.Context;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import androidx.appcompat.widget.AppCompatSpinner;
import androidx.databinding.InverseBindingListener;

import java.util.List;

import br.coop.eita.castanhadora.db.models.BaseIdentifiableModel;

public class ExtendedSpinner extends AppCompatSpinner {

    public ExtendedSpinner(Context context) {
        super(context);
    }

    public ExtendedSpinner(Context context, int mode) {
        super(context, mode);
    }

    public ExtendedSpinner(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ExtendedSpinner(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public ExtendedSpinner(Context context, AttributeSet attrs, int defStyleAttr, int mode) {
        super(context, attrs, defStyleAttr, mode);
    }

    public ExtendedSpinner(Context context, AttributeSet attrs, int defStyleAttr, int mode,
                           Resources.Theme popupTheme) {
        super(context, attrs, defStyleAttr, mode, popupTheme);
    }

    //Based on:
    //https://github.com/chetdeva/spinner-bindings/blob/master/app/src/main/java/com/chetdeva/spinnerbindings/extensions/SpinnerExtensions.kt

    /**
     * set spinner entries
     */
    public void setSpinnerEntries(List entries) {
        if (entries != null) {
            ArrayAdapter arrayAdapter = new ArrayAdapter(getContext(),
                    android.R.layout.simple_spinner_item, entries);
            arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            setAdapter(arrayAdapter);
        }
        if (tmpSelectedId != null && tmpSelectedId != 0) {
            setSelectedId(tmpSelectedId);
            tmpSelectedId = null;
        } else if (tmpSelectedValue != null) {
            setSelectedValue(tmpSelectedValue);
            tmpSelectedValue = null;
        } else { //Select first
            setSelection(0,false);
        }
    }


    private Integer tag = null;
    private Object tmpSelectedValue = null;
    private Integer tmpSelectedId = null;

    /**
     * set spinner onItemSelectedListener listener
     */
    public void setSpinnerInverseBindingListener(final InverseBindingListener listener) {
        OnItemSelectedListener onItemSelectedListener;
        if (listener == null) {
            onItemSelectedListener = null;
        } else {
            onItemSelectedListener = new OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position,
                                           long id) {
                    if (tag == null || tag != position) {
                        listener.onChange();
                        tag = position;
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            };
        }
        setOnItemSelectedListener(onItemSelectedListener);
    }

    /**
     * set spinner value
     */
    public void setSelectedValue(Object value) {
        ArrayAdapter adapter = (ArrayAdapter) getAdapter();
        if (adapter != null) {
            Integer position = adapter.getPosition(value);
            setSelection(position, false);
            tag = position;
        } else {
            tmpSelectedValue = value;
        }
    }

    /**
     * get spinner value
     */
    public Object getSelectedValue() {
        return getSelectedItem();
    }

    public Integer getSelectedId() {
        return ((BaseIdentifiableModel) getSelectedItem()).getId();
    }

    /*
    public void setSelectedId(int id) {
        setSelectedId(new Integer(id));
    }*/

    public void setSelectedId(Integer id) {
        ArrayAdapter adapter = (ArrayAdapter) getAdapter();
        if (adapter != null) {
            int count = adapter.getCount();
            int position = 0;
            for (int i = 0; i < count; i++) {
                BaseIdentifiableModel item = (BaseIdentifiableModel) adapter.getItem(i);
                if (item != null && item.getId() == id) {
                    setSelection(i, false);
                    tag = i;
                }
            }
            tag = position;
        } else {
            tmpSelectedId = id;
        }
    }

    public interface ItemSelectedListener {
        void onItemSelected(Object item);
    }


}
