package br.coop.eita.castanhadora.db.models.queries;

import androidx.sqlite.db.SimpleSQLiteQuery;

public class QueriesTotalizadoras {
    public static SimpleSQLiteQuery TotalGeral(Integer safraId) {
        return  new SimpleSQLiteQuery((
                "SELECT QTDE_PRODUZIDO as qtdeProduzido,\n" +
                "       QTDE_VENDIDO as qtdeVendido,\n" +
                "       VALOR_TOTAL_VENDIDO as valorTotalVendido,\n" +
                "       VALOR_TOTAL_DESPESAS as valorTotalDespesas,\n" +
                "       TOTAL_DIAS_TRABALHADOS as totalDiasTrabalhados,\n" +
                "       REMUNERACAO_DESEJADA_POR_DIA as remuneracaoDesejadaPorDia,\n" +
                "       DEPRECIACAO as valorDepreciacao,\n" +
                "       REMUNERACAO_DESEJADA as remuneracaoDesejada,\n" +
                "       QTDE_PRODUZIDO - QTDE_VENDIDO                                                    AS qtdeEstoque,\n" +
                "       VALOR_TOTAL_DESPESAS + REMUNERACAO_DESEJADA                                      AS valorCustosTotais,\n" +
                "       REMUNERACAO_DESEJADA + VALOR_TOTAL_DESPESAS + DEPRECIACAO                        AS valorTotalAAlcancar,\n" +
                "       CASE WHEN QTDE_VENDIDO = 0 THEN NULL ELSE VALOR_TOTAL_VENDIDO / QTDE_VENDIDO END AS valorMedioVendido\n" +
                "FROM (\n" +
                "         SELECT SUM(L.QTDE_PRODUZIDO)                                                       AS QTDE_PRODUZIDO,\n" +
                "                SUM(L.QTDE_VENDIDO)                                                         AS QTDE_VENDIDO,\n" +
                "                SUM(L.VALOR_TOTAL_VENDIDO)                                                  AS VALOR_TOTAL_VENDIDO,\n" +
                "                SUM(L.VALOR_TOTAL_DESPESAS)                                                 AS VALOR_TOTAL_DESPESAS,\n" +
                "                SUM(L.TOTAL_DIAS_TRABALHADOS)                                               AS TOTAL_DIAS_TRABALHADOS,\n" +
                "                SUM(L.REMUNERACAO_DESEJADA_POR_DIA)                                         AS REMUNERACAO_DESEJADA_POR_DIA,\n" +
                "                SUM(L.DEPRECIACAO)                                                          AS DEPRECIACAO,\n" +
                "                (SUM(L.TOTAL_DIAS_TRABALHADOS) / 100) * SUM(L.REMUNERACAO_DESEJADA_POR_DIA) AS REMUNERACAO_DESEJADA\n" +
                "         FROM (\n" +
                "                  SELECT IFNULL(SUM(P.QUANTIDADE), 0) AS QTDE_PRODUZIDO,\n" +
                "                         0                            AS QTDE_VENDIDO,\n" +
                "                         0                            AS VALOR_TOTAL_VENDIDO,\n" +
                "                         0                            AS VALOR_TOTAL_DESPESAS,\n" +
                "                         0                            AS TOTAL_DIAS_TRABALHADOS,\n" +
                "                         0                            AS REMUNERACAO_DESEJADA_POR_DIA,\n" +
                "                         0                            AS DEPRECIACAO\n" +
                "                  FROM PRODUCAO P\n" +
                "                  WHERE P.SAFRA_ID = :safraId\n" +
                "                  UNION\n" +
                "                  SELECT 0                                               AS QTDE_PRODUZIDO,\n" +
                "                         IFNULL(SUM(V.QUANTIDADE), 0)                    AS QTDE_VENDIDO,\n" +
                "                         IFNULL(SUM(V.QUANTIDADE * V.VALOR_UNITARIO), 0) AS VALOR_TOTAL_VENDIDO,\n" +
                "                         0                                               AS VALOR_TOTAL_DESPESAS,\n" +
                "                         0                                               AS TOTAL_DIAS_TRABALHADOS,\n" +
                "                         0                                               AS REMUNERACAO_DESEJADA_POR_DIA,\n" +
                "                         0                                               AS DEPRECIACAO\n" +
                "                  FROM VENDA V\n" +
                "                  WHERE V.SAFRA_ID = :safraId\n" +
                "                  UNION\n" +
                "                  SELECT 0                       AS QTDE_PRODUZIDO,\n" +
                "                         0                       AS QTDE_VENDIDO,\n" +
                "                         0                       AS VALOR_TOTAL_VENDIDO,\n" +
                "                         IFNULL(SUM(D.VALOR), 0) AS VALOR_TOTAL_DESPESAS,\n" +
                "                         0                       AS TOTAL_DIAS_TRABALHADOS,\n" +
                "                         0                       AS REMUNERACAO_DESEJADA_POR_DIA,\n" +
                "                         0                       AS DEPRECIACAO\n" +
                "                  FROM DESPESA D\n" +
                "                  WHERE D.SAFRA_ID = :safraId\n" +
                "                  UNION\n" +
                "                  SELECT 0                      AS QTDE_PRODUZIDO,\n" +
                "                         0                      AS QTDE_VENDIDO,\n" +
                "                         0                      AS VALOR_TOTAL_VENDIDO,\n" +
                "                         0                      AS VALOR_TOTAL_DESPESAS,\n" +
                "                         IFNULL(SUM(T.DIAS), 0) AS TOTAL_DIAS_TRABALHADOS,\n" +
                "                         0                      AS REMUNERACAO_DESEJADA_POR_DIA,\n" +
                "                         0                      AS DEPRECIACAO\n" +
                "                  FROM TRABALHO_UPF T\n" +
                "                           JOIN TRABALHO_UPF_INTEGRANTE TI ON T.ID = TI.TRABALHO_UPF_ID\n" +
                "                  WHERE T.SAFRA_ID = :safraId\n" +
                "                  UNION\n" +
                "                  SELECT 0                                         AS QTDE_PRODUZIDO,\n" +
                "                         0                                         AS QTDE_VENDIDO,\n" +
                "                         0                                         AS VALOR_TOTAL_VENDIDO,\n" +
                "                         0                                         AS VALOR_TOTAL_DESPESAS,\n" +
                "                         0                                         AS TOTAL_DIAS_TRABALHADOS,\n" +
                "                         IFNULL(S.REMUNERACAO_DESEJADA_POR_DIA, 0) AS REMUNERACAO_DESEJADA_POR_DIA,\n" +
                "                         0                                         AS DEPRECIACAO\n" +
                "                  FROM SAFRA S\n" +
                "                  WHERE S.ID = :safraId\n" +
                "                  UNION\n" +
                "                  SELECT 0            AS QTDE_PRODUZIDO,\n" +
                "                         0            AS QTDE_VENDIDO,\n" +
                "                         0            AS VALOR_TOTAL_VENDIDO,\n" +
                "                         0            AS VALOR_TOTAL_DESPESAS,\n" +
                "                         0            AS TOTAL_DIAS_TRABALHADOS,\n" +
                "                         0            AS REMUNERACAO_DESEJADA_POR_DIA,\n" +
                "                         IFNULL(SUM(CASE\n" +
                "                                        WHEN VALIDADE_ANOS > 0 THEN VALORCOMPRA / VALIDADE_ANOS\n" +
                "                                        ELSE 0\n" +
                "                             END), 0) AS DEPRECIACAO\n" +
                "                  FROM (\n" +
                "                           SELECT COALESCE(E.ANO_COMPRA, E.ANO_FABRICACAO) + E.DURABILIDADE_ESTIMADA_ANOS - STRFTIME('%Y', 'now') AS VALIDADE_ANOS,\n" +
                "                                  E.VALORCOMPRA,\n" +
                "                                  E.SAFRA_ID\n" +
                "                           FROM EQUIPAMENTO E\n" +
                "                           WHERE E.SAFRA_ID = SAFRA_ID\n" +
                "                       ) K\n" +
                "                  WHERE K.SAFRA_ID = :safraId\n" +
                "              ) L\n" +
                "     )").replaceAll(":safraId",Integer.toString(safraId)));
    }

}
