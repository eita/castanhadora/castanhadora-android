package br.coop.eita.castanhadora.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import java.util.List;

import br.coop.eita.castanhadora.Castanhadora;
import br.coop.eita.castanhadora.R;
import br.coop.eita.castanhadora.databinding.ListitemEquipamentoBinding;
import br.coop.eita.castanhadora.databinding.ListitemProducaoBinding;
import br.coop.eita.castanhadora.db.models.Producao;
import br.coop.eita.castanhadora.ui.activities.SafraProducoesListActivity;
import br.coop.eita.castanhadora.viewmodels.ProducaoViewModel;

public class ProducaoAdapter extends ArrayAdapter<Producao> {
    private List<Producao> eqtipos;
    private SafraProducoesListActivity activity;
    private String unidadeQtdeCastanhas;

    public ProducaoAdapter(Context context, List<Producao> objects) {
        super(context, 0, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ListitemProducaoBinding binding;

        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.listitem_producao, null);
            binding = DataBindingUtil.bind(convertView);
            convertView.setTag(binding);
        } else {
            binding = (ListitemProducaoBinding) convertView.getTag();
        }

        final Producao producao = getItem(position);
        final ProducaoAdapter self = this;

        binding.setProducao(producao);

        binding.primaryText.setText(producao.getTituloCard(unidadeQtdeCastanhas));

        binding.editBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.openEditForm(producao);
            }
        });

        binding.removeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.openDeleteDialog(producao);
            }
        });

        return binding.getRoot();
    }

    public void setActivity(SafraProducoesListActivity activity) {
        this.activity = activity;
    }

    public void setUnidadeQtdeCastanhas(String unidadeQtdeCastanhas) {
        this.unidadeQtdeCastanhas = unidadeQtdeCastanhas;
    }
}
