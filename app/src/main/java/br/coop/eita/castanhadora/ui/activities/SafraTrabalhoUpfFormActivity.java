package br.coop.eita.castanhadora.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import br.coop.eita.castanhadora.R;
import br.coop.eita.castanhadora.databinding.ActivitySafraTrabalhoUpfFormBinding;
import br.coop.eita.castanhadora.db.DataRepository;
import br.coop.eita.castanhadora.db.models.TrabalhoUpf;
import br.coop.eita.castanhadora.db.models.derived.UpfIntegranteTrabalha;
import br.coop.eita.castanhadora.ui.adapters.UpfIntegranteTrabalhaAdapter;
import br.coop.eita.castanhadora.util.DialogoCastanhadoraGridViewItem;
import br.coop.eita.castanhadora.util.Util;
import br.coop.eita.castanhadora.viewmodels.TrabalhoUpfViewModel;

public class SafraTrabalhoUpfFormActivity extends AppCompatActivity {

    public static final String TRABALHO_UPF_ID_REPLY = "br.coop.eita.castanhadora.trabalho_upf_id.REPLY";

    private TrabalhoUpfViewModel viewModel;

    private UpfIntegranteTrabalhaAdapter upfIntegranteTrabalhaAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        final ActivitySafraTrabalhoUpfFormBinding binding = DataBindingUtil.setContentView(this,
                R.layout.activity_safra_trabalho_upf_form);

        viewModel = ViewModelProviders.of(this).get(TrabalhoUpfViewModel.class);
        binding.setViewmodel(viewModel);

        binding.setLifecycleOwner(this);

        Integer trabalhoId = getIntent().getIntExtra("trabalho_upf_id", 0);

        TrabalhoUpf trabalhoUpf;

        ArrayList<DialogoCastanhadoraGridViewItem> valores = new ArrayList<>();

        BigDecimal totalDias = viewModel.getTotalDias();

        BigDecimal remuneracaoAlmejada = totalDias.multiply(viewModel.getSafraAtiva().getRemuneracaoDesejadaPorDia());

        valores.add(new DialogoCastanhadoraGridViewItem(
                getResources().getString(R.string.castanhadora_trabalho_upf_total_dias),
                totalDias.toString()));

        if (Util.deveInserirRemuneracao(viewModel.getSafraAtiva())) {
            valores.add(new DialogoCastanhadoraGridViewItem(
                    getResources().getString(R.string.castanhadora_trabalho_upf_remuneracao_almejada),
                    Util.formatMoney(remuneracaoAlmejada)));
        }

        binding.painelSintese.setValores(valores);

        LiveData<List<UpfIntegranteTrabalha>> integrantes;
        if (trabalhoId != null && trabalhoId > 0) {
            trabalhoUpf = viewModel.loadCurrentTrabalhoUpf(trabalhoId);
            integrantes = viewModel.loadIntegrantes(trabalhoId);
        } else {
            trabalhoUpf = viewModel.newTrabalhoUpf();
            integrantes = viewModel.loadIntegrantes(null);
        }

        setTitle(trabalhoUpf.getId() > 0 ? R.string.label_trabalho_upf_edit_title : R.string.label_trabalho_upf_new_title);

        final SafraTrabalhoUpfFormActivity self = this;

        integrantes.observe(this, new Observer<List<UpfIntegranteTrabalha>>() {
            @Override
            public void onChanged(List<UpfIntegranteTrabalha> upfIntegrantes) {
                if (upfIntegrantes != null) {
                    upfIntegranteTrabalhaAdapter = new UpfIntegranteTrabalhaAdapter(getApplicationContext(), upfIntegrantes);
                    binding.upfIntegranteTrabalhaListView.setAdapter(upfIntegranteTrabalhaAdapter);
                    upfIntegranteTrabalhaAdapter.setActivity(self);
                    upfIntegranteTrabalhaAdapter.notifyDataSetChanged();
                }
            }
        });

        binding.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SafraTrabalhoUpfFormActivity.this, VideoTutorialActivity.class);
                intent.putExtra("videoPath", R.raw.trabalho_familia);
                intent.putExtra("videoTitle", R.string.label_trabalho_upf_new_title);
                startActivity(intent);
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent replyIntent = new Intent();
                setResult(RESULT_CANCELED, replyIntent);
                finish();
                return true;
            case R.id.action_save:
                final SafraTrabalhoUpfFormActivity self = this;
                viewModel.saveCurrentTrabalhoUpf(new DataRepository.OnSaveEntityListener() {
                    @Override
                    public void onSuccess(Integer... ids) {
                        Intent replyIntent = new Intent();
                        replyIntent.putExtra(TRABALHO_UPF_ID_REPLY, ids[0]);
                        setResult(RESULT_OK, replyIntent);
                        Toast.makeText(self, R.string.saved_trabalho_upf, Toast.LENGTH_LONG).show();
                        finish();
                    }

                    @Override
                    public void onFailure(int errorCode, List<String> errorMessages) {
                        Toast.makeText(self, errorMessages.get(0), Toast.LENGTH_LONG).show();
                    }
                });

                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_form, menu);
        return super.onCreateOptionsMenu(menu);
    }
}
