package br.coop.eita.castanhadora.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;


import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import android.util.Log;
import android.view.MenuItem;

import android.view.View;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import br.coop.eita.castanhadora.R;
import br.coop.eita.castanhadora.databinding.ActivitySafraVendaSummaryBinding;
import br.coop.eita.castanhadora.util.Util;
import br.coop.eita.castanhadora.viewmodels.VendaViewModel;

public class SafraVendaSummaryActivity extends AppCompatActivity {

    VendaViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final ActivitySafraVendaSummaryBinding binding = DataBindingUtil.setContentView(this,
                R.layout.activity_safra_venda_summary);

        Integer vendaId = getIntent().getIntExtra("venda_id", 0);

        if (vendaId == 0 || vendaId == null) {
            Toast.makeText(this, getResources().getString(R.string.error_argument_necessary, "venda_id"), Toast.LENGTH_LONG).show();
            finish();
            return;
        }

        viewModel = ViewModelProviders.of(this).get(VendaViewModel.class);
        binding.setViewmodel(viewModel);

        viewModel.loadVendaAtual(vendaId);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(viewModel.getVendaAtual().getTituloCard(viewModel.getUpf().getUnidadeValor()));
        }

        setTitle(R.string.title_venda_detalhes);

        binding.exportSummary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bitmap bitmap = getInvoiceDrawable().getBitmap();
                Uri uri = saveImage(bitmap);
                shareImageUri(uri);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent replyIntent = new Intent();
                setResult(RESULT_CANCELED, replyIntent);
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private BitmapDrawable getInvoiceDrawable() {
        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inScaled = false;

        Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.venda_summary_template, opts)
                .copy(Bitmap.Config.ARGB_8888, true);

        Typeface tf = Typeface.create("Helvetica", Typeface.BOLD);

        Float ratio = 2.747f;

        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.DKGRAY);
        paint.setTypeface(tf);
        paint.setTextAlign(Paint.Align.LEFT);
        paint.setTextSize(28);

        //Rect textRect = new Rect();
        //paint.getTextBounds(text, 0, text.length(), textRect);

        Canvas canvas = new Canvas(bm);

        Integer posX = (int) (175/ratio);

        canvas.drawText(viewModel.getVendaAtual().getComprador(), posX, (int) (1125/ratio), paint);
        canvas.drawText(viewModel.getUpf().getNomeUsuario(), posX, (int) (1525/ratio), paint);
        canvas.drawText(getResources().getString(R.string.label_venda_medida) + ": " + viewModel.getUpf().getUnidadeValor(), posX, (int) (1925/ratio), paint);
        canvas.drawText(getResources().getString(R.string.label_venda_preco_unitario) + ": " + Util.formatMoney(viewModel.getVendaAtual().getValorUnitario()), posX, (int) (2025/ratio), paint);
        canvas.drawText(getResources().getString(R.string.label_venda_quantidade_vendida) + ": " + viewModel.getVendaAtual().getQuantidade().toString(), posX, (int) (2125/ratio), paint);
        canvas.drawText(getResources().getString(R.string.label_venda_valor_total) + ": " + Util.formatMoney(viewModel.getVendaAtual().getValorTotal()), posX, (int) (2225/ratio), paint);
        canvas.drawText(getResources().getString(R.string.label_venda_data) + ": " + Util.formatDate(viewModel.getVendaAtual().getData()), posX, (int) (2325/ratio), paint);

        return new BitmapDrawable(getResources(), bm);
    }

    /**
     * Saves the image as PNG to the app's cache directory.
     *
     * @param image Bitmap to save.
     * @return Uri of the saved file or null
     */
    private Uri saveImage(Bitmap image) {
        //TODO - Should be processed in another thread
        File imagesFolder = new File(getCacheDir(), "images");
        Uri uri = null;
        try {
            imagesFolder.mkdirs();
            File file = new File(imagesFolder, "shared_image.png");

            FileOutputStream stream = new FileOutputStream(file);
            image.compress(Bitmap.CompressFormat.PNG, 90, stream);
            stream.flush();
            stream.close();
            uri = FileProvider.getUriForFile(this, "br.coop.eita.castanhadora", file);

        } catch (IOException e) {
            Log.d("CASTANHADORA", "IOException while trying to write file for sharing: " + e.getMessage());
        }
        return uri;
    }

    /**
     * Shares the PNG image from Uri.
     *
     * @param uri Uri of image to share.
     */
    private void shareImageUri(Uri uri) {
        Intent intent = new Intent(android.content.Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_STREAM, uri);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.setType("image/png");
        startActivity(intent);
    }

}