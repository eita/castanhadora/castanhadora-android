package br.coop.eita.castanhadora.ui.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import br.coop.eita.castanhadora.R;
import br.coop.eita.castanhadora.databinding.ActivitySafraDespesaPadraoFormBinding;
import br.coop.eita.castanhadora.databinding.ActivitySafraInicioBinding;
import br.coop.eita.castanhadora.db.DataRepository;
import br.coop.eita.castanhadora.db.models.Despesa;
import br.coop.eita.castanhadora.db.models.Equipamento;
import br.coop.eita.castanhadora.ui.components.EditTextMask;
import br.coop.eita.castanhadora.util.DialogoCastanhadoraGridViewItem;
import br.coop.eita.castanhadora.util.Util;
import br.coop.eita.castanhadora.viewmodels.DespesaViewModel;
import br.coop.eita.castanhadora.viewmodels.SafraViewModel;

public class SafraDespesaPadraoFormActivity extends AppCompatActivity {

    public static final String DESPESA_ID_REPLY = "br.coop.eita.castanhadora.despesa_id.REPLY";

    private DespesaViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String tipo = getIntent().getStringExtra("tipo");

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        ActivitySafraDespesaPadraoFormBinding binding = DataBindingUtil.setContentView(this,
                R.layout.activity_safra_despesa_padrao_form);

        viewModel = ViewModelProviders.of(this).get(DespesaViewModel.class);
        binding.setViewmodel(viewModel);

        int tipoTitleResource = getResources().getIdentifier("label_despesa_" + tipo, "string",
                getPackageName());

        /*binding.tipoDespesa.setText(getResources().getString(R.string.label_despesa_tipo,
                getResources().getString(tipoTitleResource)));*/

        binding.setLifecycleOwner(this);

        Integer despesaId = getIntent().getIntExtra("despesa_id", 0);

        Despesa despesa;
        if (despesaId != null && despesaId > 0) {
            despesa = viewModel.loadCurrentDespesa(despesaId);
        } else {
            despesa = viewModel.newDespesa(tipo);
        }

        setTitle(
                despesa.getId() > 0 ? R.string.label_despesa_edit_title : R.string.label_despesa_new_title);

        ArrayList<DialogoCastanhadoraGridViewItem> valores = new ArrayList<>();

        BigDecimal totalDespesa = viewModel.getTotalDespesaParaTipo(tipo);

        valores.add(new DialogoCastanhadoraGridViewItem(
                getResources().getString(R.string.despesa_total_categoria,
                        getResources().getString(tipoTitleResource).toLowerCase()),
                Util.formatMoney(totalDespesa)));

        binding.painelSintese.setValores(valores);

        binding.inputValor.requestFocus();
        binding.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SafraDespesaPadraoFormActivity.this, VideoTutorialActivity.class);
                intent.putExtra("videoPath", R.raw.despesas);
                intent.putExtra("videoTitle", R.string.safra_totais_despesas);
                startActivity(intent);
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent replyIntent = new Intent();
                setResult(RESULT_CANCELED, replyIntent);
                finish();
                return true;
            case R.id.action_save:
                final SafraDespesaPadraoFormActivity self = this;
                viewModel.saveCurrentDespesa(new DataRepository.OnSaveEntityListener() {
                    @Override
                    public void onSuccess(Integer... ids) {
                        Intent replyIntent = new Intent();
                        replyIntent.putExtra(DESPESA_ID_REPLY, ids[0]);
                        setResult(RESULT_OK, replyIntent);
                        Toast.makeText(self, R.string.saved_despesa, Toast.LENGTH_LONG).show();
                        finish();
                    }

                    @Override
                    public void onFailure(int errorCode, List<String> errorMessages) {
                        Toast.makeText(self, errorMessages.get(0), Toast.LENGTH_LONG).show();
                    }
                });

                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_form, menu);
        return super.onCreateOptionsMenu(menu);
    }
}
