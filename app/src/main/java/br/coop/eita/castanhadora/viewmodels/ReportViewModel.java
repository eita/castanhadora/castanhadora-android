package br.coop.eita.castanhadora.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;

import br.coop.eita.castanhadora.db.models.Safra;

public class ReportViewModel extends BaseViewModel {

    private Safra safra;

    public ReportViewModel(@NonNull Application application) {
        super(application);
    }

    public Safra loadSafra(Integer safraId) {
        safra = dataRepository.load(Safra.class, safraId);
        return safra;
    }

    public Safra getSafra() {
        return safra;
    }
}
