package br.coop.eita.castanhadora.db;

import android.app.Application;
import android.database.sqlite.SQLiteConstraintException;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;

import androidx.lifecycle.LiveData;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import br.coop.eita.castanhadora.Castanhadora;
import br.coop.eita.castanhadora.Constants;
import br.coop.eita.castanhadora.R;
import br.coop.eita.castanhadora.db.dao.BaseDao;
import br.coop.eita.castanhadora.db.dao.BaseIdentifiableDao;
import br.coop.eita.castanhadora.db.dao.TrabalhoUpfDao;
import br.coop.eita.castanhadora.db.models.BaseIdentifiableModel;
import br.coop.eita.castanhadora.db.models.CategoriaDespesa;
import br.coop.eita.castanhadora.db.models.Despesa;
import br.coop.eita.castanhadora.db.models.Equipamento;
import br.coop.eita.castanhadora.db.models.Producao;
import br.coop.eita.castanhadora.db.models.Safra;
import br.coop.eita.castanhadora.db.models.TipoEquipamento;
import br.coop.eita.castanhadora.db.models.TrabalhoUpf;
import br.coop.eita.castanhadora.db.models.TrabalhoUpfIntegrante;
import br.coop.eita.castanhadora.db.models.Upf;
import br.coop.eita.castanhadora.db.models.UpfIntegrante;
import br.coop.eita.castanhadora.db.models.Venda;
import br.coop.eita.castanhadora.db.models.derived.EquipamentoETipo;
import br.coop.eita.castanhadora.db.models.derived.TotalGeral;
import br.coop.eita.castanhadora.db.models.derived.TrabalhoEIntegrantesUpf;
import br.coop.eita.castanhadora.db.models.derived.UpfIntegranteTrabalha;
import br.coop.eita.castanhadora.db.models.queries.QueriesTotalizadoras;


public class DataRepository {

    private static DataRepository sInstance;

    CastanhadoraDatabase mDb;
    Upf upf;
    Safra safraAtiva;
    Venda vendaAtual;
    TotalGeral totalGeral;

    // Note that in order to unit test the WordRepository, you have to remove the Application
    // dependency. This adds complexity and much more code, and this sample is not about testing.
    // See the BasicSample in the android-architecture-components repository at
    // https://github.com/googlesamples
    DataRepository(Application application) {
        mDb = CastanhadoraDatabase.getDatabase(application);
        CastanhadoraDatabase.backUpDataBase(application);
        upf = mDb.upfDao().first();
        loadSafraAtiva();
    }

    public static DataRepository getInstance(final Application application) {
        if (sInstance == null) {
            synchronized (DataRepository.class) {
                if (sInstance == null) {
                    sInstance = new DataRepository(application);
                }
            }
        }
        return sInstance;
    }

    /***********************************************************************************
     * META                                                                            *
     ***********************************************************************************/

    public int getStatusInicio() {
        //Safra atual é nula?
        if (getSafraCount() > 0 && getSafraAtiva() == null) {
            return Constants.NENHUMA_SAFRA_ABERTA;
        }
        if (getSafraAtiva() == null) {
            return Constants.SAFRA_ATUAL_INEXISTENTE;
        }
        //Upf tem os dados pessoais?
        if (!getUpf().dadosUsuarioCompletos()) {
            return Constants.DADOS_PESSOAIS_NAO_CADASTRADOS;
        }
        //Safra atual tem pelo menos 1 integrante? (se nao tiver, vai pra tela dos dados pessoais)
        Integer countIntegrantes = getCountUpfIntegrantes();
        if (countIntegrantes == null || countIntegrantes < 1) {
            return Constants.NENHUM_INTEGRANTE_CADASTRADO;
        }
        //Unidades estão selecionadas?
        if (!getUpf().unidadesCompletas()) {
            return Constants.UNIDADES_NAO_ESPECIFICADAS;
        }
        //Propriedades do castanhal estão definidas?
        /*
        if (!getSafraAtiva().castanhalCompleto()) {
            return Constants.CASTANHAL_INCOMPLETO;
        }*/

        //Valor esperado de ganho
        //BigDecimal remuneracaoDesejadaPorDia = getSafraAtiva().getRemuneracaoDesejadaPorDia();
        //if (remuneracaoDesejadaPorDia == null || remuneracaoDesejadaPorDia.floatValue() < 0.01) {
        //    return Constants.REMUNERACAO_NAO_ESPECIFICADA;
        //}

        return Constants.DADOS_INICIAIS_COMPLETOS;
    }


    /***********************************************************************************
     * UPF                                                                             *
     ***********************************************************************************/
    public Upf getUpf() {
        return upf;
    }

    public void saveUpf(OnSaveEntityListener listener) {
        if (upf != null) {
            save(upf, listener);
        }
    }

    public Upf reloadUpf() {
        upf = mDb.upfDao().first();
        return upf;
    }


    /***********************************************************************************
     * SAFRA                                                                           *
     ***********************************************************************************/
    public Safra getSafraAtiva() {
        return safraAtiva;
    }

    public void setSafraAtiva(Safra safraAtiva) {
        this.safraAtiva = safraAtiva;
    }

    public void setSafraAtiva(Integer safraId) {
        this.safraAtiva = load(Safra.class, safraId);
    }

    public Integer getSafraCount() {
        return mDb.safraDao().getRowCount();
    }

    public Safra newSafra() {
        Safra safra = new Safra();
        safra.setUpfId(this.upf.getId());
        safra.setDataInicio(new Date());
        safra.setCastanhalNumeroFamilias(1);
        return safra;
    }

    public LiveData<List<Safra>> getSafrasLive() {
        return mDb.safraDao().getSafrasLive();
    }

    public List<Safra> getSafras() {
        return mDb.safraDao().getSafras();
    }

    public Safra getSafraById(Integer safraId) {
        return mDb.safraDao().getSafraById(safraId);
    }

    public void loadSafraAtiva() {
        safraAtiva = mDb.safraDao().getSafraAtiva();
    }
    public LiveData<Safra> getSafraAtivaLive() {
        return mDb.safraDao().getSafraAtivaLive();
    }

    public void encerrarSafra(Integer safraId, final OnSaveEntityListener listener) {
        final Safra safra;
        if (safraAtiva != null && safraId == safraAtiva.getId()) {
            safra = safraAtiva;
        } else {
            safra = load(Safra.class, safraId);
        }

        safra.setAtiva(false);
        safra.setDataFinal(new Date());
        final DataRepository self = this;
        save(safra, new OnSaveEntityListener() {
            @Override
            public void onSuccess(Integer... ids) {
                if (safra == safraAtiva) {
                    self.safraAtiva = null;
                }
                listener.onSuccess(ids);
            }

            @Override
            public void onFailure(int errorCode, List<String> errorMessages) {
                listener.onFailure(errorCode, errorMessages);
            }
        });
    }

    public void selecionarSafraAtiva(Integer safraId) {
        Safra safra = load(Safra.class, safraId);
        if (safra != null) {
            if (safraAtiva != null) {
                safraAtiva.setAtiva(false);
            }
        }

    }

    /**
     * Copia safra, equipamentos, integrantes
     *
     * @param safraId
     * @return
     */
    public void copiaProfundaSafra(final Integer safraId, String nome, Date dataInicio,
                                   Date dataFinal, final OnSaveEntityListener listener) {
        final Safra safra = load(Safra.class, safraId);
        if (safra != null) {
            Safra novaSafra = safra.duplicar(nome, dataInicio, dataFinal);
            final SaveMultipleCounter counter = new SaveMultipleCounter(3, listener);
            save(novaSafra, new OnSaveEntityListener() {
                @Override
                public void onSuccess(Integer... ids) {
                    counter.setResponse(ids);
                    counter.achieveSuccess();
                    Integer novaSafraId = ids[0];
                    List<Equipamento> equipamentos = getEquipamentos(safraId);
                    List<Equipamento> novosEquipamentos = new ArrayList<>();

                    for (Equipamento equipamento : equipamentos) {
                        Equipamento novoEquipamento = equipamento.duplicar(novaSafraId);
                        novosEquipamentos.add(novoEquipamento);
                    }

                    save(new OnSaveEntityListener() {
                        @Override
                        public void onSuccess(Integer... ids) {
                            counter.achieveSuccess();
                        }

                        @Override
                        public void onFailure(int errorCode, List<String> errorMessages) {
                            listener.onFailure(errorCode, errorMessages);
                        }
                    }, novosEquipamentos.toArray(new Equipamento[0]));

                    List<UpfIntegrante> integrantes = getUpfIntegrantes(safraId);
                    List<UpfIntegrante> novosIntegrantes = new ArrayList<>();

                    for (UpfIntegrante integrante : integrantes) {
                        UpfIntegrante novoIntegrante = integrante.duplicar(novaSafraId);
                        novosIntegrantes.add(novoIntegrante);
                    }

                    save(new OnSaveEntityListener() {
                        @Override
                        public void onSuccess(Integer... ids) {
                            counter.achieveSuccess();
                        }

                        @Override
                        public void onFailure(int errorCode, List<String> errorMessages) {
                            listener.onFailure(errorCode, errorMessages);
                        }
                    }, novosIntegrantes.toArray(new UpfIntegrante[0]));
                }

                @Override
                public void onFailure(int errorCode, List<String> errorMessages) {
                    listener.onFailure(errorCode, errorMessages);
                }
            });
        }
    }

    /***********************************************************************************
     * INTEGRANTE_UPF                                                                  *
     ***********************************************************************************/
    public UpfIntegrante newIntegranteUpf() {
        UpfIntegrante integranteUpf = new UpfIntegrante();
        integranteUpf.setSafraId(this.safraAtiva.getId());
        return integranteUpf;
    }

    /**
     * Get Integrantes for current safra
     *
     * @return
     */
    public LiveData<List<UpfIntegrante>> getUpfIntegrantesLive() {
        return getUpfIntegrantesLive(safraAtiva.getId());
    }

    public LiveData<List<UpfIntegrante>> getUpfIntegrantesLive(Integer safraId) {
        return mDb.upfIntegranteDao().getIntegrantesLive(safraId);
    }

    public List<UpfIntegrante> getUpfIntegrantes(Integer safraId) {
        return mDb.upfIntegranteDao().getIntegrantes(safraId);
    }

    public LiveData<List<UpfIntegranteTrabalha>> getUpfIntegranteTrabalhaList(Integer trabalhoId) {
        return getUpfIntegranteTrabalhaList(trabalhoId, safraAtiva.getId());
    }

    public LiveData<List<UpfIntegranteTrabalha>> getUpfIntegranteTrabalhaList(Integer trabalhoId,
                                                                              Integer safraId) {
        if (trabalhoId == null) {
            return mDb.upfIntegranteDao().getIntegrantesETrabalho(safraId);
        } else {
            return mDb.upfIntegranteDao().getIntegrantesETrabalho(trabalhoId, safraId);
        }
    }

    public Integer getCountUpfIntegrantes() {
        return getCountUpfIntegrantes(safraAtiva.getId());
    }

    public Integer getCountUpfIntegrantes(Integer safraId) {
        return mDb.upfIntegranteDao().getCountUpfIntegrantes(safraId);
    }

    public UpfIntegrante getMainIntegrante() {
        return mDb.upfIntegranteDao().getMainIntegrante(safraAtiva.getId());
    }

    /***********************************************************************************
     * EQUIPAMENTO                                                                     *
     ***********************************************************************************/
    public Equipamento newEquipamento() {
        Equipamento equipamento = new Equipamento();
        equipamento.setSafraId(this.safraAtiva.getId());
        equipamento.setAnoFabricacao(Calendar.getInstance().get(Calendar.YEAR));
        equipamento.setAnoCompra(Calendar.getInstance().get(Calendar.YEAR));
        equipamento.setQuantidade(1);
        equipamento.setUsado(false);
        equipamento.setDurabilidadeEstimadaAnos(10);
        return equipamento;
    }

    public LiveData<List<Equipamento>> getEquipamentosLive(Integer safraId) {
        return mDb.equipamentoDao().getEquipamentosLive(safraId);
    }

    public List<Equipamento> getEquipamentos(Integer safraId) {
        return mDb.equipamentoDao().getEquipamentos(safraId);
    }

    public LiveData<List<EquipamentoETipo>> getEquipamentosETipos() {
        return getEquipamentosETipos(safraAtiva.getId());
    }

    public LiveData<List<EquipamentoETipo>> getEquipamentosETipos(Integer safraId) {
        return mDb.equipamentoDao().getEquipamentosETipos(safraId);
    }


    public LiveData<List<TipoEquipamento>> getTiposEquipamento() {
        return mDb.tipoEquipamentoDao().getTiposEquipamento();
    }

    /***********************************************************************************
     * DESPESA                                                                         *
     ***********************************************************************************/

    public LiveData<List<CategoriaDespesa>> getCategoriasDespesa() {
        return mDb.categoriaDespesaDao().getAll();
    }

    public Despesa newDespesa(String tipo) {
        Despesa despesa = new Despesa();
        despesa.setSafraId(this.safraAtiva.getId());
        if (!TextUtils.isEmpty(tipo)) {
            despesa.setTipo(tipo);
        }
        despesa.setData(new Date());
        despesa.setDiariaQuantidadeDiaristas(1);
        despesa.setDiariaDias(new BigDecimal(1));

        Despesa lastDespesa = mDb.despesaDao().getLastDespesa(safraAtiva.getId());

        if (lastDespesa != null) {
            despesa.setDiariaValorDiaria(lastDespesa.getDiariaValorDiaria());
        }

        return despesa;
    }

    public LiveData<List<Despesa>> getDespesas() {
        return mDb.despesaDao().getDespesasLive(safraAtiva.getId());
    }

    public LiveData<List<Despesa>> getDespesasBySafraIdLive(Integer safraId) {
        return mDb.despesaDao().getDespesasLive(safraId);
    }

    public List<Despesa> getDespesasBySafraId(Integer safraId) {
        return mDb.despesaDao().getDespesas(safraId);
    }

    public BigDecimal getTotalDespesaParaTipo(Integer safraId, String tipo) {
        return mDb.despesaDao().getTotalDespesas(safraId, tipo);
    }

    public BigDecimal getTotalDespesaParaTipo(String tipo) {
        return getTotalDespesaParaTipo(this.safraAtiva.getId(), tipo);
    }

    /***********************************************************************************
     * TRABALHO UPF                                                                    *
     ***********************************************************************************/

    public TrabalhoUpf newTrabalhoUpf() {
        TrabalhoUpf trabalhoUpf = new TrabalhoUpf();
        trabalhoUpf.setSafraId(this.safraAtiva.getId());
        trabalhoUpf.setData(new Date());
        trabalhoUpf.setDias(new BigDecimal(1));

        return trabalhoUpf;
    }

    public void save(final TrabalhoUpf trabalhoUpf, final List<UpfIntegranteTrabalha> trabalhaList,
                     final OnSaveEntityListener listener) {
        Integer trabalham = 0;
        for (UpfIntegranteTrabalha upfIntegranteTrabalha : trabalhaList) {
            if (upfIntegranteTrabalha.getTrabalha()) {
                trabalham++;
            }
        }
        if (trabalham == 0) {
            ArrayList<String> errors = new ArrayList<>();
            errors.add(Castanhadora.APP.getResources().getString(
                    R.string.error_val_upf_integrante_trabalha_nenhum));
            listener.onFailure(CastanhadoraDatabase.ERROR_CODE_VALIDATION, errors);
        } else {
            save(trabalhoUpf, new OnSaveEntityListener() {
                @Override
                public void onSuccess(Integer... ids) {

                    mDb.trabalhoUpfIntegranteDao().updateIntegranteTrabalhaList(trabalhaList,
                            ids[0]);

                    if (listener != null) {
                        listener.onSuccess(ids);
                    }
                }

                @Override
                public void onFailure(int errorCode, List<String> errorMessages) {
                    if (listener != null) {
                        listener.onFailure(errorCode, errorMessages);
                    }
                }
            });
        }
    }

    public LiveData<List<TrabalhoUpf>> getTrabalhos() {
        return mDb.trabalhoUpfDao().getTrabalhos(safraAtiva.getId());
    }

    public LiveData<List<TrabalhoEIntegrantesUpf>> getTrabalhosEIntegrantesUpf() {
        return mDb.trabalhoUpfDao().getTrabalhosEIntegrantesUpfLive(safraAtiva.getId());
    }

    public LiveData<List<TrabalhoEIntegrantesUpf>> getTrabalhosEIntegrantesUpfBySafraIdLive(Integer safraId) {
        return mDb.trabalhoUpfDao().getTrabalhosEIntegrantesUpfLive(safraId);
    }

    public List<TrabalhoEIntegrantesUpf> getTrabalhosEIntegrantesUpfBySafraId(Integer safraId) {
        return mDb.trabalhoUpfDao().getTrabalhosEIntegrantesUpf(safraId);
    }

    public BigDecimal getTotalDias() {
        return mDb.trabalhoUpfDao().getTotalDias(safraAtiva.getId());
    }

    public List<TrabalhoUpfIntegrante> getIntegrantesByTrabalhoId(Integer trabalhoId) {
        return mDb.trabalhoUpfIntegranteDao().getIntegrantesByTrabalhoId(trabalhoId);
    }




    /***********************************************************************************
     * PRODUCAO                                                                        *
     ***********************************************************************************/

    public LiveData<List<Producao>> getProducoes() {
        return mDb.producaoDao().getProducoesLive(safraAtiva.getId());
    }

    public LiveData<List<Producao>> getProducoesBySafraIdLive(Integer safraId) {
        return mDb.producaoDao().getProducoesLive(safraId);
    }

    public List<Producao> getProducoesBySafraId(Integer safraId) {
        return mDb.producaoDao().getProducoes(safraId);
    }

    public Integer getProducaoTotal() {
        return mDb.producaoDao().getProducaoTotal(safraAtiva.getId());
    }

    public Integer getProducaoVendida() {
        return mDb.producaoDao().getProducaoVendida(safraAtiva.getId());
    }

    public Producao newProducao() {
        Producao producao = new Producao();
        producao.setSafraId(safraAtiva.getId());
        producao.setData(new Date());
        return producao;
    }

    /***********************************************************************************
     * VENDA                                                                           *
     ***********************************************************************************/

    public LiveData<List<Venda>> getVendas() {
        return mDb.vendaDao().getVendasLive(safraAtiva.getId());
    }

    public LiveData<List<Venda>> getVendasBySafraIdLive(Integer safraId) {
        return mDb.vendaDao().getVendasLive(safraId);
    }

    public List<Venda> getVendasBySafraId(Integer safraId) {
        return mDb.vendaDao().getVendas(safraId);
    }

    /**
     * Cria venda e a transforma na venda atual
     *
     * @return
     */
    public Venda newVenda() {
        Venda venda = new Venda();
        venda.setSafraId(safraAtiva.getId());
        venda.setData(new Date());
        venda.setQuitacaoAviamento(false);
        venda.setValorUnitario(
                totalGeral.getPrecoSugerido() != null ? totalGeral.getPrecoSugerido() :
                        BigDecimal.ZERO);
        vendaAtual = venda;
        return venda;
    }

    public Venda getVendaAtual() {
        return vendaAtual;
    }

    public void loadVendaAtual(Integer vendaId) {
        vendaAtual = load(Venda.class,vendaId);
    }

    /***********************************************************************************
     * TOTAIS                                                                          *
     ***********************************************************************************/

    public LiveData<TotalGeral> getTotalGeralLive() {
        return mDb.safraDao().getTotalGeralLive(
                QueriesTotalizadoras.TotalGeral(safraAtiva.getId()));
    }

    public TotalGeral getTotalGeral() {
        totalGeral = mDb.safraDao().getTotalGeral(
                QueriesTotalizadoras.TotalGeral(safraAtiva.getId()));
        return totalGeral;
    }

    public TotalGeral getTotalGeral(Integer safraId) {
        return mDb.safraDao().getTotalGeral(
                QueriesTotalizadoras.TotalGeral(safraId));
    }

    /**
     * Retorna o último TotalGeral retornado por getTotalGeral(), sem fazer consulta na base de dados
     *
     * @return
     */
    public TotalGeral getCachedTotalGeral() {
        return totalGeral;
    }

    /***********************************************************************************
     * GENERIC METHODS: load, save                                                     *
     ***********************************************************************************/
    public <T extends BaseIdentifiableModel> T load(Class<T> klass, Integer id) {
        BaseIdentifiableDao<T> dao = (BaseIdentifiableDao<T>) mDb.getDaoForType(klass);
        return dao.load(id);
    }

    /**
     * @param models   models to save
     * @param listener
     * @return a list of validation errors, if any
     */
    public <T extends BaseIdentifiableModel> void save(OnSaveEntityListener listener, T... models) {

        for (T model : models) {
            List<String> validationErrors = model.validate();
            if (validationErrors.size() > 0 && listener != null) {
                listener.onFailure(CastanhadoraDatabase.ERROR_CODE_VALIDATION, validationErrors);
            }
        }
        new saveTask<T>(mDb, listener).execute(models);
    }

    /**
     * @param model    model to save
     * @param listener
     * @return a list of validation errors, if any
     */
    public <T extends BaseIdentifiableModel> void save(T model, OnSaveEntityListener listener) {

        if (model != null) {
            List<String> validationErrors = model.validate();
            if (validationErrors.size() == 0) {
                new saveTask<T>(mDb, listener).execute(model);
            } else {
                if (listener != null) {
                    listener.onFailure(CastanhadoraDatabase.ERROR_CODE_VALIDATION,
                            validationErrors);
                }
            }
        } else {
            if (listener != null) {
                ArrayList<String> errors = new ArrayList<>();
                errors.add(
                        Castanhadora.APP.getResources().getString(R.string.error_val_null_model));
                listener.onFailure(CastanhadoraDatabase.ERROR_CODE_NULL_MODEL, errors);
            }
        }
    }

    public <T extends BaseIdentifiableModel> void delete(T model) {
        delete(model, null);
    }

    public <T extends BaseIdentifiableModel> void delete(T model, OnDeleteEntityListener listener) {
        if (model != null) {
            List<String> validationErrors = model.validate();
            if (validationErrors.size() == 0) {
                new deleteTask<T>(mDb, listener).execute(model);
            } else {
                if (listener != null) {
                    listener.onFailure(CastanhadoraDatabase.ERROR_CODE_VALIDATION,
                            validationErrors);
                }
            }
        } else {
            if (listener != null) {
                ArrayList<String> errors = new ArrayList<>();
                errors.add(
                        Castanhadora.APP.getResources().getString(R.string.error_val_null_model));
                listener.onFailure(CastanhadoraDatabase.ERROR_CODE_NULL_MODEL, errors);
            }
        }
    }

    public interface OnSafraAtivaListener {
        void onSuccess();

        void onFailure();
    }

    public void ativarSafra(Integer safraId) {
        ativarSafra(safraId, null);
    }

    public void ativarSafra(Integer safraId, final OnSafraAtivaListener listener) {
        if (safraAtiva != null && safraAtiva.getId() == safraId && safraAtiva.getAtiva()) {
            return;
        }
        final Safra safra = load(Safra.class, safraId);
        if (safra != null) {
            safra.setAtiva(true);
            save(safra, new OnSaveEntityListener() {
                @Override
                public void onSuccess(Integer... ids) {
                    if (safraAtiva != null) {
                        safraAtiva.setAtiva(false);
                        save(safraAtiva, null);
                    }
                    safraAtiva = safra;
                    if (listener != null) {
                        listener.onSuccess();
                    }
                }

                @Override
                public void onFailure(int errorCode, List<String> errorMessages) {
                    if (listener != null) {
                        listener.onFailure();
                    }
                }
            });
        }
    }

    public interface OnSaveEntityListener {
        public void onSuccess(Integer... ids);

        public void onFailure(int errorCode, List<String> errorMessages);
    }

    public interface OnDeleteEntityListener {
        public void onSuccess(Integer... ids);

        public void onFailure(int errorCode, List<String> errorMessages);
    }


    private static class DbWriteResult {
        Integer[] ids;
        Integer errorCode;
        ArrayList<String> errorMessages;
    }


    private static class saveTask<T extends BaseIdentifiableModel> extends AsyncTask<T, Void, DbWriteResult> {

        private BaseDao<T> mDao;
        private OnSaveEntityListener mListener;
        private CastanhadoraDatabase mDb;

        saveTask(CastanhadoraDatabase db, OnSaveEntityListener listener) {
            mDb = db;
            mListener = listener;
        }

        @Override
        protected DbWriteResult doInBackground(final T... objs) {
            final DbWriteResult result = new DbWriteResult();


            final ArrayList<Integer> resultIds = new ArrayList<Integer>();

            try {
                mDb.runInTransaction(new Runnable() {
                    @Override
                    public void run() {
                        for (T obj : objs) {
                            if (mDao == null) {
                                mDao = (BaseDao<T>) mDb.getDaoForType(obj.getClass());
                            }
                            if (obj.getId() > 0) {
                                mDao.update(obj);
                                resultIds.add(obj.getId());
                            } else {
                                resultIds.add((int) mDao.insert(obj));
                            }
                        }
                    }
                });
                result.ids = resultIds.toArray(new Integer[0]);
            } catch (SQLiteConstraintException exp) {
                Log.d("CASTANHADORA", "Erro SQLITE: " + exp.getMessage());
                result.errorCode = CastanhadoraDatabase.ERROR_CODE_SQLITE_CONSTRAINT;
                result.errorMessages = new ArrayList<String>();
                result.errorMessages.add(Castanhadora.APP.getResources().getString(
                        R.string.error_db_sqlite_constraint_exception_save));
            }

            return result;
        }

        @Override
        protected void onPostExecute(DbWriteResult result) {
            super.onPostExecute(result);

            if (mListener != null) {
                if (result.errorCode == null) {
                    mListener.onSuccess(result.ids);
                    MediaPlayer mp = MediaPlayer.create(Castanhadora.APP.getApplicationContext(),
                            R.raw.saved);
                    mp.start();
                } else {
                    mListener.onFailure(result.errorCode, result.errorMessages);
                }
            }

        }

    }

    private static class deleteTask<T extends BaseIdentifiableModel> extends AsyncTask<T, Void, DbWriteResult> {

        private BaseDao<T> mDao;
        private OnDeleteEntityListener mListener;
        private CastanhadoraDatabase mDb;

        deleteTask(CastanhadoraDatabase db, OnDeleteEntityListener listener) {
            mDb = db;
            mListener = listener;
        }

        @Override
        protected DbWriteResult doInBackground(final T... objs) {
            final DbWriteResult result = new DbWriteResult();


            final ArrayList<Integer> resultIds = new ArrayList<Integer>();

            try {
                mDb.runInTransaction(new Runnable() {
                    @Override
                    public void run() {
                        for (T obj : objs) {
                            if (mDao == null) {
                                mDao = (BaseDao<T>) mDb.getDaoForType(obj.getClass());
                            }
                            if (obj.getId() > 0) {
                                Integer objId = obj.getId();
                                if (mDao instanceof TrabalhoUpfDao) {
                                    ((TrabalhoUpfDao) mDao).deleteTrabalho((TrabalhoUpf) obj);
                                } else {
                                    mDao.delete(obj);
                                }
                                resultIds.add(objId);
                            }
                        }
                    }
                });
                result.ids = resultIds.toArray(new Integer[0]);
            } catch (SQLiteConstraintException exp) {
                Log.d("CASTANHADORA", "Erro SQLITE: " + exp.getMessage());
                result.errorCode = CastanhadoraDatabase.ERROR_CODE_SQLITE_CONSTRAINT;
                result.errorMessages = new ArrayList<String>();
                result.errorMessages.add(Castanhadora.APP.getResources().getString(
                        R.string.error_db_sqlite_constraint_exception_delete));
            }

            return result;
        }

        @Override
        protected void onPostExecute(DbWriteResult result) {
            super.onPostExecute(result);

            if (mListener != null) {
                if (result.errorCode == null) {
                    mListener.onSuccess(result.ids);
                } else {
                    mListener.onFailure(result.errorCode, result.errorMessages);
                }
            }
        }
    }

    public class SaveMultipleCounter {
        private Integer countSuccessesNeeded, countSuccessesAchieved;
        private OnSaveEntityListener listener;
        private Integer[] ids;

        public SaveMultipleCounter(Integer countSuccessesNeeded, OnSaveEntityListener listener) {
            this.countSuccessesNeeded = countSuccessesNeeded;
            this.countSuccessesAchieved = 0;
            this.listener = listener;
        }

        public void setResponse(Integer... ids) {
            this.ids = ids;
        }

        public void achieveSuccess() {
            countSuccessesAchieved++;
            if (countSuccessesNeeded == countSuccessesAchieved) {
                listener.onSuccess(ids);
            }
        }
    }
}
