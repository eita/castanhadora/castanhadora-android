package br.coop.eita.castanhadora.db.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Query;
import androidx.room.Transaction;

import java.util.ArrayList;
import java.util.List;

import br.coop.eita.castanhadora.db.models.TrabalhoUpfIntegrante;
import br.coop.eita.castanhadora.db.models.UpfIntegrante;
import br.coop.eita.castanhadora.db.models.derived.UpfIntegranteTrabalha;

@Dao
public abstract class TrabalhoUpfIntegranteDao extends BaseDao<TrabalhoUpfIntegrante> {

    public void updateIntegranteTrabalhaList(List<UpfIntegranteTrabalha> integranteTrabalhaList,
                                             Integer trabalhoId) {
        ArrayList<TrabalhoUpfIntegrante> integranteList = new ArrayList<TrabalhoUpfIntegrante>();
        for (UpfIntegranteTrabalha integranteTrabalha : integranteTrabalhaList) {
            if (integranteTrabalha.getTrabalha()) {
                integranteList.add(new TrabalhoUpfIntegrante(trabalhoId.intValue(),
                        integranteTrabalha.getUpfIntegrante().getId()));
            }
        }
        updateData(integranteList,trabalhoId);
    }

    @Transaction
    public void updateData(List<TrabalhoUpfIntegrante> integranteList, Integer trabalhoId) {
        deleteAll(trabalhoId);
        insert(integranteList);
    }

    @Query("DELETE FROM " + TrabalhoUpfIntegrante.TABLE_NAME + " WHERE trabalho_upf_id = :trabalhoId")
    public abstract void deleteAll(Integer trabalhoId);

    @Query("SELECT * FROM " + TrabalhoUpfIntegrante.TABLE_NAME + " WHERE trabalho_upf_id = :trabalhoId")
    public abstract List<TrabalhoUpfIntegrante> getIntegrantesByTrabalhoId(Integer trabalhoId);
}
