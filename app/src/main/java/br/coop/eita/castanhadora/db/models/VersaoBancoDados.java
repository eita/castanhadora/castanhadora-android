package br.coop.eita.castanhadora.db.models;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = VersaoBancoDados.TABLE_NAME)
public class VersaoBancoDados extends BaseIdentifiableModel {
    public static final String TABLE_NAME = "versao_banco_dados";

    @PrimaryKey(autoGenerate = true)
    private int id;

    @NonNull
    @ColumnInfo(name = "versao")
    private String versao;

    public VersaoBancoDados() {
    }

    @Ignore
    public VersaoBancoDados(@NonNull String versao) {
        this.versao = versao;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @NonNull
    public String getVersao() {
        return versao;
    }

    public void setVersao(@NonNull String versao) {
        this.versao = versao;
    }
}


