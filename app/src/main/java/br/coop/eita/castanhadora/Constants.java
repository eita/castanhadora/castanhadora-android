package br.coop.eita.castanhadora;

public class Constants {
    public static final int SAFRA_ATUAL_INEXISTENTE = 1;
    public static final int DADOS_PESSOAIS_NAO_CADASTRADOS = 2;
    public static final int NENHUM_INTEGRANTE_CADASTRADO = 3;
    public static final int UNIDADES_NAO_ESPECIFICADAS = 4;
    public static final int CASTANHAL_INCOMPLETO = 5;
    public static final int REMUNERACAO_NAO_ESPECIFICADA = 6;
    public static final int DADOS_INICIAIS_COMPLETOS = 7;

    public static final int NENHUMA_SAFRA_ABERTA = 100;

    public static final String NOME_BASE_DADOS = "castanhadora_db";

}
