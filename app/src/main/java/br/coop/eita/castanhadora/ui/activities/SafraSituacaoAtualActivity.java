package br.coop.eita.castanhadora.ui.activities;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import java.math.BigDecimal;
import java.util.ArrayList;

import br.coop.eita.castanhadora.R;
import br.coop.eita.castanhadora.databinding.ActivitySafraInicioBinding;
import br.coop.eita.castanhadora.databinding.ActivitySafraSituacaoAtualBinding;
import br.coop.eita.castanhadora.databinding.ActivitySafraVendaSimulacaoBinding;
import br.coop.eita.castanhadora.db.models.Safra;
import br.coop.eita.castanhadora.db.models.Upf;
import br.coop.eita.castanhadora.db.models.Venda;
import br.coop.eita.castanhadora.db.models.derived.TotalGeral;
import br.coop.eita.castanhadora.util.DialogoCastanhadoraGridViewItem;
import br.coop.eita.castanhadora.util.Util;
import br.coop.eita.castanhadora.viewmodels.VendaViewModel;


public class SafraSituacaoAtualActivity extends AppCompatActivity {

    private VendaViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final ActivitySafraSituacaoAtualBinding binding = DataBindingUtil.setContentView(this,
                R.layout.activity_safra_situacao_atual);

        viewModel = ViewModelProviders.of(this).get(VendaViewModel.class);

        TotalGeral totalGeral = viewModel.getTotalGeral();

        setTitle(getResources().getString(R.string.title_activity_situacao_atual));

        String unidade = viewModel.getUpf().getUnidadeValor();

        BigDecimal precoSugerido = totalGeral.getPrecoSugerido();
        String precoSugeridoStr = "";

        if (precoSugerido == null) {
            precoSugeridoStr = getResources().getString(
                    R.string.castanhadora_nao_ha_preco_sugerido);
        } else if (precoSugerido.compareTo(BigDecimal.ZERO) <= 0) {
            precoSugeridoStr = getResources().getString(
                    R.string.castanhadora_qualquer_preco);
        } else {
            precoSugeridoStr = Util.formatMoney(totalGeral.getPrecoSugerido()) + " por " +
                    unidade.substring(0, unidade.length() - 1);
        }

        if (viewModel.getSafraAtiva() != null) {
            populateCastanhadora(binding, viewModel.getSafraAtiva(), precoSugeridoStr,
                    Util.formatMoney(totalGeral.getValorTotalVendido()),
                    Util.formatMoney(totalGeral.getValorTotalDespesas()),
                    Util.formatMoney(totalGeral.getValorDepreciacao()),
                    Util.formatMoney(totalGeral.getRemuneracaoDesejada()),
                    Util.formatMoney(totalGeral.getExcedenteAtual()),
                    Util.formatNumber(totalGeral.getQtdeEstoque()) + " " + unidade,
                    Util.formatNumber(totalGeral.getQtdeVendido()) + " " + unidade,
                    viewModel.getSafraAtiva().getPeriodo().toString() + " meses"
            );
        } else {
            populateCastanhadora(binding, null, "", "", "", "", "", "", "", "", "");
        }


        TotalGeral.DadosSimulacaoVenda dadosSimulacaoVenda = totalGeral.simularVenda(
                BigDecimal.ZERO, 1);
        ArrayList<String> informacoesConclusao = new ArrayList<>();

        //Cobre custos totais de despesas?
        Integer simulacaoCoberturaDespesas = dadosSimulacaoVenda.getSimulacaoCoberturaDespesas();
        if (simulacaoCoberturaDespesas == 0) {
            informacoesConclusao.add(getResources().getString(R.string.venda_sim_producao_nao));
        } else if (simulacaoCoberturaDespesas == 100) {
            informacoesConclusao.add(getResources().getString(R.string.venda_sim_producao_sim));
        } else {
            informacoesConclusao.add(getResources().getString(R.string.venda_sim_producao_parcial,
                    simulacaoCoberturaDespesas.toString() + "%"));
        }

        //Cobre custos totais de remuneração familiar?
        if (Util.deveInserirRemuneracao(viewModel.getSafraAtiva())) {
            Integer simulacaoCoberturaRemuneracao = dadosSimulacaoVenda.getSimulacaoCoberturaRemuneracao();
            if (simulacaoCoberturaRemuneracao == 0) {
                informacoesConclusao.add(getResources().getString(R.string.venda_sim_remuneracao_nao));
            } else if (simulacaoCoberturaRemuneracao == 100) {
                informacoesConclusao.add(getResources().getString(R.string.venda_sim_remuneracao_sim));
            } else {
                informacoesConclusao.add(
                        getResources().getString(R.string.venda_sim_remuneracao_parcial,
                                simulacaoCoberturaRemuneracao.toString() + "%"));
            }
        }


        //Cobre custos totais de depreciação?
        Integer simulacaoCoberturaDepreciacao = dadosSimulacaoVenda.getSimulacaoCoberturaDepreciacao();
        if (simulacaoCoberturaDepreciacao == 0) {
            informacoesConclusao.add(getResources().getString(R.string.venda_sim_depreciacao_nao));
        } else if (simulacaoCoberturaDepreciacao == 100) {
            informacoesConclusao.add(getResources().getString(R.string.venda_sim_depreciacao_sim));
        } else {
            informacoesConclusao.add(
                    getResources().getString(R.string.venda_sim_depreciacao_parcial,
                            simulacaoCoberturaDepreciacao.toString() + "%"));
        }

        //Tem excedente?
        BigDecimal simulacaoExcedente = dadosSimulacaoVenda.getSimulacaoExcedente();
        if (simulacaoExcedente.equals(BigDecimal.ZERO)) {
            informacoesConclusao.add(getResources().getString(R.string.venda_sim_excedente_nao));
        } else {
            informacoesConclusao.add(getResources().getString(R.string.venda_sim_excedente_sim,
                    Util.formatMoney(simulacaoExcedente)));
        }

        binding.conclusaoInformacoes.setText(TextUtils.join("\n", informacoesConclusao));
    }

    private void populateCastanhadora(ActivitySafraSituacaoAtualBinding binding, Safra safra, String precoSugerido,
                                      String faturamento, String despesas, String depreciacao,
                                      String remuneracaoAlmejada, String excedente, String estoque,
                                      String qtdeVendida, String periodo) {

        ArrayList<DialogoCastanhadoraGridViewItem> valores = new ArrayList<>();

        valores.add(new DialogoCastanhadoraGridViewItem(
                getResources().getString(R.string.safra_totais_preco_sugerido), precoSugerido));

        valores.add(new DialogoCastanhadoraGridViewItem(
                getResources().getString(R.string.safra_totais_vendas), faturamento));

        valores.add(new DialogoCastanhadoraGridViewItem(
                getResources().getString(R.string.safra_totais_despesas), despesas, true));

        valores.add(new DialogoCastanhadoraGridViewItem(
                getResources().getString(R.string.safra_totais_depreciacao), depreciacao, true));

        if (Util.deveInserirRemuneracao(safra)) {
            valores.add(new DialogoCastanhadoraGridViewItem(
                    getResources().getString(R.string.safra_totais_remuneracao_almejada),
                    remuneracaoAlmejada, true));
        }

        valores.add(new DialogoCastanhadoraGridViewItem(
                getResources().getString(R.string.excedente), excedente));

        valores.add(new DialogoCastanhadoraGridViewItem(
                getResources().getString(R.string.producao_em_estoque), estoque));

        valores.add(new DialogoCastanhadoraGridViewItem(
                getResources().getString(R.string.venda_qtde_vendida), qtdeVendida));

        binding.painelSintese.setValores(valores);
    }

    public void fechar(View view) {
        finish();
    }
}
