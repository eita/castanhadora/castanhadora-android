package br.coop.eita.castanhadora.ui.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import br.coop.eita.castanhadora.Castanhadora;
import br.coop.eita.castanhadora.R;
import br.coop.eita.castanhadora.databinding.ActivitySafraVendaFormBinding;
import br.coop.eita.castanhadora.databinding.ActivitySafraVendaInicioFormBinding;
import br.coop.eita.castanhadora.db.DataRepository;
import br.coop.eita.castanhadora.util.DialogoCastanhadoraGridViewItem;
import br.coop.eita.castanhadora.util.Util;
import br.coop.eita.castanhadora.viewmodels.SafraViewModel;
import br.coop.eita.castanhadora.viewmodels.VendaViewModel;

public class SafraVendaFormActivity extends AppCompatActivity {

    VendaViewModel viewModel;

    Integer quantidadeVendaOriginal;

    public static final String VENDA_ID_REPLY = "br.coop.eita.castanhadora.venda_id.REPLY";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        final ActivitySafraVendaFormBinding binding = DataBindingUtil.setContentView(this,
                R.layout.activity_safra_venda_form);

        viewModel = ViewModelProviders.of(this).get(VendaViewModel.class);
        binding.setViewmodel(viewModel);

        int vendaId = getIntent().getIntExtra("venda_id", 0);

        if (vendaId > 0) {
            viewModel.loadVendaAtual(vendaId);
            quantidadeVendaOriginal = viewModel.getVendaAtual().getQuantidade();
            binding.vendaQuantidade.setEnabled(true);
            binding.vendaValor.setEnabled(true);
            binding.vendaValorTotal.setEnabled(false);;
        } else {
            quantidadeVendaOriginal = 0;
            binding.vendaQuantidade.setEnabled(false);
            binding.vendaValor.setEnabled(false);
            binding.vendaValorTotal.setEnabled(false);
        }
        setTitle(vendaId > 0 ? R.string.label_dialog_venda_edit_title : R.string.title_venda_fechar);
    }

    public void onClickFinalizarVenda(View view) {
        final SafraVendaFormActivity self = this;
        SafraViewModel safraViewModel = ViewModelProviders.of(this).get(SafraViewModel.class);
        safraViewModel.ativarSafra(viewModel.getSafraAtiva().getId());
        if (viewModel.getVendaAtual().getQuantidade() > safraViewModel.getProducaoEmEstoque() + quantidadeVendaOriginal) {
            Toast.makeText(self, Castanhadora.APP.getResources().getString(
                    R.string.error_quant_venda_maior_estoque), Toast.LENGTH_LONG).show();
        } else {
            viewModel.saveVenda(new DataRepository.OnSaveEntityListener() {
                @Override
                public void onSuccess(Integer... ids) {
                    Intent replyIntent2 = new Intent();
                    replyIntent2.putExtra(VENDA_ID_REPLY, ids[0]);
                    setResult(RESULT_OK, replyIntent2);

                    //Opens result Screen
                    Intent summaryScreenIntent = new Intent(SafraVendaFormActivity.this,
                            SafraVendaSummaryActivity.class);
                    summaryScreenIntent.putExtra("venda_id", ids[0]);
                    startActivity(summaryScreenIntent);
                    finish();
                }

                @Override
                public void onFailure(int errorCode, List<String> errorMessages) {
                    Toast.makeText(self, errorMessages.get(0), Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent replyIntent = new Intent();
                setResult(RESULT_CANCELED, replyIntent);
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
