package br.coop.eita.castanhadora.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import androidx.databinding.DataBindingUtil;

import java.math.BigDecimal;
import java.util.List;

import br.coop.eita.castanhadora.R;
import br.coop.eita.castanhadora.databinding.ListitemTrabalhoEIntegrantesUpfBinding;
import br.coop.eita.castanhadora.db.models.derived.TrabalhoEIntegrantesUpf;
import br.coop.eita.castanhadora.ui.activities.SafraTrabalhosUpfListActivity;
import br.coop.eita.castanhadora.util.Util;

public class TrabalhoEIntegrantesUpfAdapter extends ArrayAdapter<TrabalhoEIntegrantesUpf> {
    private SafraTrabalhosUpfListActivity activity;
    private BigDecimal remuneracaoAlmejadaSafra;
    private String tituloCard;

    public TrabalhoEIntegrantesUpfAdapter(Context context, List<TrabalhoEIntegrantesUpf> objects) {
        super(context, 0, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ListitemTrabalhoEIntegrantesUpfBinding binding;

        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.listitem_trabalho_e_integrantes_upf, null);
            binding = DataBindingUtil.bind(convertView);
            convertView.setTag(binding);
        } else {
            binding = (ListitemTrabalhoEIntegrantesUpfBinding) convertView.getTag();
        }

        final TrabalhoEIntegrantesUpf trabalhoEIntegrantesUpf = getItem(position);
        final TrabalhoEIntegrantesUpfAdapter self = this;

        binding.setTrabalho(trabalhoEIntegrantesUpf);

        binding.editBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.openEditForm(trabalhoEIntegrantesUpf.getTrabalhoUpf());
            }
        });

        binding.removeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.openDeleteDialog(trabalhoEIntegrantesUpf.getTrabalhoUpf());
            }
        });

        // binding.subText.setText(trabalhoEIntegrantesUpf.getSubtituloCard(remuneracaoAlmejadaSafra));
        binding.primaryText.setText(trabalhoEIntegrantesUpf.getTituloCard(remuneracaoAlmejadaSafra));

        return binding.getRoot();
    }

    public void setActivity(SafraTrabalhosUpfListActivity activity) {
        this.activity = activity;
    }

    public void setRemuneracaoAlmejadaSafra(BigDecimal remuneracaoAlmejadaSafra) {
        this.remuneracaoAlmejadaSafra = remuneracaoAlmejadaSafra;
    }

    public void setTituloCard() {
        this.tituloCard = tituloCard;
    }

}
