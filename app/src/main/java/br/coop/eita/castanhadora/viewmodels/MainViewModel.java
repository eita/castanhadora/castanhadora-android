package br.coop.eita.castanhadora.viewmodels;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.databinding.Bindable;

import br.coop.eita.castanhadora.BR;
import br.coop.eita.castanhadora.db.models.Safra;
import br.coop.eita.castanhadora.db.models.Upf;

public class MainViewModel extends BaseViewModel {

    private Boolean parentSafraInicio = false;

    public MainViewModel(@NonNull Application application) {
        super(application);
    }

    @Bindable
    public Safra getSafraAtiva() {
        return dataRepository.getSafraAtiva();
    }

    public void setSafraAtiva(Safra safra) {
        dataRepository.setSafraAtiva(safra);
        notifyPropertyChanged(BR.safraAtiva);
    }

    public void setSafraAtiva(Integer safraId) {
        dataRepository.setSafraAtiva(safraId);
        notifyPropertyChanged(BR.safraAtiva);
    }

    public Integer getSafraCount() {
        return dataRepository.getSafraCount();
    }

    public Upf getUpf() {
        return dataRepository.getUpf();
    }

    public Boolean getParentSafraInicio() {
        return parentSafraInicio;
    }

    public void setParentSafraInicio(Boolean parentSafraInicio) {
        this.parentSafraInicio = parentSafraInicio;
    }

    public Integer getStatusInicio() {
        return dataRepository.getStatusInicio();
    }
}
