package br.coop.eita.castanhadora.ui.activities;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RadioButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import java.util.List;

import br.coop.eita.castanhadora.R;
import br.coop.eita.castanhadora.databinding.ActivityPermissaoCompartilharNuvemBinding;
import br.coop.eita.castanhadora.db.DataRepository;
import br.coop.eita.castanhadora.ui.util.Wizardable;
import br.coop.eita.castanhadora.viewmodels.UpfViewModel;

public class PermissaoCompartilharNuvemActivity extends AppCompatActivity implements Wizardable {

    private UpfViewModel mUpfViewModel;

    private Boolean isWizard;

    final private int REQUEST_CODE_ASK_PERMISSIONS = 123;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        isWizard = getIntent().getBooleanExtra("wizard", false);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null && !isWizard) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        ActivityPermissaoCompartilharNuvemBinding binding = DataBindingUtil.setContentView(this,
                R.layout.activity_permissao_compartilhar_nuvem);

        mUpfViewModel = ViewModelProviders.of(this).get(UpfViewModel.class);
        binding.setViewmodel(mUpfViewModel);
        binding.setWizard(isWizard);
    }

    public void onRadioCmpartilharSafraClicked(View view) {
        // Is the home_action_nova_despesa now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio home_action_nova_despesa was clicked
        switch (view.getId()) {
            case R.id.radioCompartlharSafraNao:
                if (checked) mUpfViewModel.getUpf().setCompartilharSafra(false);
                break;
            case R.id.radioCompartlharSafraSim:
                if (checked) mUpfViewModel.getUpf().setCompartilharSafra(true);
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!isWizard) {
            getMenuInflater().inflate(R.menu.menu_form, menu);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mUpfViewModel.reloadUpf();
                finish();
                return true;
            case R.id.action_save:
                if (mUpfViewModel.getUpf().getCompartilharSafra()) {
                    askPermissionShareLocation();
                } else {
                    saveUpf(new AfterSaveListener() {
                        @Override
                        public void doAfterSave(Integer id) {
                            afterSave();
                        }
                    });
                }

                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private interface AfterSaveListener {
        void doAfterSave(Integer id);
    }

    @Override
    public void onClickWizardPrevious(View view) {
        Intent intent = new Intent(this, SafraRemuneracaoDesejadaActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        intent.putExtra("wizard", true);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        finish();
    }

    @Override
    public void onClickWizardNext(View view) {
        final PermissaoCompartilharNuvemActivity self = this;
        saveUpf(new AfterSaveListener() {
            @Override
            public void doAfterSave(Integer id) {
                if (mUpfViewModel.getUpf().getCompartilharSafra()) {
                    askPermissionShareLocation();
                } else {
                    afterSave();
                }
            }
        });
    }

    public void saveUpf(final AfterSaveListener afterSaveListener) {
        final PermissaoCompartilharNuvemActivity self = this;
        mUpfViewModel.saveUpf(new DataRepository.OnSaveEntityListener() {
            @Override
            public void onSuccess(Integer... ids) {
                afterSaveListener.doAfterSave(ids[0]);
            }

            @Override
            public void onFailure(int errorCode, List<String> errorMessages) {
                Toast.makeText(self, errorMessages.get(0), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void afterSave(){
        if (!isWizard) {
            Intent replyIntent2 = new Intent();
            setResult(RESULT_OK, replyIntent2);
            Toast.makeText(this, R.string.saved_permissao_compartilhar, Toast.LENGTH_LONG).show();
            finish();
        } else {
            Intent intent = new Intent(this, WizardFimActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            intent.putExtra("wizard", true);
            startActivity(intent);
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            finish();
        }
    }

    private void askPermissionShareLocation() {
        if ( Build.VERSION.SDK_INT >= 23){
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) !=
                    PackageManager.PERMISSION_GRANTED){
                requestPermissions(new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CODE_ASK_PERMISSIONS);
            } else {
                saveUpf(new AfterSaveListener() {
                    @Override
                    public void doAfterSave(Integer id) {
                        afterSave();
                    }
                });
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    saveUpf(new AfterSaveListener() {
                        @Override
                        public void doAfterSave(Integer id) {
                            afterSave();
                        }
                    });
                } else {
                    // Permission Denied
                    Toast.makeText( this,R.string.compartilhar_localizacao_para_continuar , Toast.LENGTH_SHORT)
                            .show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}
