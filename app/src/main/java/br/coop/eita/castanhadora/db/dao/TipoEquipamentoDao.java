package br.coop.eita.castanhadora.db.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Query;

import java.util.List;

import br.coop.eita.castanhadora.db.models.Safra;
import br.coop.eita.castanhadora.db.models.TipoEquipamento;

@Dao
public abstract class TipoEquipamentoDao extends BaseIdentifiableDao<TipoEquipamento> {

    @Query("SELECT * FROM " + TipoEquipamento.TABLE_NAME + " ORDER BY nome")
    public abstract LiveData<List<TipoEquipamento>> getTiposEquipamento();
}
