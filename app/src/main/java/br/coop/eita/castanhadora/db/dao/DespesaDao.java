package br.coop.eita.castanhadora.db.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Query;

import java.math.BigDecimal;
import java.util.List;

import br.coop.eita.castanhadora.db.models.Despesa;
import br.coop.eita.castanhadora.db.models.Equipamento;

@Dao
public abstract class DespesaDao extends BaseIdentifiableDao<Despesa> {

    @Query("SELECT * FROM " + Despesa.TABLE_NAME + " WHERE safra_id = :safraId ORDER BY data DESC")
    public abstract LiveData<List<Despesa>> getDespesasLive(Integer safraId);

    @Query("SELECT * FROM " + Despesa.TABLE_NAME + " WHERE safra_id = :safraId ORDER BY data DESC")
    public abstract List<Despesa> getDespesas(Integer safraId);

    @Query("SELECT * FROM " + Despesa.TABLE_NAME + " WHERE safra_id = :safraId ORDER BY data DESC LIMIT 1")
    public abstract Despesa getLastDespesa (Integer safraId);

    @Query("SELECT SUM(valor) FROM " + Despesa.TABLE_NAME + " WHERE safra_id = :safraId and tipo = :tipo")
    public abstract BigDecimal getTotalDespesas(Integer safraId, String tipo);

}
