package br.coop.eita.castanhadora.ui.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import java.util.List;

import br.coop.eita.castanhadora.R;
import br.coop.eita.castanhadora.databinding.ActivitySafraProducoesListBinding;
import br.coop.eita.castanhadora.db.models.Despesa;
import br.coop.eita.castanhadora.db.models.Equipamento;
import br.coop.eita.castanhadora.db.models.Producao;
import br.coop.eita.castanhadora.db.models.derived.EquipamentoETipo;
import br.coop.eita.castanhadora.ui.adapters.DespesaAdapter;
import br.coop.eita.castanhadora.ui.adapters.ProducaoAdapter;
import br.coop.eita.castanhadora.viewmodels.ProducaoViewModel;

public class SafraProducoesListActivity extends AppCompatActivity {

    public static final int EDITAR_PRODUCAO_ID_REPLY = 1;

    private ProducaoViewModel viewModel;

    private ProducaoAdapter producaoAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        final ActivitySafraProducoesListBinding binding = DataBindingUtil.setContentView(this,
                R.layout.activity_safra_producoes_list);

        viewModel = ViewModelProviders.of(this).get(ProducaoViewModel.class);
        binding.setViewmodel(viewModel);


        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(R.string.label_producoes);
        }

        LiveData<List<Producao>> producoes = viewModel.getProducoes();

        final SafraProducoesListActivity self = this;

        producoes.observe(this, new Observer<List<Producao>>() {
            @Override
            public void onChanged(List<Producao> prods) {
                if (prods != null) {
                    if (prods.size() == 0) {
                        Toast.makeText(self, R.string.list_sem_producoes, Toast.LENGTH_LONG).show();
                        finish();
                    }
                    producaoAdapter = new ProducaoAdapter(getApplicationContext(), prods);
                    binding.producaoList.setAdapter(producaoAdapter);
                    producaoAdapter.setUnidadeQtdeCastanhas(viewModel.getUpf().unidadeValor);
                    producaoAdapter.setActivity(self);
                    producaoAdapter.notifyDataSetChanged();
                }
            }
        });

    }

    public void openEditForm(Producao producao) {
        Intent intent = new Intent(SafraProducoesListActivity.this, SafraProducaoFormActivity.class).putExtra(
                "producao_id", producao.getId());
        startActivityForResult(intent, EDITAR_PRODUCAO_ID_REPLY);
    }

    public void openDeleteDialog(final Producao producao) {
        new AlertDialog.Builder(this)
                .setMessage(getResources().getString(R.string.msg_dialog_producao_remover,producao.getDescricao()))
                .setPositiveButton(R.string.dialog_sim, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        viewModel.deleteProducao(producao);
                    }
                })
                .setNegativeButton(R.string.dialog_nao, null)
                .setTitle(R.string.label_dialog_producao_delete_title)
                .show();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent replyIntent = new Intent();
                setResult(RESULT_CANCELED, replyIntent);
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
