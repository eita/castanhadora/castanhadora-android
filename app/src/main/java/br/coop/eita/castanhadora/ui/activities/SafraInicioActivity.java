package br.coop.eita.castanhadora.ui.activities;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import br.coop.eita.castanhadora.BuildConfig;
import br.coop.eita.castanhadora.R;
import br.coop.eita.castanhadora.databinding.ActivitySafraInicioBinding;
import br.coop.eita.castanhadora.databinding.DialogoSelectTipoLancamentoBinding;
import br.coop.eita.castanhadora.databinding.DialogoSincNuvemBinding;
import br.coop.eita.castanhadora.db.DataRepository;
import br.coop.eita.castanhadora.db.models.Safra;
import br.coop.eita.castanhadora.db.models.derived.TotalGeral;
import br.coop.eita.castanhadora.ui.adapters.DialogoCastanhadoraSafraInicioAdapter;
import br.coop.eita.castanhadora.ui.fragments.SelectTipoDespesaDialogFragment;
import br.coop.eita.castanhadora.ui.fragments.SelectTipoLancamentoDialogFragment;
import br.coop.eita.castanhadora.ui.util.SafraSummaryBuilder;
import br.coop.eita.castanhadora.util.DbCastanhadoraUtil;
import br.coop.eita.castanhadora.util.DialogoCastanhadoraGridViewItem;
import br.coop.eita.castanhadora.util.Util;
import br.coop.eita.castanhadora.viewmodels.DespesaViewModel;
import br.coop.eita.castanhadora.viewmodels.EquipamentoViewModel;
import br.coop.eita.castanhadora.viewmodels.ProducaoViewModel;
import br.coop.eita.castanhadora.viewmodels.SafraViewModel;
import br.coop.eita.castanhadora.viewmodels.TrabalhoUpfViewModel;
import br.coop.eita.castanhadora.viewmodels.UpfIntegranteViewModel;
import br.coop.eita.castanhadora.viewmodels.VendaViewModel;


public class SafraInicioActivity extends AppCompatActivity {

    public static final int CRIAR_PRODUCAO_ACTIVITY_REQUEST_CODE = 1;
    public static final int CRIAR_EQUIPAMENTO_ACTIVITY_REQUEST_CODE = 2;
    public static final int OPCOES_REQUEST_CODE = 3;
    public static final int CRIAR_TRABALHO_ACTIVITY_REQUEST_CODE = 4;
    public static final int CRIAR_VENDA_ACTIVITY_REQUEST_CODE = 5;
    public static final int LISTAR_SAFRAS_ACTIVITY_REQUEST_CODE = 6;
    public static final int CAMINHO_BACKUP_REQUEST_CODE = 7;


    private SafraViewModel viewModel;
    private DialogoCastanhadoraSafraInicioAdapter dialogoCastanhadoraSafraInicioAdapter;

    private Uri uriImportacaoBackup;
    private SafraSummaryBuilder safraSummaryBuilder;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CAMINHO_BACKUP_REQUEST_CODE && resultCode == RESULT_OK && data != null) {
            this.uriImportacaoBackup = data.getData();
            importarArquivoBackup();
        } else if (resultCode == Safra.RESULT_SAFRA_ATIVA_ALTERADA) {
            Safra safraAtiva = viewModel.getSafraAtiva();
            if (safraAtiva.necessitaWizard()) {
                Intent intent = new Intent(this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            } else {
                Toast.makeText(this, getResources().getString(R.string.safra_ativa_alterada,
                        safraAtiva.getNome()), Toast.LENGTH_LONG).show();
                recreate();
            }
        }


/*
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Safra.RESULT_SAFRA_ATIVA_ALTERADA) {
            Intent replyIntent2 = new Intent();
            replyIntent2.putExtra(SAFRA_ID_REPLY, id);
            if (isNew) {
                viewModel.ativarSafra(id);
                setResult(Safra.RESULT_SAFRA_ATIVA_ALTERADA);
            } else {
                setResult(RESULT_OK, replyIntent2);
            }

            finish();
            recreate();
        }
        */
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent viewIntent = getIntent();
        String action = viewIntent.getAction();
        String type = viewIntent.getType();

        if (Intent.ACTION_VIEW.equals(action) && type != null) {
            if (!DbCastanhadoraUtil.getFileNameFromUri(viewIntent.getData(), getApplicationContext()).toLowerCase().contains("castanhadora")) {
                exibirDialogoFalhaImportacaoBackup();
            } else {
                this.uriImportacaoBackup = viewIntent.getData();
                exibirDialogoConfirmacaoImportacao();
            }
        }

        viewModel = ViewModelProviders.of(this).get(SafraViewModel.class);
        Safra safraAtiva = viewModel.getSafraAtiva();
        //Fazemos mais uma checagem para não deixar entrar nesta tela se não tiver safra ativa
        if (safraAtiva == null) {
            Intent intent = new Intent(this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
            return;
        }

        final ActivitySafraInicioBinding binding = DataBindingUtil.setContentView(this,
                R.layout.activity_safra_inicio);

        binding.setViewmodel(viewModel);

        binding.setLifecycleOwner(this);

        LiveData<TotalGeral> totalGeral = viewModel.getTotalGeralLive();

        populateCastanhadora(binding, "", "", "", "", "", "", "", "", "", "", "");

        totalGeral.observe(this, new Observer<TotalGeral>() {
            @Override
            public void onChanged(TotalGeral totalGeral) {
                Integer producaoTotal = viewModel.getProducaoTotal();
                Integer producaoEmEstoque = viewModel.getProducaoEmEstoque();

                String unidade = viewModel.getUpf().getUnidadeValor();

                BigDecimal precoSugerido = totalGeral.getPrecoSugerido();
                String precoSugeridoStr = "";

                if (precoSugerido == null) {
                    precoSugeridoStr = getResources().getString(
                            R.string.castanhadora_nao_ha_preco_sugerido);
                } else if (precoSugerido.compareTo(BigDecimal.ZERO) <= 0) {
                    precoSugeridoStr = getResources().getString(
                            R.string.castanhadora_qualquer_preco);
                } else {
                    precoSugeridoStr = Util.formatMoney(totalGeral.getPrecoSugerido()) + " por " +
                            unidade.substring(0, unidade.length() - 1);
                }

                if (viewModel.getSafraAtiva() != null) {
                    populateCastanhadora(
                            binding, precoSugeridoStr,
                            Util.formatMoney(totalGeral.getValorTotalVendido()),
                            Util.formatMoney(totalGeral.getValorTotalDespesas()),
                            Util.formatMoney(totalGeral.getValorCustosTotais()),
                            Util.formatMoney(totalGeral.getValorDepreciacao()),
                            Util.formatMoney(totalGeral.getRemuneracaoDesejada()),
                            Util.formatNumber(totalGeral.getQtdeEstoque()) + " " + unidade,
                            Util.formatNumber(totalGeral.getQtdeVendido()) + " " + unidade,
                            Util.formatNumber(totalGeral.getQtdeVendido()) + " / " + Util.formatNumber(totalGeral.getQtdeProduzido()) + " " + unidade,
                            viewModel.getSafraAtiva().getPeriodo().toString() + " meses",
                            Util.formatMoney(totalGeral.getExcedenteAtual())
                    );
                }
            }
        });

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            LiveData<Safra> safra = viewModel.getSafraAtivaLive();
            safra.observe(this, new Observer<Safra>() {
                @Override
                public void onChanged(Safra safra) {
                    if (safra != null) {
                        actionBar.setTitle(getResources().getString(R.string.safra_inicio_titulo, safra.getNome()));
                    } else {
                        actionBar.setTitle(getResources().getString(R.string.safra_inicio_titulo, ""));
                    }
                }
            });
        }
    }

    private void populateCastanhadora(
            ActivitySafraInicioBinding binding,
            String precoSugerido,
            String faturamento,
            String despesas,
            String custosTotais,
            String depreciacao,
            String remuneracaoAlmejada,
            String estoque,
            String qtdeVendida,
            String producao,
            String periodo,
            String excedente
    ) {

        ArrayList<DialogoCastanhadoraGridViewItem> valores = new ArrayList<>();

        valores.add(new DialogoCastanhadoraGridViewItem(
                getResources().getString(R.string.safra_totais_preco_sugerido), precoSugerido));

        valores.add(new DialogoCastanhadoraGridViewItem(
                getResources().getString(R.string.safra_totais_vendas), faturamento));

        valores.add(new DialogoCastanhadoraGridViewItem(
                getResources().getString(R.string.safra_totais_custos), custosTotais, true));

//        valores.add(new DialogoCastanhadoraGridViewItem(
//                getResources().getString(R.string.safra_totais_depreciacao), depreciacao));

//        valores.add(new DialogoCastanhadoraGridViewItem(
//                getResources().getString(R.string.safra_totais_remuneracao_almejada),
//                remuneracaoAlmejada));

        valores.add(new DialogoCastanhadoraGridViewItem(
                getResources().getString(R.string.producao), producao));

//        valores.add(new DialogoCastanhadoraGridViewItem(
//                getResources().getString(R.string.venda_qtde_vendida), qtdeVendida));

        /*valores.add(new DialogoCastanhadoraGridViewItem(
                getResources().getString(R.string.safra_totais_periodo), periodo));*/

//        valores.add(new DialogoCastanhadoraGridViewItem(
//                getResources().getString(R.string.excedente), excedente));

        binding.painelSintese.setValores(valores);
        binding.painelSintese.enableSituacaoAtualButton();
    }

    public void openNovaProducaoForm(View view) {
        Intent intent = new Intent(SafraInicioActivity.this, SafraProducaoFormActivity.class);
        startActivityForResult(intent, CRIAR_PRODUCAO_ACTIVITY_REQUEST_CODE);
    }

    public void openVerRelatoriosView(View view) {
    }

    public void openNovaVendaForm(View view) {
        TotalGeral totalGeral = viewModel.getTotalGeral();
        if (totalGeral.getQtdeEstoque() > 0 && totalGeral.getPrecoSugerido() != null) {
            Intent intent = new Intent(SafraInicioActivity.this,
                    SafraVendaInicioFormActivity.class);
            startActivityForResult(intent, CRIAR_VENDA_ACTIVITY_REQUEST_CODE);
        } else {
            Toast.makeText(this, R.string.castanhadora_nao_ha_preco_sugerido,
                    Toast.LENGTH_LONG).show();
        }

    }

    public void openSelectTipoLancamentoDialogFragment(View view) {
        DialogFragment newFragment = SelectTipoLancamentoDialogFragment.newInstance();
        newFragment.show(getSupportFragmentManager(), "dialog");
    }

    public void openEquipamentosList(View view) {
        Intent intent = new Intent(SafraInicioActivity.this, EquipamentoFormActivity.class);
        startActivityForResult(intent, CRIAR_EQUIPAMENTO_ACTIVITY_REQUEST_CODE);
    }

    public void openSituacaoAtualActivity(View view) {
        Intent intent = new Intent(SafraInicioActivity.this, SafraSituacaoAtualActivity.class);
        startActivity(intent);
    }

    public void openNovoTrabalhoForm(View view) {
        Intent intent = new Intent(SafraInicioActivity.this, SafraTrabalhoUpfFormActivity.class);
        startActivityForResult(intent, CRIAR_TRABALHO_ACTIVITY_REQUEST_CODE);
    }

    public void openNovaDespesaForm(View view) {
        // Create and show the dialog.
        DialogFragment newFragment = SelectTipoDespesaDialogFragment.newInstance();
        newFragment.show(getSupportFragmentManager(), "dialog");
    }

    public void openListsIndexDialog(View view) {
        // Create and show the dialog.
        DialogFragment newFragment = SelectTipoLancamentoDialogFragment.newInstance();
        newFragment.show(getSupportFragmentManager(), "dialog");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        Safra safra = viewModel.getSafraAtiva();

        if (safra.getDataFinal() != null) {
            for (int i = 0; i < menu.size(); i++) {
                if (menu.getItem(i).getItemId() == R.id.action_safra_encerrar_safra) {
                    menu.getItem(i).setVisible(false);
                }
            }
        }

        return true;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up home_action_nova_despesa, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        Intent intent;

        //noinspection SimplifiableIfStatement
        switch (id) {
            /*case R.id.action_safra_ver_relatorios:
                break;*/
            /*case R.id.action_safra_alterar_lancamentos:
                DialogFragment newFragment = SelectTipoLancamentoDialogFragment.newInstance();
                newFragment.show(getSupportFragmentManager(), "dialog");
                break;*/
            case R.id.action_safra_mudar_safra:
                Intent intent2 = new Intent(SafraInicioActivity.this, SafraListActivity.class);
                intent2.putExtra("parent_safra", true);
                startActivityForResult(intent2, LISTAR_SAFRAS_ACTIVITY_REQUEST_CODE);
                break;
            case R.id.action_safra_encerrar_safra:
                final SafraInicioActivity self = this;
                final Safra safraAtiva = viewModel.getSafraAtiva();
                viewModel.encerrarSafraAtiva(new DataRepository.OnSaveEntityListener() {
                    @Override
                    public void onSuccess(Integer... ids) {
//                        Toast.makeText(self, getResources().getString(R.string.safra_encerrada,
//                                safraAtiva.getNome()), Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(self, SafraListActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("safra_id_from_encerrar", safraAtiva.getId());
                        startActivity(intent);
                        finish();
                    }

                    @Override
                    public void onFailure(int errorCode, List<String> errorMessages) {

                    }
                });
                break;
            case R.id.action_sobre:
                intent = new Intent(SafraInicioActivity.this, SobreActivity.class);
                startActivityForResult(intent, OPCOES_REQUEST_CODE);
                break;
            case R.id.action_invite_friends:
                try {
                    Intent shareIntent = new Intent(Intent.ACTION_SEND);
                    shareIntent.setType("text/plain");
                    shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Castanhadora");
                    String shareMessage= "\n"+ getResources().getString(R.string.message_recommend_app)+ "\n\n";
                    shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID +"\n\n";
                    shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
                    startActivity(Intent.createChooser(shareIntent, getResources().getString(R.string.message_choose_share)));
                } catch(Exception e) {
                    //e.toString();
                }
                break;
            case R.id.action_videos_tutoriais:
                intent = new Intent(SafraInicioActivity.this, TutoriaisActivity.class);
                startActivity(intent);
                break;
            case R.id.action_safra_share_summary:
                if (viewModel.getUpf().getCompartilharSafra()) {
                    shareSafraSummary();
                } else {
                    changeSharePermissonConfig();
                }
                break;
            case R.id.action_security_copy:
                exibirDialogoCopiaSeguranca();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    public void shareSafraSummary() {
        viewModel = ViewModelProviders.of(this).get(SafraViewModel.class);
        safraSummaryBuilder = new SafraSummaryBuilder(this, viewModel.getTotalGeral(viewModel.getSafraAtiva().getId()), viewModel.getUpf(), viewModel.getSafraAtiva());
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        safraSummaryBuilder.setIncludeValues(true);
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        safraSummaryBuilder.setIncludeValues(false);
                        break;
                    case DialogInterface.BUTTON_NEUTRAL:
                        return;
                }
                sincronizarNuvem();
                safraSummaryBuilder.shareImageUri(safraSummaryBuilder.getImageUri());
            }
        };

        androidx.appcompat.app.AlertDialog.Builder alertDialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(this);
        alertDialogBuilder
                .setTitle(getString(R.string.dialog_share_safra_title))
                .setMessage(getString(R.string.dialog_share_safra) + "\n\n" + getString(R.string.dialog_share_safra2) + "\n\n" + getString(R.string.dialog_include_value))
                .setNeutralButton(getString(R.string.label_dialog_cancel), dialogClickListener)
                .setPositiveButton(getString(R.string.dialog_share_include_value_positive), dialogClickListener)
                .setNegativeButton(getString(R.string.dialog_share_include_value_negative), dialogClickListener)
                .show();
    }

    public void changeSharePermissonConfig() {
        final SafraInicioActivity self = this;
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        Intent intent = new Intent(self, PermissaoCompartilharNuvemActivity.class);
                        startActivity(intent);
                }
            }
        };

        androidx.appcompat.app.AlertDialog.Builder alertDialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(this);
        alertDialogBuilder.setMessage(getString(R.string.dialog_sem_permissao_compartilhar))
                .setPositiveButton(getString(R.string.dialog_alterar_configuracao), dialogClickListener)
                .setNegativeButton(getString(R.string.label_dialog_cancel), dialogClickListener)
                .show();
    }

    private void sincronizarNuvem() {
        EquipamentoViewModel equipamentoViewModel = ViewModelProviders.of(this).get(EquipamentoViewModel.class);
        DespesaViewModel despesaViewModel = ViewModelProviders.of(this).get(DespesaViewModel.class);
        VendaViewModel vendaViewModel = ViewModelProviders.of(this).get(VendaViewModel.class);
        ProducaoViewModel producaoViewModel = ViewModelProviders.of(this).get(ProducaoViewModel.class);
        TrabalhoUpfViewModel trabalhoUpfViewModel = ViewModelProviders.of(this).get(TrabalhoUpfViewModel.class);
        UpfIntegranteViewModel upfIntegranteViewModel = ViewModelProviders.of(this).get(UpfIntegranteViewModel.class);
        safraSummaryBuilder.sincSafra(equipamentoViewModel, despesaViewModel, vendaViewModel, producaoViewModel, trabalhoUpfViewModel, upfIntegranteViewModel);
    }

    private void exibirDialogoConfirmacaoImportacao() {
        new AlertDialog.Builder(this)
            .setMessage(R.string.msg_dialog_certeza_importar)
            .setNegativeButton(R.string.dialog_nao, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    fecharApp();
                }
            })
            .setPositiveButton(R.string.dialog_sim, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    importarArquivoBackup();
                }
            })
            .show();
    }

    private void exibirDialogoFalhaImportacaoBackup() {
        new AlertDialog.Builder(this)
            .setMessage(R.string.msg_dialog_falha_importar_backup)
            .setPositiveButton(R.string.label_dialog_ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    fecharApp();
                }
            })
            .setCancelable(false)
            .show();
    }

    private void exibirDialogoCopiaSeguranca() {
        new AlertDialog.Builder(this)
                .setMessage(R.string.msg_dialog_security_copy)
                .setPositiveButton(R.string.label_importar_backup, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent importIntent = new Intent(Intent.ACTION_GET_CONTENT);
                        importIntent.setType("*/*");
                        startActivityForResult(Intent.createChooser(importIntent, getResources().getString(R.string.selecione_arquivo_backup)), CAMINHO_BACKUP_REQUEST_CODE);
                    }
                })
                .setNegativeButton(R.string.label_exportar_backup, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Context context = getApplicationContext();
                        File arquivoBackupExportado = DbCastanhadoraUtil.backupDatabase(context);
                        compartilharArquivoBackup(arquivoBackupExportado);
                    }
                })
                .setTitle(R.string.label_security_copy)
                .show();
    }

    private void importarArquivoBackup() {
        if (this.uriImportacaoBackup != null) {
            boolean importouSucesso = DbCastanhadoraUtil.importarBackup(this.uriImportacaoBackup, getApplicationContext());
            if (importouSucesso) {
                Toast.makeText(this, R.string.importou_backup, Toast.LENGTH_LONG).show();
                reinicializaApp();
            } else {
                Toast.makeText(this, R.string.nao_importou_backup, Toast.LENGTH_LONG).show();
            }
        }
    }

    private void compartilharArquivoBackup(File arquivoBackupExportado){
        Intent intentShareFile = new Intent(Intent.ACTION_SEND);
        Uri uri = FileProvider.getUriForFile(this, "br.coop.eita.castanhadora", arquivoBackupExportado);
        if (arquivoBackupExportado != null && arquivoBackupExportado.exists()) {
            intentShareFile.setType("application/octet-stream");
            intentShareFile.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.titulo_email));
            intentShareFile.putExtra(Intent.EXTRA_TEXT, getResources().getString(R.string.titulo_email));
            intentShareFile.putExtra(Intent.EXTRA_STREAM, uri);
            intentShareFile.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivity(Intent.createChooser(intentShareFile, getResources().getString(R.string.compartilhar_backup)));
        }
    }

    private void reinicializaApp() {
        Context context = getApplicationContext();
        Intent safraInicioIntent = new Intent(this, SafraInicioActivity.class);
        int mPendingIntentId = 123456;
        PendingIntent mPendingIntent = PendingIntent.getActivity(context, mPendingIntentId, safraInicioIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager mgr = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 3000, mPendingIntent);
        new CountDownTimer(3000, 1000) {
            public void onFinish() {
                System.exit(0);
            }
            public void onTick(long millisUntilFinished) {}
        }.start();
    }

    private void fecharApp() {
        this.finish();
        System.exit(0);
    }
}