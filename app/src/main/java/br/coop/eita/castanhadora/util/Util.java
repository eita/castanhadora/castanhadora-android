package br.coop.eita.castanhadora.util;

import android.text.format.DateUtils;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.Currency;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import br.coop.eita.castanhadora.Castanhadora;
import br.coop.eita.castanhadora.db.models.Safra;

public class Util {
    public static String formatMoney(BigDecimal money) {
        if (money == null) {
            money = BigDecimal.ZERO;
        }
        NumberFormat format = NumberFormat.getCurrencyInstance(Locale.getDefault());
        String result = format.format(money.floatValue());
        return result;
    }

    public static int getYearsBetweenDates(Date first, Date second) {
        Calendar firstCal = GregorianCalendar.getInstance();
        Calendar secondCal = GregorianCalendar.getInstance();

        firstCal.setTime(first);
        secondCal.setTime(second);

        secondCal.add(Calendar.DAY_OF_YEAR, 1 - firstCal.get(Calendar.DAY_OF_YEAR));

        return secondCal.get(Calendar.YEAR) - firstCal.get(Calendar.YEAR);
    }

    public static String formatNumber(Object number) {
        return NumberFormat.getInstance().format(number);
    }

    public static String unidadeValorSingular(String unidadeValor) {
        return unidadeValor.substring(0,unidadeValor.length()-1);
    }

    public static String formatDate(Date value) {
        return DateUtils.formatDateTime(Castanhadora.APP, value.getTime(),
                DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_NUMERIC_DATE | DateUtils.FORMAT_SHOW_YEAR);
    }

    public static boolean deveInserirRemuneracao(Safra safra) {
        return safra == null || safra.getRemuneracaoDesejadaPorDia().compareTo(BigDecimal.ZERO) > 0;
    }
}
