package br.coop.eita.castanhadora.db.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Query;

import java.util.List;

import br.coop.eita.castanhadora.db.models.TipoEquipamento;
import br.coop.eita.castanhadora.db.models.VersaoBancoDados;

@Dao
public abstract class VersaoBancoDadosDao extends BaseIdentifiableDao<VersaoBancoDados> {

}
