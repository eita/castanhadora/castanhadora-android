package br.coop.eita.castanhadora.viewmodels;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.databinding.Bindable;
import androidx.lifecycle.LiveData;

import java.util.List;

import br.coop.eita.castanhadora.db.DataRepository;
import br.coop.eita.castanhadora.db.models.UpfIntegrante;
import br.coop.eita.castanhadora.db.models.Safra;
import br.coop.eita.castanhadora.db.models.Upf;

public class UpfViewModel extends BaseViewModel {

    private Upf upf;

    private UpfIntegrante currentIntegranteUpf;

    private Integer currentIntegranteUpfId;


    public UpfViewModel(@NonNull Application application) {
        super(application);
        Log.i("TEST", "create upf view model");
        this.upf = dataRepository.getUpf();
    }

    public Upf getUpf() {
        return upf;
    }

    public String getTesteVal() {
        return "Teste";
    }

    public Safra getSafraAtiva() {
        return dataRepository.getSafraAtiva();
    }

    public void saveUpf(DataRepository.OnSaveEntityListener listener) {
        dataRepository.saveUpf(listener);
    }

    public void reloadUpf() {
        this.upf = dataRepository.reloadUpf();
    }

    @Bindable
    public UpfIntegrante getCurrentIntegranteUpf() {
        return currentIntegranteUpf;
    }

    public void setCurrentIntegranteUpf(UpfIntegrante currentIntegranteUpf) {
        this.currentIntegranteUpf = currentIntegranteUpf;
        currentIntegranteUpfId = currentIntegranteUpf != null ? currentIntegranteUpf.getId() : null;
    }

    public UpfIntegrante loadCurrentIntegranteUpf(Integer integranteUpfId) {
        if (integranteUpfId != currentIntegranteUpfId) {
            if (integranteUpfId != null && integranteUpfId > 0) {
                currentIntegranteUpf = dataRepository.load(UpfIntegrante.class, integranteUpfId);
            } else {
                currentIntegranteUpf = dataRepository.newIntegranteUpf();
            }
            currentIntegranteUpfId = integranteUpfId;
        }

        return currentIntegranteUpf;
    }

    public void saveCurrentIntegranteUpf(DataRepository.OnSaveEntityListener listener) {
        dataRepository.save(currentIntegranteUpf, listener);
    }

    /**
     * Get Integrantes for current Safra
     *
     * @return
     */
    public LiveData<List<UpfIntegrante>> getIntegrantes() {
        return dataRepository.getUpfIntegrantesLive();
    }

    public void deleteUpfIntegrante(UpfIntegrante upfIntegrante) {
        dataRepository.delete(upfIntegrante);
    }

    public void saveMainIntegrante(DataRepository.OnSaveEntityListener listener) {
        Upf upf = getUpf();
        UpfIntegrante mainIntegrante = dataRepository.getMainIntegrante();
        if (mainIntegrante == null) {
            mainIntegrante = dataRepository.newIntegranteUpf();
        }
        mainIntegrante.setDataNascimento(upf.getDataNascimentoUsuario());
        mainIntegrante.setNome(upf.getNomeUsuario());
        mainIntegrante.setMainIntegrante(true);
        mainIntegrante.setGenero(upf.getGeneroUsuario());
        mainIntegrante.setParentesco("");
        dataRepository.save(mainIntegrante, listener);

    }
}
