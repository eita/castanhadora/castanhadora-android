package br.coop.eita.castanhadora.ui.fragments;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;

import br.coop.eita.castanhadora.R;
import br.coop.eita.castanhadora.databinding.DialogoSelectTipoDespesaBinding;
import br.coop.eita.castanhadora.db.models.Despesa;
import br.coop.eita.castanhadora.ui.activities.SafraDespesaDiariaFormActivity;
import br.coop.eita.castanhadora.ui.activities.SafraDespesaPadraoFormActivity;

public class SelectTipoDespesaDialogFragment extends DialogFragment {

    public static final int OPEN_DESPESA_PADRAO_FORM = 1;
    public static final int OPEN_DESPESA_DIARIAS_FORM = 2;

    public static SelectTipoDespesaDialogFragment newInstance() {
        SelectTipoDespesaDialogFragment f = new SelectTipoDespesaDialogFragment();
        return f;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Esse tipo de coisa dá pau no SDK 16
        //setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Material_Light_Dialog);
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT);
        }
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        getDialog().setTitle(R.string.label_despesa_new_title);

        DialogoSelectTipoDespesaBinding binding = DataBindingUtil.inflate(inflater,
                R.layout.dialogo_select_tipo_despesa, container, false);

        binding.btnDespesaAlimento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), SafraDespesaPadraoFormActivity.class);
                intent.putExtra("tipo", Despesa.TIPO_ALIMENTO);
                startActivityForResult(intent, OPEN_DESPESA_PADRAO_FORM);
                dismiss();
            }
        });

        binding.btnDespesaCombustivel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), SafraDespesaPadraoFormActivity.class);
                intent.putExtra("tipo", Despesa.TIPO_COMBUSTIVEL);
                startActivityForResult(intent, OPEN_DESPESA_PADRAO_FORM);
                dismiss();
            }
        });

        binding.btnDespesaManutencao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), SafraDespesaPadraoFormActivity.class);
                intent.putExtra("tipo", Despesa.TIPO_MANUTENCAO);
                startActivityForResult(intent, OPEN_DESPESA_PADRAO_FORM);
                dismiss();
            }
        });

        binding.btnDespesaTransporte.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), SafraDespesaPadraoFormActivity.class);
                intent.putExtra("tipo", Despesa.TIPO_TRANSPORTE);
                startActivityForResult(intent, OPEN_DESPESA_PADRAO_FORM);
                dismiss();
            }
        });

        binding.btnDespesaContas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), SafraDespesaPadraoFormActivity.class);
                intent.putExtra("tipo", Despesa.TIPO_CONTAS);
                startActivityForResult(intent, OPEN_DESPESA_PADRAO_FORM);
                dismiss();
            }
        });

        binding.btnDespesaReuniao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), SafraDespesaPadraoFormActivity.class);
                intent.putExtra("tipo", Despesa.TIPO_REUNIAO);
                startActivityForResult(intent, OPEN_DESPESA_PADRAO_FORM);
                dismiss();
            }
        });

        binding.btnDespesaFerramentas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), SafraDespesaPadraoFormActivity.class);
                intent.putExtra("tipo", Despesa.TIPO_FERRAMENTAS);
                startActivityForResult(intent, OPEN_DESPESA_PADRAO_FORM);
                dismiss();
            }
        });

        binding.btnDespesaDiarias.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), SafraDespesaDiariaFormActivity.class);
                startActivityForResult(intent, OPEN_DESPESA_DIARIAS_FORM);
                dismiss();
            }
        });

        View v = binding.getRoot();

        return v;
    }

}
