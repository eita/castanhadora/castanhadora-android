package br.coop.eita.castanhadora.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import androidx.databinding.DataBindingUtil;

import java.util.List;

import br.coop.eita.castanhadora.R;
import br.coop.eita.castanhadora.databinding.ListitemEquipamentoBinding;
import br.coop.eita.castanhadora.databinding.ListitemUpfIntegranteTrabalhaBinding;
import br.coop.eita.castanhadora.db.models.derived.UpfIntegranteTrabalha;
import br.coop.eita.castanhadora.ui.activities.SafraTrabalhoUpfFormActivity;

public class UpfIntegranteTrabalhaAdapter extends ArrayAdapter<UpfIntegranteTrabalha> {
    private List<UpfIntegranteTrabalha> eqtipos;
    private SafraTrabalhoUpfFormActivity activity;

    public UpfIntegranteTrabalhaAdapter(Context context, List<UpfIntegranteTrabalha> objects) {
        super(context, 0, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ListitemUpfIntegranteTrabalhaBinding binding;

        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(
                    R.layout.listitem_upf_integrante_trabalha, null);
            binding = DataBindingUtil.bind(convertView);
            convertView.setTag(binding);
        } else {
            binding = (ListitemUpfIntegranteTrabalhaBinding) convertView.getTag();
        }

        final UpfIntegranteTrabalha integrante = getItem(position);
        final UpfIntegranteTrabalhaAdapter self = this;

        binding.setIntegrante(integrante);

        return binding.getRoot();
    }

    public void setActivity(SafraTrabalhoUpfFormActivity activity) {
        this.activity = activity;
    }
}
