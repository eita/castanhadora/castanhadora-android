package br.coop.eita.castanhadora.db.models;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = Uf.TABLE_NAME)
public class Uf extends BaseIdentifiableModel {
    public static final String TABLE_NAME = "uf";

    @PrimaryKey(autoGenerate = true)
    private int id;

    @NonNull
    @ColumnInfo(name = "nome")
    private String nome;

    @NonNull
    @ColumnInfo(name = "sigla")
    private String sigla;

    public Uf() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @NonNull
    public String getNome() {
        return nome;
    }

    public void setNome(@NonNull String nome) {
        this.nome = nome;
    }

    @NonNull
    public String getSigla() {
        return sigla;
    }

    public void setSigla(@NonNull String sigla) {
        this.sigla = sigla;
    }
}
