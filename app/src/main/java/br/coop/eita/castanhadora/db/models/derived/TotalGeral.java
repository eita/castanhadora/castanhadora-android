package br.coop.eita.castanhadora.db.models.derived;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class TotalGeral {
    private Integer qtdeProduzido;

    private Integer qtdeVendido;

    private Integer qtdeEstoque;

    //SUM(venda.valorUnitario * venda.quantidade)
    private BigDecimal valorTotalVendido;

    //SUM(despesa.valor)
    private BigDecimal valorTotalDespesas;

    //Total de dias (por pessoa) trabalhados
    private BigDecimal totalDiasTrabalhados;

    //Safra.remuneracaoDesejadaPorDia
    private BigDecimal remuneracaoDesejadaPorDia;

    //SUM(Dias Trabalhados * Remuneracao desejada por dia)
    private BigDecimal remuneracaoDesejada;

    //Depreciação do equipamento
    private BigDecimal valorDepreciacao;

    //Valor Total Despesas + remuneração desejada
    private BigDecimal valorCustosTotais;

    //Valor Total Despesas + Remuneracao Desejada + Depreciacao
    private BigDecimal valorTotalAAlcancar;

    //Valor total vendido / quantidade vendido (pode ser null)
    private BigDecimal valorMedioVendido;

    public Integer getQtdeProduzido() {
        return qtdeProduzido;
    }

    public void setQtdeProduzido(Integer qtdeProduzido) {
        this.qtdeProduzido = qtdeProduzido;
    }

    public Integer getQtdeVendido() {
        return qtdeVendido;
    }

    public void setQtdeVendido(Integer qtdeVendido) {
        this.qtdeVendido = qtdeVendido;
    }

    public Integer getQtdeEstoque() {
        return qtdeEstoque;
    }

    public void setQtdeEstoque(Integer qtdeEstoque) {
        this.qtdeEstoque = qtdeEstoque;
    }

    public BigDecimal getValorTotalVendido() {
        return valorTotalVendido;
    }

    public void setValorTotalVendido(BigDecimal valorTotalVendido) {
        this.valorTotalVendido = valorTotalVendido;
    }

    public BigDecimal getValorTotalDespesas() {
        return valorTotalDespesas;
    }

    public void setValorTotalDespesas(BigDecimal valorTotalDespesas) {
        this.valorTotalDespesas = valorTotalDespesas;
    }

    public BigDecimal getTotalDiasTrabalhados() {
        return totalDiasTrabalhados;
    }

    public void setTotalDiasTrabalhados(BigDecimal totalDiasTrabalhados) {
        this.totalDiasTrabalhados = totalDiasTrabalhados;
    }

    public BigDecimal getRemuneracaoDesejadaPorDia() {
        return remuneracaoDesejadaPorDia;
    }

    public void setRemuneracaoDesejadaPorDia(BigDecimal remuneracaoDesejadaPorDia) {
        this.remuneracaoDesejadaPorDia = remuneracaoDesejadaPorDia;
    }

    public BigDecimal getRemuneracaoDesejada() {
        return remuneracaoDesejada;
    }

    public void setRemuneracaoDesejada(BigDecimal remuneracaoDesejada) {
        this.remuneracaoDesejada = remuneracaoDesejada;
    }

    public BigDecimal getValorDepreciacao() {
        return valorDepreciacao;
    }

    public void setValorDepreciacao(BigDecimal valorDepreciacao) {
        this.valorDepreciacao = valorDepreciacao;
    }

    public BigDecimal getValorCustosTotais() {
        return valorCustosTotais;
    }

    public void setValorCustosTotais(BigDecimal valorCustosTotais) {
        this.valorCustosTotais = valorCustosTotais;
    }

    public BigDecimal getValorTotalAAlcancar() {
        return valorTotalAAlcancar;
    }

    public void setValorTotalAAlcancar(BigDecimal valorTotalAAlcancar) {
        this.valorTotalAAlcancar = valorTotalAAlcancar;
    }

    public BigDecimal getValorMedioVendido() {
        return valorMedioVendido;
    }

    public void setValorMedioVendido(BigDecimal valorMedioVendido) {
        this.valorMedioVendido = valorMedioVendido;
    }

    // O preço sugerido é a divisão do (valor almejado menos o valor já vendido), pelo estoque faltante
    public BigDecimal getPrecoSugerido() {
        return qtdeEstoque == 0 ? null :
                valorTotalAAlcancar.subtract(valorTotalVendido).divide(new BigDecimal(qtdeEstoque),
                        2, RoundingMode.HALF_EVEN);
    }

    public BigDecimal getPrecoSemDepreciacao() {
        return qtdeEstoque == 0 ? null :
                valorCustosTotais.subtract(valorTotalVendido).divide(new BigDecimal(qtdeEstoque), 2,
                        RoundingMode.HALF_EVEN);
    }

    public BigDecimal getPrecoApenasDespesas() {
        return qtdeEstoque == 0 ? null :
                valorTotalDespesas.subtract(valorTotalVendido).divide(new BigDecimal(qtdeEstoque),
                        2, RoundingMode.HALF_EVEN);
    }

    public DadosSimulacaoVenda simularVenda(BigDecimal valorUnitarioSugerido, Integer quantidade) {
        return new DadosSimulacaoVenda(valorUnitarioSugerido, quantidade, this);
    }

    public BigDecimal getExcedenteAtual() {
        BigDecimal valorTotalAAlcancar = getValorTotalAAlcancar();
        BigDecimal valorTotalVendido = getValorTotalVendido();
        
        BigDecimal excedente = valorTotalVendido.subtract(valorTotalAAlcancar);

        if (excedente.floatValue() <= 0) {
            return BigDecimal.ZERO;
        } else {
            return excedente;
        }
    }


    public class DadosSimulacaoVenda {
        private BigDecimal valorUnitarioSugerido;
        private Integer quantidade;
        private TotalGeral totalGeral;

        public DadosSimulacaoVenda(BigDecimal valorUnitarioSugerido, Integer quantidade,
                                   TotalGeral totalGeral) {
            this.valorUnitarioSugerido = valorUnitarioSugerido;
            this.quantidade = quantidade;
            this.totalGeral = totalGeral;
        }

        /**
         * Porcentagem de despesas já cobertas com o valor sugerido, considerando a produção total
         *
         * @return Integer 0-100
         */
        public Integer getSimulacaoCoberturaDespesas() {
            return getCalculoCoberturaDespesas(quantidade);
        }

        private Integer getCalculoCoberturaDespesas(Integer qtd) {
            Float valorTotalDespesas = totalGeral.getValorTotalDespesas().floatValue();
            if (valorTotalDespesas == 0.0) {
                return 100;
            }

            Float valorTotalVendido = totalGeral.getValorTotalVendido().floatValue();

            Float valorVenda = valorUnitarioSugerido.floatValue() * qtd;
            Float vendidoPrevisto = valorTotalVendido + valorVenda;

            return cutToPercent(vendidoPrevisto * 100 / valorTotalDespesas);
        }

        /**
         * Porcentagem de remuneração familiar já coberta com o valor sugerido, considerando a
         * produção total
         *
         * @return Integer 0-100
         */
        public Integer getSimulacaoCoberturaRemuneracao() {
            return getCalculoCoberturaRemuneracao(quantidade);
        }

        private Integer getCalculoCoberturaRemuneracao(Integer qtd) {
            Float remuneracaoDesejada = totalGeral.getRemuneracaoDesejada().floatValue();
            if (remuneracaoDesejada == 0.0) {
                return 100;
            }
            Float valorTotalDespesas = totalGeral.getValorTotalDespesas().floatValue();
            Float valorTotalVendido = totalGeral.getValorTotalVendido().floatValue();

            Float vendidoParaRemuneracao = valorTotalVendido - valorTotalDespesas;

            Float valorVenda = valorUnitarioSugerido.floatValue() * qtd;
            Float vendidoPrevisto = vendidoParaRemuneracao + valorVenda;

            return cutToPercent(vendidoPrevisto * 100 / remuneracaoDesejada);
        }

        /**
         * Porcentagem de depreciação já coberta com o valor sugerido, considerando a produção total
         *
         * @return Integer 0-100
         */
        public Integer getSimulacaoCoberturaDepreciacao() {
            return getCalculoCoberturaDepreciacao(quantidade);
        }

        private Integer getCalculoCoberturaDepreciacao(Integer qtd) {
            Float depreciacao = totalGeral.getValorDepreciacao().floatValue();
            if (depreciacao == 0.0) {
                return 100;
            }
            Float valorCustosTotais = totalGeral.getValorCustosTotais().floatValue();
            Float valorTotalVendido = totalGeral.getValorTotalVendido().floatValue();

            Float vendidoParaDepreciacao = valorTotalVendido - valorCustosTotais;

            Float valorVenda = valorUnitarioSugerido.floatValue() * qtd;
            Float vendidoPrevisto = vendidoParaDepreciacao + valorVenda;

            return cutToPercent(vendidoPrevisto * 100 / depreciacao);
        }

        /**
         * Excedente obtido considerando o valor sugerido
         *
         * @return BigDecimal
         */
        public BigDecimal getSimulacaoExcedente() {
            return getCalculoExcedente(quantidade);
        }

        private BigDecimal getCalculoExcedente(Integer qtd) {
            BigDecimal valorTotalAAlcancar = totalGeral.getValorTotalAAlcancar();
            BigDecimal valorTotalVendido = totalGeral.getValorTotalVendido();

            BigDecimal valorVenda = valorUnitarioSugerido.multiply(new BigDecimal(qtd));

            BigDecimal excedente = valorTotalVendido.add(valorVenda).subtract(valorTotalAAlcancar);

            if (excedente.floatValue() <= 0) {
                return BigDecimal.ZERO;
            } else {
                return excedente;
            }
        }

        /**
         * Porcentagem de despesas a serem cobertas com o valor sugerido, se todo o resto do estoque
         * for vendido a este preço
         *
         * @return Integer 0-100
         */
        public Integer getProjecaoCoberturaDespesas() {
            return getCalculoCoberturaDespesas(totalGeral.getQtdeEstoque());
        }

        /**
         * Porcentagem de remuneração familiar a ser coberta com o valor sugerido, se todo o resto
         * do estoque for vendido a este preço
         *
         * @return Integer 0-100
         */
        public Integer getProjecaoCoberturaRemuneracao() {
            return getCalculoCoberturaRemuneracao(totalGeral.getQtdeEstoque());
        }

        /**
         * Porcentagem de depreciação a ser coberta com o valor sugerido, se todo o resto do estoque
         * for vendido a este preço
         *
         * @return Integer 0-100
         */
        public Integer getProjecaoCoberturaDepreciacao() {
            return getCalculoCoberturaDepreciacao(totalGeral.getQtdeEstoque());
        }

        /**
         * Excedente a ser obtido se todo o resto do estoque for vendido a este preço
         *
         * @return BigDecimal
         */
        public BigDecimal getProjecaoExcedente() {
            return getCalculoExcedente(totalGeral.getQtdeEstoque());
        }

        public BigDecimal getNovoValorPrevisto() {
            BigDecimal valorTotalAAlcancar = totalGeral.getValorTotalAAlcancar();
            BigDecimal valorTotalVendido = totalGeral.getValorTotalVendido();

            BigDecimal valorVenda = valorUnitarioSugerido.multiply(new BigDecimal(quantidade));

            BigDecimal novoValorVendido = valorTotalVendido.add(valorVenda);
            Integer novaQuantidadeEstoque = totalGeral.getQtdeEstoque() - quantidade;

            if (novaQuantidadeEstoque == 0) {
                return null;
            }

            return valorTotalAAlcancar.subtract(novoValorVendido).divide(new BigDecimal(novaQuantidadeEstoque), 2,
                    RoundingMode.HALF_EVEN);
        }

        /**
         * normaliza o valor para um inteiro na escala de 0-100 (cortando se é maior que
         * 100 ou menor que 0)
         *
         * @param value
         * @return
         */
        public Integer cutToPercent(Float value) {
            if (value <= 0) {
                return 0;
            } else if (value >= 100) {
                return 100;
            } else {
                return Math.round(value);
            }
        }


    }
}
