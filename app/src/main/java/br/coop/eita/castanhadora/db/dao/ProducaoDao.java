package br.coop.eita.castanhadora.db.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Query;

import java.util.List;

import br.coop.eita.castanhadora.db.models.Despesa;
import br.coop.eita.castanhadora.db.models.Equipamento;
import br.coop.eita.castanhadora.db.models.Producao;
import br.coop.eita.castanhadora.db.models.Venda;

@Dao
public abstract class ProducaoDao extends BaseIdentifiableDao<Producao> {

    @Query("SELECT * FROM " + Producao.TABLE_NAME + " WHERE safra_id = :safraId ORDER BY data DESC")
    public abstract LiveData<List<Producao>> getProducoesLive(Integer safraId);

    @Query("SELECT * FROM " + Producao.TABLE_NAME + " WHERE safra_id = :safraId ORDER BY data DESC")
    public abstract List<Producao> getProducoes(Integer safraId);

    @Query("SELECT SUM(quantidade) FROM " + Producao.TABLE_NAME + " WHERE safra_id = :safraId")
    public abstract Integer getProducaoTotal(Integer safraId);

    @Query("SELECT SUM(quantidade) FROM " + Venda.TABLE_NAME + " WHERE safra_id = :safraId")
    public abstract Integer getProducaoVendida(Integer safraId);

}
