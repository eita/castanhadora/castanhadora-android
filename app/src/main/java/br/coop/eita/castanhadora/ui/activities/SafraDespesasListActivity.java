package br.coop.eita.castanhadora.ui.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.List;

import br.coop.eita.castanhadora.R;
import br.coop.eita.castanhadora.databinding.ActivitySafraDespesaPadraoFormBinding;
import br.coop.eita.castanhadora.databinding.ActivitySafraDespesasListBinding;
import br.coop.eita.castanhadora.db.models.Despesa;
import br.coop.eita.castanhadora.db.models.Equipamento;
import br.coop.eita.castanhadora.db.models.derived.EquipamentoETipo;
import br.coop.eita.castanhadora.ui.adapters.DespesaAdapter;
import br.coop.eita.castanhadora.ui.adapters.EquipamentoAdapter;
import br.coop.eita.castanhadora.viewmodels.DespesaViewModel;

public class SafraDespesasListActivity extends AppCompatActivity {

    public static final int EDITAR_DESPESA_ID_REPLY = 1;

    private DespesaViewModel viewModel;

    private DespesaAdapter despesaAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final ActivitySafraDespesasListBinding binding = DataBindingUtil.setContentView(this,
                R.layout.activity_safra_despesas_list);

        viewModel = ViewModelProviders.of(this).get(DespesaViewModel.class);
        binding.setViewmodel(viewModel);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(R.string.label_despesas);
        }

        LiveData<List<Despesa>> despesas = viewModel.getDespesas();

        final SafraDespesasListActivity self = this;

        despesas.observe(this, new Observer<List<Despesa>>() {
            @Override
            public void onChanged(List<Despesa> desps) {
                if (desps != null) {
                    if (desps.size() == 0) {
                        Toast.makeText(self, R.string.list_sem_despesas, Toast.LENGTH_LONG).show();
                        finish();
                    }
                    despesaAdapter = new DespesaAdapter(getApplicationContext(), desps);
                    binding.despesaList.setAdapter(despesaAdapter);
                    despesaAdapter.setActivity(self);
                    despesaAdapter.notifyDataSetChanged();
                }
            }
        });

    }

    public void openEditForm(Despesa despesa) {
        Intent intent;

        if (despesa.getTipo().equals(Despesa.TIPO_DIARIAS)) {
            intent = new Intent(this, SafraDespesaDiariaFormActivity.class);
        } else {
            intent = new Intent(this, SafraDespesaPadraoFormActivity.class);
            intent.putExtra("tipo", despesa.getTipo());
        }
        intent.putExtra("despesa_id", despesa.getId());
        startActivityForResult(intent, EDITAR_DESPESA_ID_REPLY);
    }

    public void openDeleteDialog(final Despesa despesa) {
        new AlertDialog.Builder(this).setMessage(
                getResources().getString(R.string.msg_dialog_despesa_remover,
                        despesa.getDescricao())).setPositiveButton(R.string.dialog_sim,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        viewModel.deleteDespesa(despesa);
                    }
                }).setNegativeButton(R.string.dialog_nao, null).setTitle(
                R.string.label_dialog_despesa_delete_title).show();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent replyIntent = new Intent();
                setResult(RESULT_CANCELED, replyIntent);
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
