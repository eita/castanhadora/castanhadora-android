package br.coop.eita.castanhadora.db.models;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = TipoEquipamento.TABLE_NAME)
public class TipoEquipamento extends BaseIdentifiableModel {
    public static final String TABLE_NAME = "tipo_equipamento";

    @PrimaryKey(autoGenerate = true)
    private int id;

    @NonNull
    @ColumnInfo(name = "nome")
    private String nome;

    public TipoEquipamento() {
    }

    @Ignore
    public TipoEquipamento(@NonNull String nome) {
        this.nome = nome;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @NonNull
    public String getNome() {
        return nome;
    }

    public void setNome(@NonNull String nome) {
        this.nome = nome;
    }

    @Override
    public boolean equals(Object obj) {
        return (obj instanceof Equipamento && ((Equipamento) obj).getId() == id);
    }

    @Override
    public String toString() {
        return nome;
    }
}


