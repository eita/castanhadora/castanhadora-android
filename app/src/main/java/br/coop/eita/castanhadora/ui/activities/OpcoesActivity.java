package br.coop.eita.castanhadora.ui.activities;

import android.Manifest;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import java.io.File;

import br.coop.eita.castanhadora.R;
import br.coop.eita.castanhadora.databinding.ActivityOpcoesBinding;
import br.coop.eita.castanhadora.db.models.Safra;
import br.coop.eita.castanhadora.util.DbCastanhadoraUtil;
import br.coop.eita.castanhadora.viewmodels.MainViewModel;

public class OpcoesActivity extends AppCompatActivity {

    private MainViewModel viewModel;

    public static final int CRIAR_SAFRA_ACTIVITY_REQUEST_CODE = 1;
    public static final int EDITAR_SAFRA_ACTIVITY_REQUEST_CODE = 2;

    public static final int PERMISSAO_ESCRITA_REQUEST_CODE = 1;
    private Uri uriImportacaoBackup;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        viewModel = ViewModelProviders.of(this).get(MainViewModel.class);

        ActivityOpcoesBinding binding = DataBindingUtil.setContentView(this,
                R.layout.activity_opcoes);
        binding.setViewmodel(viewModel);

        //Required to bind live data to view
        binding.setLifecycleOwner(this);

        Boolean parentSafraInicio = getIntent().getBooleanExtra("parent_safra",false);
        viewModel.setParentSafraInicio(parentSafraInicio);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null && parentSafraInicio) {
            actionBar.setTitle(R.string.label_safra_home_alterar_dados_iniciais);
            if (parentSafraInicio) {
                actionBar.setDisplayHomeAsUpEnabled(true);
            }
        }

    }

    public void openUnidadesActivity(View view) {
        Intent intent = new Intent(OpcoesActivity.this, UnidadeFormActivity.class);
        startActivity(intent);
    }

    public void openPermissaoCompartilhamentoActivity(View view) {
        Intent intent = new Intent(OpcoesActivity.this, PermissaoCompartilharNuvemActivity.class);
        startActivity(intent);
    }

    public void openUpfActivity(View view) {
        Intent intent = new Intent(OpcoesActivity.this, UpfIntegranteListActivity.class);
        startActivity(intent);
    }

    public void openCriarSafraActivity(View view) {
        Intent intent = new Intent(OpcoesActivity.this, SafraFormActivity.class).putExtra("upf_id",
                viewModel.getUpf().getId());
        startActivityForResult(intent, CRIAR_SAFRA_ACTIVITY_REQUEST_CODE);
    }

    public void openSafraInicioActivity(View view) {
        Intent intent = new Intent(OpcoesActivity.this, SafraInicioActivity.class);
        startActivity(intent);
        finish();
    }

    public void openEditarSafraActivity(View view) {
        Intent intent = new Intent(OpcoesActivity.this, SafraFormActivity.class).putExtra(
                "safra_id", viewModel.getSafraAtiva().getId());
        startActivityForResult(intent, EDITAR_SAFRA_ACTIVITY_REQUEST_CODE);
    }

    public void openCastanhalFormActivity(View view) {
        Intent intent = new Intent(OpcoesActivity.this, SafraCastanhalFormActivity.class);
        startActivity(intent);
    }

    public void openRemuneracaoAlmejadaActivity(View view) {
        Intent intent = new Intent(OpcoesActivity.this, SafraRemuneracaoDesejadaActivity.class);
        startActivity(intent);
    }

    public void openUpfDadosPessoaisActivity(View view) {
        Intent intent = new Intent(OpcoesActivity.this, UpfDadosPessoaisActivity.class);
        startActivity(intent);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CRIAR_SAFRA_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            viewModel.setSafraAtiva(data.getIntExtra(SafraFormActivity.SAFRA_ID_REPLY, 0));
        } else if (resultCode == Safra.RESULT_SAFRA_ENCERROU) {
            Intent replyIntent2 = new Intent();
            setResult(Safra.RESULT_SAFRA_ENCERROU, replyIntent2);
            finish();
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent replyIntent = new Intent();
                setResult(RESULT_CANCELED, replyIntent);
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
