package br.coop.eita.castanhadora.ui.components;

import android.content.Context;
import android.text.format.DateUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.Observable;
import androidx.databinding.library.baseAdapters.BR;
import androidx.fragment.app.FragmentActivity;

import java.util.Date;

import br.coop.eita.castanhadora.Castanhadora;
import br.coop.eita.castanhadora.databinding.ComponentDateChooserBinding;
import br.coop.eita.castanhadora.ui.fragments.DatePickerDialogFragment;
import br.coop.eita.castanhadora.util.OnSelectedDateOrTimeCallback;

public class DateChooser extends ConstraintLayout {

    private ComponentDateChooserBinding mBinding;

    private Context mContext;

    private DateChooserViewModel vm;

    public DateChooser(@NonNull Context context) {
        this(context, null);
    }

    public DateChooser(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DateChooser(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        LayoutInflater inflater = LayoutInflater.from(context);

        mBinding = ComponentDateChooserBinding.inflate(inflater, this, true);

        vm = new DateChooserViewModel();

        mBinding.setViewmodel(vm);
        mContext = context;

        mBinding.dateText.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                openDatePìcker(view);
            }
        });

        mBinding.dateText.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    openDatePìcker(view);
                }
            }
        });

        mBinding.dateText.setKeyListener(null);

        mBinding.removeDateButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickRemoveDate(view);
            }
        });

    }

    public void openDatePìcker(View view) {
        DatePickerDialogFragment dialogFragment = new DatePickerDialogFragment(
                vm.getDateValue(), vm.getYearFirst(), new OnSelectedDateOrTimeCallback() {
            @Override
            public void onSelect(Date dataSelecionada) {
                vm.setDateValue(dataSelecionada);
                //viewModel.getSafra().setData(dataInicio);
                String datestr =  DateUtils.formatDateTime(Castanhadora.APP, dataSelecionada.getTime(),
                        DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_NUMERIC_DATE | DateUtils.FORMAT_SHOW_YEAR);
                //Log.i("TEST","DATA INICIAL COMPONENT:" + datestr);
            }
        });

        dialogFragment.show(((FragmentActivity) mContext).getSupportFragmentManager(), "datePickerInicio");
    }

    public void onClickRemoveDate(View view) {
        vm.setDateValue(null);
    }

    public class DateChooserViewModel extends BaseObservable {
        private Date dateValue;

        private Boolean yearFirst = false;

        @Bindable
        public Date getDateValue() {
            return dateValue;
        }

        public void setDateValue(Date dateValue) {
            this.dateValue = dateValue;
            notifyPropertyChanged(BR.dateValue);
        }

        @Bindable
        public Boolean getYearFirst() {
            return yearFirst;
        }

        public void setYearFirst(Boolean yearFirst) {
            this.yearFirst = yearFirst;
            notifyPropertyChanged(BR.yearFirst);
        }
    }

    public void setDate(Date date) {
        vm.setDateValue(date);
    }

    public Date getDate() {
        return vm.getDateValue();
    }

    public void setYearFirst(Boolean yearFirst) {
        vm.setYearFirst(yearFirst);
    }

    public interface OnDateChangedListener {
        public void onDateChanged(Date newDate);
    }

    public void addDateChangedListener(final OnDateChangedListener listener) {
        vm.addOnPropertyChangedCallback(new Observable.OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(Observable sender, int propertyId) {
                if (propertyId == BR.dateValue) {
                    DateChooserViewModel viewModel = (DateChooserViewModel) sender;
                    listener.onDateChanged(viewModel.getDateValue());
                }
            }
        });
    }

}
