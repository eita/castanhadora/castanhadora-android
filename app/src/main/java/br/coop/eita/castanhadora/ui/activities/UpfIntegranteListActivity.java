package br.coop.eita.castanhadora.ui.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import java.util.List;

import br.coop.eita.castanhadora.R;
import br.coop.eita.castanhadora.databinding.ActivityUpfIntegranteListBinding;
import br.coop.eita.castanhadora.db.DataRepository;
import br.coop.eita.castanhadora.db.models.UpfIntegrante;
import br.coop.eita.castanhadora.ui.adapters.UpfIntegranteAdapter;
import br.coop.eita.castanhadora.ui.util.Wizardable;
import br.coop.eita.castanhadora.viewmodels.UpfViewModel;

public class UpfIntegranteListActivity extends AppCompatActivity implements Wizardable {

    public static final int CRIAR_INTEGRANTE_UPF_ID_REPLY = 1;
    public static final int EDITAR_INTEGRANTE_UPF_ID_REPLY = 2;


    private UpfViewModel mUpfViewModel;
    private UpfIntegranteAdapter upfIntegranteAdapter;

    private Boolean isWizard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        isWizard = getIntent().getBooleanExtra("wizard", false);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null && !isWizard) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        final ActivityUpfIntegranteListBinding binding = DataBindingUtil.setContentView(this,
                R.layout.activity_upf_integrante_list);

        mUpfViewModel = ViewModelProviders.of(this).get(UpfViewModel.class);

        binding.setViewmodel(mUpfViewModel);

        LiveData<List<UpfIntegrante>> integrantes = mUpfViewModel.getIntegrantes();

        final UpfIntegranteListActivity self = this;

        integrantes.observe(this, new Observer<List<UpfIntegrante>>() {
            @Override
            public void onChanged(List<UpfIntegrante> upfIntegrantes) {
                if (upfIntegrantes != null) {
                    upfIntegranteAdapter = new UpfIntegranteAdapter(getApplicationContext(), upfIntegrantes);
                    binding.upfIntegrantes.setAdapter(upfIntegranteAdapter);
                    upfIntegranteAdapter.setActivity(self);
                    upfIntegranteAdapter.notifyDataSetChanged();
                }
            }
        });

        binding.setWizard(isWizard);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!isWizard) {
            getMenuInflater().inflate(R.menu.menu_form, menu);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        final UpfIntegranteListActivity self = this;

        switch (item.getItemId()) {
            case android.R.id.home:
                mUpfViewModel.reloadUpf();
                finish();
                return true;
            case R.id.action_save:
                mUpfViewModel.saveUpf(new DataRepository.OnSaveEntityListener() {
                    @Override
                    public void onSuccess(Integer... ids) {
                        Intent replyIntent2 = new Intent();
                        setResult(RESULT_OK, replyIntent2);
                        Toast.makeText(self, R.string.saved_generic, Toast.LENGTH_LONG).show();
                        finish();
                    }

                    @Override
                    public void onFailure(int errorCode, List<String> errorMessages) {
                        Toast.makeText(self, errorMessages.get(0), Toast.LENGTH_LONG).show();
                    }
                });
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void openNewIntegranteForm(View view) {
        Intent intent = new Intent(UpfIntegranteListActivity.this, UpfIntegranteFormActivity.class).putExtra(
                "safra_id", mUpfViewModel.getSafraAtiva().getId());
        startActivityForResult(intent, CRIAR_INTEGRANTE_UPF_ID_REPLY);
    }

    public void openEditIntegranteForm(UpfIntegrante upfIntegrante) {
        Intent intent = new Intent(UpfIntegranteListActivity.this, UpfIntegranteFormActivity.class).putExtra(
                "upf_integrante_id", upfIntegrante.getId());
        startActivityForResult(intent, EDITAR_INTEGRANTE_UPF_ID_REPLY);
    }

    public void openDeleteIntegranteDialog(final UpfIntegrante upfIntegrante) {
        new AlertDialog.Builder(this)
                .setMessage(getResources().getString(R.string.msg_dialog_upf_integrante_remover,upfIntegrante.getNome()))
                .setPositiveButton(R.string.dialog_sim, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mUpfViewModel.deleteUpfIntegrante(upfIntegrante);
                    }
                })
                .setNegativeButton(R.string.dialog_nao, null)
                .setTitle(R.string.label_dialog_upf_integrante_delete_title)
                .show();
    }

    @Override
    public void onClickWizardPrevious(View view) {
        Intent intent = new Intent(this, UpfDadosPessoaisActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        intent.putExtra("wizard", true);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        finish();
    }

    @Override
    public void onClickWizardNext(View view) {
        UpfIntegranteListActivity self = this;
        if (upfIntegranteAdapter.getCount() >= 1) {
            Intent intent = new Intent(self, UnidadeFormActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            intent.putExtra("wizard", true);
            startActivity(intent);
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            finish();
        } else {

        }
    }
}
